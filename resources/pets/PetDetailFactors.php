<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:03 AM
 */

namespace apptlibrary\resources\pets;

/**
 * Class PetDetailFactors
 * @package apptlibrary\resources\pets
 */
class PetDetailFactors
{
    /**
     * The ID of the pet detail.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The client ID of the pet detail object.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The associated pet ID.
     * @access protected
     * @var integer
     */
    protected $petId;
    /**
     * The associated factor ID.
     * @access protected
     * @var integer
     */
    protected $factorId;
    /**
     * The associated pet factor object.
     * @access protected
     * @var PetFactors
     */
    protected $petFactorObject;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     *
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     *
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the petId property value
     *
     * @return int
     */
    public function getPetId()
    {
        return $this->petId;
    }

    /**
     * Sets the petId property value
     *
     * @param int $petId
     *
     * @return $this
     */
    public function setPetId($petId)
    {
        $this->petId = $petId;
        return $this;
    }

    /**
     * Returns the factorId property value
     *
     * @return int
     */
    public function getFactorId()
    {
        return $this->factorId;
    }

    /**
     * Sets the factorId property value
     *
     * @param int $factorId
     *
     * @return $this
     */
    public function setFactorId($factorId)
    {
        $this->factorId = $factorId;
        return $this;
    }

    /**
     * Returns the petFactorObject property value
     *
     * @return PetFactors
     */
    public function getPetFactorObject()
    {
        return $this->petFactorObject;
    }

    /**
     * Sets the petFactorObject property value
     *
     * @param PetFactors $petFactorObject
     *
     * @return $this
     */
    public function setPetFactorObject(PetFactors $petFactorObject)
    {
        $this->petFactorObject = $petFactorObject;
        return $this;
    }
}