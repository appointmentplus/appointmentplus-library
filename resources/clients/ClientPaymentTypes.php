<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 12:13 AM
 */

namespace apptlibrary\resources\clients;

use \Exception;

/**
 * Class ClientPaymentTypes
 * @package apptlibrary\resources\clients
 */
class ClientPaymentTypes
{
    /**
     * Value for not displaying the client type
     * TODO: Confirm description
     */
    const NO = 0;
    /**
     * Value for displaying the client type
     * TODO: Confirm description
     */
    const YES = 1;
    /**
     * Value for the client type
     * TODO: Confirm description
     */
    const CREDIT = 1;
    /**
     * Value for the client type
     * TODO: Confirm description
     */
    const GIFT_CERTIFICATE = 2;
    /**
     * Value for the client type
     * TODO: Confirm description
     */
    const OTHER = 3;
    /**
     * The ID of the payment type.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The ID of the client for the payment.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The description of the payment type.
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * Flag which specifies if payment type should be displayed. (0 = no, 1 = yes)
     * @access protected
     * @var integer
     */
    protected $display;
    /**
     * The ID of the payment type. (1 = credit, 2 = gift certificate, 3 = other)
     * @access protected
     * @var integer
     */
    protected $typeId;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the display property value
     *
     * @return int
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Sets the display property value
     * @param int $display
     *
     * @return $this
     * @throws Exception
     */
    public function setDisplay($display)
    {
        if (in_array($display, array(self::NO, self::YES)))
        {
            $this->display = $display;
        }
        else
        {
            throw new Exception ('Value is not valid, expecting: self::NO or self::YES');
        }
        return $this;
    }

    /**
     * Returns the typeId property value
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Sets the typeId property value
     * @param int $typeId
     *
     * @return $this
     * @throws Exception
     */
    public function setTypeId($typeId)
    {
        if (in_array($typeId, array(self::CREDIT, self::GIFT_CERTIFICATE, self::OTHER)))
        {
            $this->typeId = $typeId;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::CREDIT, self::GIFT_CERTIFICATE or self::OTHER');
        }
        return $this;
    }
}
