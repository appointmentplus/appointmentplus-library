<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 12:29 AM
 */

namespace apptlibrary\resources\clients;

/**
 * Class Clients
 * @package apptlibrary\resources\clients
 */
class Clients
{
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $status;
    /**
     * The ID of the client.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The client master ID of the client.
     * @access protected
     * @var integer
     */
    protected $clientMasterId;
    /**
     * The name of the client.
     * @access protected
     * @var string
     */
    protected $name;
    /**
     * The client's first name contact.
     * @access protected
     * @var string
     */
    protected $contactFirstName;
    /**
     * The client's last name contact.
     * @access protected
     * @var string
     */
    protected $contactLastName;
    /**
     * The primary address for the client.
     * @access protected
     * @var string
     */
    protected $address1;
    /**
     * The secondary address for the client.
     * @access protected
     * @var string
     */
    protected $address2;
    /**
     * The city of the client's address.
     * @access protected
     * @var string
     */
    protected $city;
    /**
     * The state of the client's address.
     * @access protected
     * @var string
     */
    protected $state;
    /**
     * The postal code of the client's address.
     * @access protected
     * @var string
     */
    protected $postalCode;
    /**
     * The ID of the country for the client's address. See country
     * @access protected
     * @var integer
     */
    protected $countryId;
    /**
     * The primary phone number of the client.
     * @access protected
     * @var string
     */
    protected $phone;
    /**
     * The alternative phone number of the client.
     * @access protected
     * @var string
     */
    protected $alternativePhone;
    /**
     * The fax number of the client.
     * @access protected
     * @var string
     */
    protected $fax;
    /**
     * The email address for the client.
     * @access protected
     * @var string
     */
    protected $email;
    /**
     * The store number for the client.
     * @access protected
     * @var string
     */
    protected $storeNumber;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $siteType;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $businessType;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $website;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $absolutePath;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $frontEndFolderName;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $secureUrl;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $appointmentMethod;
    /**
     * The name of the database for the client.
     * @access protected
     * @var string
     */
    protected $databaseName;
    /**
     * The date/time the client was created.
     * @access protected
     * @var string
     */
    protected $whenAdded;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $setupComplete;
    /**
     * The parent client ID for the client. (If client is the parent then this value will match the ID)
     * @access protected
     * @var integer
     */
    protected $parentId;
    /**
     * The name of the location for the client.
     * @access protected
     * @var string
     */
    protected $locationName;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $shareParentPreferences;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $resellerId;
    /**
     * The directions to the client's location.
     * @access protected
     * @var string
     */
    protected $directions;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $locationNameAbbreviation;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $clientBillingStatusId;
    /**
     * The ID of the sales manager.
     * @access protected
     * @var integer
     */
    protected $salesManagerId;
    /**
     * The ID of the sales person.
     * @access protected
     * @var integer
     */
    protected $salesPersonId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $territoryId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $internalSalesPersonId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $sendWelcomeEmails;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $sendEndOfTrialEmails;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $resellerNotes;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $showWelcomeBanner;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $tierId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $brandId;
    /**
     * The time zone ID for the client. See time zones
     * @access protected
     * @var integer
     */
    protected $timeZoneId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $referrerDomain;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $referrerKeyword;
    /**
     * The account number for the client. (e.g., AP###-###)
     * @access protected
     * @var string
     */
    protected $accountNumber;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $payPerClickCampaign;
    /**
     * The date/time the client was updated.
     * @access protected
     * @var string
     */
    protected $whenUpdated;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $clientSizeId;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * A list of the schedule objects for the client. See client schedule object
     * @access protected
     * @var ClientSchedule
     */
    protected $clientSchedule;

    /**
     * Returns the status property value
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status property value
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientMasterId property value
     *
     * @return int
     */
    public function getClientMasterId()
    {
        return $this->clientMasterId;
    }

    /**
     * Sets the clientMasterId property value
     * @param int $clientMasterId
     *
     * @return $this
     */
    public function setClientMasterId($clientMasterId)
    {
        $this->clientMasterId = $clientMasterId;
        return $this;
    }

    /**
     * Returns the name property value
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name property value
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns the contactFirstName property value
     *
     * @return string
     */
    public function getContactFirstName()
    {
        return $this->contactFirstName;
    }

    /**
     * Sets the contactFirstName property value
     * @param string $contactFirstName
     *
     * @return $this
     */
    public function setContactFirstName($contactFirstName)
    {
        $this->contactFirstName = $contactFirstName;
        return $this;
    }

    /**
     * Returns the contactLastName property value
     *
     * @return string
     */
    public function getContactLastName()
    {
        return $this->contactLastName;
    }

    /**
     * Sets the contactLastName property value
     * @param string $contactLastName
     *
     * @return $this
     */
    public function setContactLastName($contactLastName)
    {
        $this->contactLastName = $contactLastName;
        return $this;
    }

    /**
     * Returns the address1 property value
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Sets the address1 property value
     * @param string $address1
     *
     * @return $this
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
        return $this;
    }

    /**
     * Returns the address2 property value
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Sets the address2 property value
     * @param string $address2
     *
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * Returns the city property value
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city property value
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Returns the state property value
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets the state property value
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Returns the postalCode property value
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Sets the postalCode property value
     * @param string $postalCode
     *
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * Returns the countryId property value
     *
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Sets the countryId property value
     * @param int $countryId
     *
     * @return $this
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
        return $this;
    }

    /**
     * Returns the phone property value
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone property value
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Returns the alternativePhone property value
     *
     * @return string
     */
    public function getAlternativePhone()
    {
        return $this->alternativePhone;
    }

    /**
     * Sets the alternativePhone property value
     * @param string $alternativePhone
     *
     * @return $this
     */
    public function setAlternativePhone($alternativePhone)
    {
        $this->alternativePhone = $alternativePhone;
        return $this;
    }

    /**
     * Returns the fax property value
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax property value
     * @param string $fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Returns the email property value
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email property value
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Returns the storeNumber property value
     *
     * @return string
     */
    public function getStoreNumber()
    {
        return $this->storeNumber;
    }

    /**
     * Sets the storeNumber property value
     * @param string $storeNumber
     *
     * @return $this
     */
    public function setStoreNumber($storeNumber)
    {
        $this->storeNumber = $storeNumber;
        return $this;
    }

    /**
     * Returns the siteType property value
     *
     * @return int
     */
    public function getSiteType()
    {
        return $this->siteType;
    }

    /**
     * Sets the siteType property value
     * @param int $siteType
     *
     * @return $this
     */
    public function setSiteType($siteType)
    {
        $this->siteType = $siteType;
        return $this;
    }

    /**
     * Returns the businessType property value
     *
     * @return int
     */
    public function getBusinessType()
    {
        return $this->businessType;
    }

    /**
     * Sets the businessType property value
     * @param int $businessType
     *
     * @return $this
     */
    public function setBusinessType($businessType)
    {
        $this->businessType = $businessType;
        return $this;
    }

    /**
     * Returns the website property value
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website property value
     * @param string $website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * Returns the absolutePath property value
     *
     * @return string
     */
    public function getAbsolutePath()
    {
        return $this->absolutePath;
    }

    /**
     * Sets the absolutePath property value
     * @param string $absolutePath
     *
     * @return $this
     */
    public function setAbsolutePath($absolutePath)
    {
        $this->absolutePath = $absolutePath;
        return $this;
    }

    /**
     * Returns the frontEndFolderName property value
     *
     * @return string
     */
    public function getFrontEndFolderName()
    {
        return $this->frontEndFolderName;
    }

    /**
     * Sets the frontEndFolderName property value
     * @param string $frontEndFolderName
     *
     * @return $this
     */
    public function setFrontEndFolderName($frontEndFolderName)
    {
        $this->frontEndFolderName = $frontEndFolderName;
        return $this;
    }

    /**
     * Returns the secureUrl property value
     *
     * @return string
     */
    public function getSecureUrl()
    {
        return $this->secureUrl;
    }

    /**
     * Sets the secureUrl property value
     * @param string $secureUrl
     *
     * @return $this
     */
    public function setSecureUrl($secureUrl)
    {
        $this->secureUrl = $secureUrl;
        return $this;
    }

    /**
     * Returns the appointmentMethod property value
     *
     * @return int
     */
    public function getAppointmentMethod()
    {
        return $this->appointmentMethod;
    }

    /**
     * Sets the appointmentMethod property value
     * @param int $appointmentMethod
     *
     * @return $this
     */
    public function setAppointmentMethod($appointmentMethod)
    {
        $this->appointmentMethod = $appointmentMethod;
        return $this;
    }

    /**
     * Returns the databaseName property value
     *
     * @return string
     */
    public function getDatabaseName()
    {
        return $this->databaseName;
    }

    /**
     * Sets the databaseName property value
     * @param string $databaseName
     *
     * @return $this
     */
    public function setDatabaseName($databaseName)
    {
        $this->databaseName = $databaseName;
        return $this;
    }

    /**
     * Returns the whenAdded property value
     *
     * @return string
     */
    public function getWhenAdded()
    {
        return $this->whenAdded;
    }

    /**
     * Sets the whenAdded property value
     * @param string $whenAdded
     *
     * @return $this
     */
    public function setWhenAdded($whenAdded)
    {
        $this->whenAdded = $whenAdded;
        return $this;
    }

    /**
     * Returns the setupComplete property value
     *
     * @return int
     */
    public function getSetupComplete()
    {
        return $this->setupComplete;
    }

    /**
     * Sets the setupComplete property value
     * @param int $setupComplete
     *
     * @return $this
     */
    public function setSetupComplete($setupComplete)
    {
        $this->setupComplete = $setupComplete;
        return $this;
    }

    /**
     * Returns the parentId property value
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Sets the parentId property value
     * @param int $parentId
     *
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * Returns the locationName property value
     *
     * @return string
     */
    public function getLocationName()
    {
        return $this->locationName;
    }

    /**
     * Sets the locationName property value
     * @param string $locationName
     *
     * @return $this
     */
    public function setLocationName($locationName)
    {
        $this->locationName = $locationName;
        return $this;
    }

    /**
     * Returns the shareParentPreferences property value
     *
     * @return int
     */
    public function getShareParentPreferences()
    {
        return $this->shareParentPreferences;
    }

    /**
     * Sets the shareParentPreferences property value
     * @param int $shareParentPreferences
     *
     * @return $this
     */
    public function setShareParentPreferences($shareParentPreferences)
    {
        $this->shareParentPreferences = $shareParentPreferences;
        return $this;
    }

    /**
     * Returns the resellerId property value
     *
     * @return int
     */
    public function getResellerId()
    {
        return $this->resellerId;
    }

    /**
     * Sets the resellerId property value
     * @param int $resellerId
     *
     * @return $this
     */
    public function setResellerId($resellerId)
    {
        $this->resellerId = $resellerId;
        return $this;
    }

    /**
     * Returns the directions property value
     *
     * @return string
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * Sets the directions property value
     * @param string $directions
     *
     * @return $this
     */
    public function setDirections($directions)
    {
        $this->directions = $directions;
        return $this;
    }

    /**
     * Returns the locationNameAbbreviation property value
     *
     * @return string
     */
    public function getLocationNameAbbreviation()
    {
        return $this->locationNameAbbreviation;
    }

    /**
     * Sets the locationNameAbbreviation property value
     * @param string $locationNameAbbreviation
     *
     * @return $this
     */
    public function setLocationNameAbbreviation($locationNameAbbreviation)
    {
        $this->locationNameAbbreviation = $locationNameAbbreviation;
        return $this;
    }

    /**
     * Returns the clientBillingStatusId property value
     *
     * @return int
     */
    public function getClientBillingStatusId()
    {
        return $this->clientBillingStatusId;
    }

    /**
     * Sets the clientBillingStatusId property value
     * @param int $clientBillingStatusId
     *
     * @return $this
     */
    public function setClientBillingStatusId($clientBillingStatusId)
    {
        $this->clientBillingStatusId = $clientBillingStatusId;
        return $this;
    }

    /**
     * Returns the salesManagerId property value
     *
     * @return int
     */
    public function getSalesManagerId()
    {
        return $this->salesManagerId;
    }

    /**
     * Sets the salesManagerId property value
     * @param int $salesManagerId
     *
     * @return $this
     */
    public function setSalesManagerId($salesManagerId)
    {
        $this->salesManagerId = $salesManagerId;
        return $this;
    }

    /**
     * Returns the salesPersonId property value
     *
     * @return int
     */
    public function getSalesPersonId()
    {
        return $this->salesPersonId;
    }

    /**
     * Sets the salesPersonId property value
     * @param int $salesPersonId
     *
     * @return $this
     */
    public function setSalesPersonId($salesPersonId)
    {
        $this->salesPersonId = $salesPersonId;
        return $this;
    }

    /**
     * Returns the territoryId property value
     *
     * @return int
     */
    public function getTerritoryId()
    {
        return $this->territoryId;
    }

    /**
     * Sets the territoryId property value
     * @param int $territoryId
     *
     * @return $this
     */
    public function setTerritoryId($territoryId)
    {
        $this->territoryId = $territoryId;
        return $this;
    }

    /**
     * Returns the internalSalesPersonId property value
     *
     * @return int
     */
    public function getInternalSalesPersonId()
    {
        return $this->internalSalesPersonId;
    }

    /**
     * Sets the internalSalesPersonId property value
     * @param int $internalSalesPersonId
     *
     * @return $this
     */
    public function setInternalSalesPersonId($internalSalesPersonId)
    {
        $this->internalSalesPersonId = $internalSalesPersonId;
        return $this;
    }

    /**
     * Returns the sendWelcomeEmails property value
     *
     * @return int
     */
    public function getSendWelcomeEmails()
    {
        return $this->sendWelcomeEmails;
    }

    /**
     * Sets the sendWelcomeEmails property value
     * @param int $sendWelcomeEmails
     *
     * @return $this
     */
    public function setSendWelcomeEmails($sendWelcomeEmails)
    {
        $this->sendWelcomeEmails = $sendWelcomeEmails;
        return $this;
    }

    /**
     * Returns the sendEndOfTrialEmails property value
     *
     * @return int
     */
    public function getSendEndOfTrialEmails()
    {
        return $this->sendEndOfTrialEmails;
    }

    /**
     * Sets the sendEndOfTrialEmails property value
     * @param int $sendEndOfTrialEmails
     *
     * @return $this
     */
    public function setSendEndOfTrialEmails($sendEndOfTrialEmails)
    {
        $this->sendEndOfTrialEmails = $sendEndOfTrialEmails;
        return $this;
    }

    /**
     * Returns the resellerNotes property value
     *
     * @return string
     */
    public function getResellerNotes()
    {
        return $this->resellerNotes;
    }

    /**
     * Sets the resellerNotes property value
     * @param string $resellerNotes
     *
     * @return $this
     */
    public function setResellerNotes($resellerNotes)
    {
        $this->resellerNotes = $resellerNotes;
        return $this;
    }

    /**
     * Returns the showWelcomeBanner property value
     *
     * @return int
     */
    public function getShowWelcomeBanner()
    {
        return $this->showWelcomeBanner;
    }

    /**
     * Sets the showWelcomeBanner property value
     * @param int $showWelcomeBanner
     *
     * @return $this
     */
    public function setShowWelcomeBanner($showWelcomeBanner)
    {
        $this->showWelcomeBanner = $showWelcomeBanner;
        return $this;
    }

    /**
     * Returns the tierId property value
     *
     * @return int
     */
    public function getTierId()
    {
        return $this->tierId;
    }

    /**
     * Sets the tierId property value
     * @param int $tierId
     *
     * @return $this
     */
    public function setTierId($tierId)
    {
        $this->tierId = $tierId;
        return $this;
    }

    /**
     * Returns the brandId property value
     *
     * @return int
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * Sets the brandId property value
     * @param int $brandId
     *
     * @return $this
     */
    public function setBrandId($brandId)
    {
        $this->brandId = $brandId;
        return $this;
    }

    /**
     * Returns the timeZoneId property value
     *
     * @return int
     */
    public function getTimeZoneId()
    {
        return $this->timeZoneId;
    }

    /**
     * Sets the timeZoneId property value
     * @param int $timeZoneId
     *
     * @return $this
     */
    public function setTimeZoneId($timeZoneId)
    {
        $this->timeZoneId = $timeZoneId;
        return $this;
    }

    /**
     * Returns the referrerDomain property value
     *
     * @return string
     */
    public function getReferrerDomain()
    {
        return $this->referrerDomain;
    }

    /**
     * Sets the referrerDomain property value
     * @param string $referrerDomain
     *
     * @return $this
     */
    public function setReferrerDomain($referrerDomain)
    {
        $this->referrerDomain = $referrerDomain;
        return $this;
    }

    /**
     * Returns the referrerKeyword property value
     *
     * @return string
     */
    public function getReferrerKeyword()
    {
        return $this->referrerKeyword;
    }

    /**
     * Sets the referrerKeyword property value
     * @param string $referrerKeyword
     *
     * @return $this
     */
    public function setReferrerKeyword($referrerKeyword)
    {
        $this->referrerKeyword = $referrerKeyword;
        return $this;
    }

    /**
     * Returns the accountNumber property value
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Sets the accountNumber property value
     * @param string $accountNumber
     *
     * @return $this
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    /**
     * Returns the payPerClickCampaign property value
     *
     * @return string
     */
    public function getPayPerClickCampaign()
    {
        return $this->payPerClickCampaign;
    }

    /**
     * Sets the payPerClickCampaign property value
     * @param string $payPerClickCampaign
     *
     * @return $this
     */
    public function setPayPerClickCampaign($payPerClickCampaign)
    {
        $this->payPerClickCampaign = $payPerClickCampaign;
        return $this;
    }

    /**
     * Returns the whenUpdated property value
     *
     * @return string
     */
    public function getWhenUpdated()
    {
        return $this->whenUpdated;
    }

    /**
     * Sets the whenUpdated property value
     * @param string $whenUpdated
     *
     * @return $this
     */
    public function setWhenUpdated($whenUpdated)
    {
        $this->whenUpdated = $whenUpdated;
        return $this;
    }

    /**
     * Returns the clientSizeId property value
     *
     * @return int
     */
    public function getClientSizeId()
    {
        return $this->clientSizeId;
    }

    /**
     * Sets the clientSizeId property value
     * @param int $clientSizeId
     *
     * @return $this
     */
    public function setClientSizeId($clientSizeId)
    {
        $this->clientSizeId = $clientSizeId;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the clientSchedule property value
     *
     * @return ClientSchedule
     */
    public function getClientSchedule()
    {
        return $this->clientSchedule;
    }

    /**
     * Sets the clientSchedule property value
     * @param ClientSchedule $clientSchedule
     *
     * @return $this
     */
    public function setClientSchedule(ClientSchedule $clientSchedule)
    {
        $this->clientSchedule = $clientSchedule;
        return $this;
    }
}