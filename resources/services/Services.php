<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:13 AM
 */

namespace apptlibrary\resources\services;

use \Exception;

/**
 * Class Services
 * @package apptlibrary\resources\services
 */
class Services
{
    /**
     * The ID for the record.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The client ID for the record.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The title of the service.
     * @access protected
     * @var string
     */
    protected $title;
    /**
     * The description of the service.
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * Default duration of the service in minutes (e.g., 60).
     * @access protected
     * @var integer
     */
    protected $duration;
    /**
     * Default cost of server.
     * @access protected
     * @var float
     */
    protected $cost;
    /**
     * The type ID of the service.
     * @access protected
     * @var integer
     */
    protected $typeId;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * Flag for specifing whether or not to show in customer view.
     * @access protected
     * @var boolean
     */
    protected $displayToCustomer;
    /**
     * Number of spots required for service.
     * @access protected
     * @var integer
     */
    protected $numberOfSpots;
    /**
     * The product code for service.
     * @access protected
     * @var string
     */
    protected $productCode;
    /**
     * Specifies whether or not the service is taxable.
     * @access protected
     * @var boolean
     */
    protected $taxable;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $productCategoryId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var boolean
     */
    protected $calculateServiceSpots;
    /**
     * The list of ServiceScheduleObjects.
     * @access protected
     * @var array
     */
    protected $serviceScheduleObjects;
    /**
     * The list of assigned service schedule IDs.
     * @access protected
     * @var array
     */
    protected $assignedServiceIds;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the title property value
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title property value
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the duration property value
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Sets the duration property value
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * Returns the cost property value
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Sets the cost property value
     * @param float $cost
     *
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }

    /**
     * Returns the typeId property value
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Sets the typeId property value
     * @param int $typeId
     *
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the displayToCustomer property value
     *
     * @return boolean
     */
    public function getDisplayToCustomer()
    {
        return $this->displayToCustomer;
    }

    /**
     * Sets the displayToCustomer property value
     * @param boolean $displayToCustomer
     *
     * @return $this
     */
    public function setDisplayToCustomer($displayToCustomer)
    {
        $this->displayToCustomer = $displayToCustomer;
        return $this;
    }

    /**
     * Returns the numberOfSpots property value
     *
     * @return int
     */
    public function getNumberOfSpots()
    {
        return $this->numberOfSpots;
    }

    /**
     * Sets the numberOfSpots property value
     * @param int $numberOfSpots
     *
     * @return $this
     */
    public function setNumberOfSpots($numberOfSpots)
    {
        $this->numberOfSpots = $numberOfSpots;
        return $this;
    }

    /**
     * Returns the productCode property value
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Sets the productCode property value
     * @param string $productCode
     *
     * @return $this
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
        return $this;
    }

    /**
     * Returns the taxable property value
     *
     * @return boolean
     */
    public function getTaxable()
    {
        return $this->taxable;
    }

    /**
     * Sets the taxable property value
     * @param boolean $taxable
     *
     * @return $this
     */
    public function setTaxable($taxable)
    {
        $this->taxable = $taxable;
        return $this;
    }

    /**
     * Returns the productCategoryId property value
     *
     * @return int
     */
    public function getProductCategoryId()
    {
        return $this->productCategoryId;
    }

    /**
     * Sets the productCategoryId property value
     * @param int $productCategoryId
     *
     * @return $this
     */
    public function setProductCategoryId($productCategoryId)
    {
        $this->productCategoryId = $productCategoryId;
        return $this;
    }

    /**
     * Returns the calculateServiceSpots property value
     *
     * @return boolean
     */
    public function getCalculateServiceSpots()
    {
        return $this->calculateServiceSpots;
    }

    /**
     * Sets the calculateServiceSpots property value
     * @param boolean $calculateServiceSpots
     *
     * @return $this
     */
    public function setCalculateServiceSpots($calculateServiceSpots)
    {
        $this->calculateServiceSpots = $calculateServiceSpots;
        return $this;
    }

    /**
     * Returns the serviceScheduleObjects property value
     *
     * @return array
     */
    public function getServiceScheduleObjects()
    {
        return $this->serviceScheduleObjects;
    }

    /**
     * Sets the serviceScheduleObjects property value
     * @param array $serviceScheduleObjects
     *
     * @return $this
     * @throws Exception
     */
    public function setServiceScheduleObjects(array $serviceScheduleObjects)
    {
        if (!empty($serviceScheduleObjects))
        {
            $aux = 0;
            foreach ($serviceScheduleObjects as $serviceScheduleObject)
            {
                if ($serviceScheduleObject instanceof ServiceSchedules)
                {
                    $this->addServiceScheduleObjects($serviceScheduleObject);
                }
                else
                {
                    throw new Exception('Expecting array element ' . $aux . ' to be an instance of ServiceSchedules');
                }
                $aux++;
            }
        }
        return $this;
    }

    /**
     * Adds a new element to the array of employee service schedules
     * @param ServiceSchedules $serviceSchedules
     * @return $this
     */
    public function addServiceScheduleObjects(ServiceSchedules $serviceSchedules)
    {
        $this->serviceScheduleObjects[] = $serviceSchedules;
        return $this;
    }

    /**
     * Returns the assignedServiceIds property value
     *
     * @return array
     */
    public function getAssignedServiceIds()
    {
        return $this->assignedServiceIds;
    }

    /**
     * Sets the assignedServiceIds property value
     * @param array $assignedServiceIds
     *
     * @return $this
     */
    public function setAssignedServiceIds(array $assignedServiceIds)
    {
        $this->assignedServiceIds = $assignedServiceIds;
        return $this;
    }
}