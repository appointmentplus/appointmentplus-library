<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:11 AM
 */

namespace apptlibrary\resources\services;

/**
 * Class ServiceRelationships
 * @package apptlibrary\resources\services
 */
class ServiceRelationships
{
    /**
     * The ID for the service relationship record.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The service ID for the record.
     * @access protected
     * @var integer
     */
    protected $serviceId;
    /**
     * The assigned to service record.
     * @access protected
     * @var integer
     */
    protected $subServiceId;
    /**
     * The primary service object.
     * @access protected
     * @var Services
     */
    protected $serviceObject;
    /**
     * The assigned to service object.
     * @access protected
     * @var Services
     */
    protected $subServiceObject;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the serviceId property value
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Sets the serviceId property value
     * @param int $serviceId
     *
     * @return $this
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    /**
     * Returns the subServiceId property value
     *
     * @return int
     */
    public function getSubServiceId()
    {
        return $this->subServiceId;
    }

    /**
     * Sets the subServiceId property value
     * @param int $subServiceId
     *
     * @return $this
     */
    public function setSubServiceId($subServiceId)
    {
        $this->subServiceId = $subServiceId;
        return $this;
    }

    /**
     * Returns the serviceObject property value
     *
     * @return Services
     */
    public function getServiceObject()
    {
        return $this->serviceObject;
    }

    /**
     * Sets the serviceObject property value
     * @param Services $serviceObject
     *
     * @return $this
     */
    public function setServiceObject(Services $serviceObject)
    {
        $this->serviceObject = $serviceObject;
        return $this;
    }

    /**
     * Returns the subServiceObject property value
     *
     * @return Services
     */
    public function getSubServiceObject()
    {
        return $this->subServiceObject;
    }

    /**
     * Sets the subServiceObject property value
     * @param Services $subServiceObject
     *
     * @return $this
     */
    public function setSubServiceObject(Services $subServiceObject)
    {
        $this->subServiceObject = $subServiceObject;
        return $this;
    }
}