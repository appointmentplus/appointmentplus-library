<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:14 AM
 */

namespace apptlibrary\resources\countries;

/**
 * Class Countries
 * @package apptlibrary\resources\countries
 */
class Countries
{
    /**
     * The ID of the country record.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The short name of the country record.
     * @access protected
     * @var string
     */
    protected $shortName;
    /**
     * The long name of the country record.
     * @access protected
     * @var string
     */
    protected $longName;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the shortName property value
     *
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Sets the shortName property value
     * @param string $shortName
     *
     * @return $this
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * Returns the longName property value
     *
     * @return string
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * Sets the longName property value
     * @param string $longName
     *
     * @return $this
     */
    public function setLongName($longName)
    {
        $this->longName = $longName;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }
}
