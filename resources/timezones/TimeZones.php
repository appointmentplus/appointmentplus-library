<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:25 AM
 */

namespace apptlibrary\resources\timezones;

/**
 * Class TimeZones
 * @package apptlibrary\resources\timezones
 */
class TimeZones
{
    /**
     * The ID of the time zone object.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The time zone (e.g, America/Phoenix)
     * @access protected
     * @var string
     */
    protected $timeZone;
    /**
     * The time offset from UTC.
     * @access protected
     * @var string
     */
    protected $offset;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the timeZone property value
     *
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * Sets the timeZone property value
     * @param string $timeZone
     *
     * @return $this
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
        return $this;
    }

    /**
     * Returns the offset property value
     *
     * @return string
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Sets the offset property value
     * @param string $offset
     *
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }
}