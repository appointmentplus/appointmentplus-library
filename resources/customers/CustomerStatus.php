<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:51 AM
 */

namespace apptlibrary\resources\customers;

use \Exception;

/**
 * Class CustomerStatus
 * @package apptlibrary\resources\customers
 */
class CustomerStatus
{
    /**
     * Value for new status type
     * TODO: Confirm description
     */
    const NEW_TYPE = 1;
    /**
     * Value for active status type
     * TODO: Confirm description
     */
    const ACTIVE = 2;
    /**
     * Value for inactive status type
     * TODO: Confirm description
     */
    const INACTIVE = 4;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * 1 = new, 2 = Active, 4 = Inactive
     * @access protected
     * @var integer
     */
    protected $statusType;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the statusType property value
     *
     * @return int
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Sets the statusType property value
     * @param int $statusType
     *
     * @return $this
     * @throws Exception
     */
    public function setStatusType($statusType)
    {
        if (in_array($statusType, array(self::ACTIVE, self::INACTIVE, self::NEW_TYPE)))
        {
            $this->statusType = $statusType;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::ACTIVE, self::INACTIVE or self::NEW_TYPE');
        }
        return $this;
    }
}