<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:59 AM
 */

namespace apptlibrary\resources\customers;

/**
 * Class CustomerTypes
 * @package apptlibrary\resources\customers
 */
class CustomerTypes
{
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }
}