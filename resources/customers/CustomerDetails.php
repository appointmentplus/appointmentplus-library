<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:19 AM
 */

namespace apptlibrary\resources\customers;

/**
 * Class CustomerDetails
 * @package apptlibrary\resources\customers
 */
class CustomerDetails
{
    /**
     * The ID of the customer detail.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The first name of the customer.
     * @access protected
     * @var string
     */
    protected $firstName;
    /**
     * The middle name of the customer.
     * @access protected
     * @var string
     */
    protected $middleName;
    /**
     * The last name of the customer.
     * @access protected
     * @var string
     */
    protected $lastName;
    /**
     * The date/time the customer signed up.
     * @access protected
     * @var string
     */
    protected $signupDate;
    /**
     * The ID of the client for the customer.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $locationId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $employeeId;
    /**
     * The parent customer ID for the customer.
     * @access protected
     * @var integer
     */
    protected $parentId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $address1;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $address2;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $city;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $state;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $postalCode;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $daytimePhone;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $nighttimePhone;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $mobilePhone;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $fax;
    /**
     * The email address of the customer.
     * @access protected
     * @var string
     */
    protected $email;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherAddress1;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherAddress2;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherCity;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherState;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherPostalCode;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherPhone1;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $otherPhone2;
    /**
     * The login for customer view.
     * @access protected
     * @var string
     */
    protected $login;
    /**
     * The password for customer view.
     * @access protected
     * @var string
     */
    protected $password;
    /**
     * The temporary password for customer view.
     * @access protected
     * @var string
     */
    protected $temporaryPassword;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $customerTypeId;
    /**
     * Flag to specify okay to contact customer.
     * @access protected
     * @var boolean
     */
    protected $okayToContact;
    /**
     * Flag to specify okay to call customer.
     * @access protected
     * @var boolean
     */
    protected $okayToCall;
    /**
     * Flag to specify okay to email customer.
     * @access protected
     * @var boolean
     */
    protected $okayToEmail;
    /**
     * Flag to specify okay to mail customer.
     * @access protected
     * @var boolean
     */
    protected $okayToMail;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $paymentTypeId;
    /**
     * Last four digits on credit card.
     * @access protected
     * @var integer
     */
    protected $lastFourOnCard;
    /**
     * Name listed on credit card.
     * @access protected
     * @var string
     */
    protected $nameOnCard;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $merchantToken;
    /**
     * The ID of the employee who last updated customer record.
     * @access protected
     * @var integer
     */
    protected $lastUpdateEmployeeId;
    /**
     * The date/time customer record was updated.
     * @access protected
     * @var string
     */
    protected $lastUpdateTimestamp;
    /**
     * The ID of the lead type. See lead types
     * @access protected
     * @var integer
     */
    protected $leadTypeId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $assignedToEmployeeId;
    /**
     * The customer's employer.
     * @access protected
     * @var string
     */
    protected $employer;
    /**
     * The customer's occupation.
     * @access protected
     * @var string
     */
    protected $occupation;
    /**
     * The customer's birthdate.
     * @access protected
     * @var string
     */
    protected $birthDate;
    /**
     * The customer's gender.
     * @access protected
     * @var string
     */
    protected $gender;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $customerStatusId;
    /**
     * Specify's whether the customer is allowed to login via customer view.
     * @access protected
     * @var boolean
     */
    protected $allowLogin;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $dropDownTypeId1;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $dropDownTypeId2;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $dropDownTypeId3;
    /**
     * The account number for the customer.
     * @access protected
     * @var string
     */
    protected $accountNumber;
    /**
     * The customer's needs and requests.
     * @access protected
     * @var string
     */
    protected $specialNeeds;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $prefix;
    /**
     * Primary insurance information for the customer.
     * @access protected
     * @var string
     */
    protected $primaryInsurance;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $primaryInsured;
    /**
     * Secondary insurance information for the customer.
     * @access protected
     * @var string
     */
    protected $secondaryInsurance;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $insuranceType;
    /**
     * Insurance plan number for the customer.
     * @access protected
     * @var string
     */
    protected $insurancePlanNumber;
    /**
     * Insurance group number for the customer.
     * @access protected
     * @var string
     */
    protected $insuranceGroupNumber;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $ssn;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $referredBy;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $internalAlertMessage;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $mobileCarrierId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $timeZoneId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $loginChangeDate;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $passwordChangeDate;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $updateInformationPromptDate;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the firstName property value
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName property value
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Returns the middleName property value
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Sets the middleName property value
     * @param string $middleName
     *
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * Returns the lastName property value
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName property value
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Returns the signupDate property value
     *
     * @return string
     */
    public function getSignupDate()
    {
        return $this->signupDate;
    }

    /**
     * Sets the signupDate property value
     * @param string $signupDate
     *
     * @return $this
     */
    public function setSignupDate($signupDate)
    {
        $this->signupDate = $signupDate;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the locationId property value
     *
     * @return int
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Sets the locationId property value
     * @param int $locationId
     *
     * @return $this
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * Returns the employeeId property value
     *
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Sets the employeeId property value
     * @param int $employeeId
     *
     * @return $this
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Returns the parentId property value
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Sets the parentId property value
     * @param int $parentId
     *
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * Returns the address1 property value
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Sets the address1 property value
     * @param string $address1
     *
     * @return $this
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;
        return $this;
    }

    /**
     * Returns the address2 property value
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Sets the address2 property value
     * @param string $address2
     *
     * @return $this
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;
        return $this;
    }

    /**
     * Returns the city property value
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city property value
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Returns the state property value
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets the state property value
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Returns the postalCode property value
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Sets the postalCode property value
     * @param string $postalCode
     *
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * Returns the daytimePhone property value
     *
     * @return string
     */
    public function getDaytimePhone()
    {
        return $this->daytimePhone;
    }

    /**
     * Sets the daytimePhone property value
     * @param string $daytimePhone
     *
     * @return $this
     */
    public function setDaytimePhone($daytimePhone)
    {
        $this->daytimePhone = $daytimePhone;
        return $this;
    }

    /**
     * Returns the nighttimePhone property value
     *
     * @return string
     */
    public function getNighttimePhone()
    {
        return $this->nighttimePhone;
    }

    /**
     * Sets the nighttimePhone property value
     * @param string $nighttimePhone
     *
     * @return $this
     */
    public function setNighttimePhone($nighttimePhone)
    {
        $this->nighttimePhone = $nighttimePhone;
        return $this;
    }

    /**
     * Returns the mobilePhone property value
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Sets the mobilePhone property value
     * @param string $mobilePhone
     *
     * @return $this
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
        return $this;
    }

    /**
     * Returns the fax property value
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax property value
     * @param string $fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Returns the email property value
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email property value
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Returns the otherAddress1 property value
     *
     * @return string
     */
    public function getOtherAddress1()
    {
        return $this->otherAddress1;
    }

    /**
     * Sets the otherAddress1 property value
     * @param string $otherAddress1
     *
     * @return $this
     */
    public function setOtherAddress1($otherAddress1)
    {
        $this->otherAddress1 = $otherAddress1;
        return $this;
    }

    /**
     * Returns the otherAddress2 property value
     *
     * @return string
     */
    public function getOtherAddress2()
    {
        return $this->otherAddress2;
    }

    /**
     * Sets the otherAddress2 property value
     * @param string $otherAddress2
     *
     * @return $this
     */
    public function setOtherAddress2($otherAddress2)
    {
        $this->otherAddress2 = $otherAddress2;
        return $this;
    }

    /**
     * Returns the otherCity property value
     *
     * @return string
     */
    public function getOtherCity()
    {
        return $this->otherCity;
    }

    /**
     * Sets the otherCity property value
     * @param string $otherCity
     *
     * @return $this
     */
    public function setOtherCity($otherCity)
    {
        $this->otherCity = $otherCity;
        return $this;
    }

    /**
     * Returns the otherState property value
     *
     * @return string
     */
    public function getOtherState()
    {
        return $this->otherState;
    }

    /**
     * Sets the otherState property value
     * @param string $otherState
     *
     * @return $this
     */
    public function setOtherState($otherState)
    {
        $this->otherState = $otherState;
        return $this;
    }

    /**
     * Returns the otherPostalCode property value
     *
     * @return string
     */
    public function getOtherPostalCode()
    {
        return $this->otherPostalCode;
    }

    /**
     * Sets the otherPostalCode property value
     * @param string $otherPostalCode
     *
     * @return $this
     */
    public function setOtherPostalCode($otherPostalCode)
    {
        $this->otherPostalCode = $otherPostalCode;
        return $this;
    }

    /**
     * Returns the otherPhone1 property value
     *
     * @return string
     */
    public function getOtherPhone1()
    {
        return $this->otherPhone1;
    }

    /**
     * Sets the otherPhone1 property value
     * @param string $otherPhone1
     *
     * @return $this
     */
    public function setOtherPhone1($otherPhone1)
    {
        $this->otherPhone1 = $otherPhone1;
        return $this;
    }

    /**
     * Returns the otherPhone2 property value
     *
     * @return string
     */
    public function getOtherPhone2()
    {
        return $this->otherPhone2;
    }

    /**
     * Sets the otherPhone2 property value
     * @param string $otherPhone2
     *
     * @return $this
     */
    public function setOtherPhone2($otherPhone2)
    {
        $this->otherPhone2 = $otherPhone2;
        return $this;
    }

    /**
     * Returns the login property value
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Sets the login property value
     * @param string $login
     *
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * Returns the password property value
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the password property value
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Returns the temporaryPassword property value
     *
     * @return string
     */
    public function getTemporaryPassword()
    {
        return $this->temporaryPassword;
    }

    /**
     * Sets the temporaryPassword property value
     * @param string $temporaryPassword
     *
     * @return $this
     */
    public function setTemporaryPassword($temporaryPassword)
    {
        $this->temporaryPassword = $temporaryPassword;
        return $this;
    }

    /**
     * Returns the customerTypeId property value
     *
     * @return int
     */
    public function getCustomerTypeId()
    {
        return $this->customerTypeId;
    }

    /**
     * Sets the customerTypeId property value
     * @param int $customerTypeId
     *
     * @return $this
     */
    public function setCustomerTypeId($customerTypeId)
    {
        $this->customerTypeId = $customerTypeId;
        return $this;
    }

    /**
     * Returns the okayToContact property value
     *
     * @return boolean
     */
    public function getOkayToContact()
    {
        return $this->okayToContact;
    }

    /**
     * Sets the okayToContact property value
     * @param boolean $okayToContact
     *
     * @return $this
     */
    public function setOkayToContact($okayToContact)
    {
        $this->okayToContact = $okayToContact;
        return $this;
    }

    /**
     * Returns the okayToCall property value
     *
     * @return boolean
     */
    public function getOkayToCall()
    {
        return $this->okayToCall;
    }

    /**
     * Sets the okayToCall property value
     * @param boolean $okayToCall
     *
     * @return $this
     */
    public function setOkayToCall($okayToCall)
    {
        $this->okayToCall = $okayToCall;
        return $this;
    }

    /**
     * Returns the okayToEmail property value
     *
     * @return boolean
     */
    public function getOkayToEmail()
    {
        return $this->okayToEmail;
    }

    /**
     * Sets the okayToEmail property value
     * @param boolean $okayToEmail
     *
     * @return $this
     */
    public function setOkayToEmail($okayToEmail)
    {
        $this->okayToEmail = $okayToEmail;
        return $this;
    }

    /**
     * Returns the okayToMail property value
     *
     * @return boolean
     */
    public function getOkayToMail()
    {
        return $this->okayToMail;
    }

    /**
     * Sets the okayToMail property value
     * @param boolean $okayToMail
     *
     * @return $this
     */
    public function setOkayToMail($okayToMail)
    {
        $this->okayToMail = $okayToMail;
        return $this;
    }

    /**
     * Returns the paymentTypeId property value
     *
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * Sets the paymentTypeId property value
     * @param int $paymentTypeId
     *
     * @return $this
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->paymentTypeId = $paymentTypeId;
        return $this;
    }

    /**
     * Returns the lastFourOnCard property value
     *
     * @return int
     */
    public function getLastFourOnCard()
    {
        return $this->lastFourOnCard;
    }

    /**
     * Sets the lastFourOnCard property value
     * @param int $lastFourOnCard
     *
     * @return $this
     */
    public function setLastFourOnCard($lastFourOnCard)
    {
        $this->lastFourOnCard = $lastFourOnCard;
        return $this;
    }

    /**
     * Returns the nameOnCard property value
     *
     * @return string
     */
    public function getNameOnCard()
    {
        return $this->nameOnCard;
    }

    /**
     * Sets the nameOnCard property value
     * @param string $nameOnCard
     *
     * @return $this
     */
    public function setNameOnCard($nameOnCard)
    {
        $this->nameOnCard = $nameOnCard;
        return $this;
    }

    /**
     * Returns the merchantToken property value
     *
     * @return string
     */
    public function getMerchantToken()
    {
        return $this->merchantToken;
    }

    /**
     * Sets the merchantToken property value
     * @param string $merchantToken
     *
     * @return $this
     */
    public function setMerchantToken($merchantToken)
    {
        $this->merchantToken = $merchantToken;
        return $this;
    }

    /**
     * Returns the lastUpdateEmployeeId property value
     *
     * @return int
     */
    public function getLastUpdateEmployeeId()
    {
        return $this->lastUpdateEmployeeId;
    }

    /**
     * Sets the lastUpdateEmployeeId property value
     * @param int $lastUpdateEmployeeId
     *
     * @return $this
     */
    public function setLastUpdateEmployeeId($lastUpdateEmployeeId)
    {
        $this->lastUpdateEmployeeId = $lastUpdateEmployeeId;
        return $this;
    }

    /**
     * Returns the lastUpdateTimestamp property value
     *
     * @return string
     */
    public function getLastUpdateTimestamp()
    {
        return $this->lastUpdateTimestamp;
    }

    /**
     * Sets the lastUpdateTimestamp property value
     * @param string $lastUpdateTimestamp
     *
     * @return $this
     */
    public function setLastUpdateTimestamp($lastUpdateTimestamp)
    {
        $this->lastUpdateTimestamp = $lastUpdateTimestamp;
        return $this;
    }

    /**
     * Returns the leadTypeId property value
     *
     * @return int
     */
    public function getLeadTypeId()
    {
        return $this->leadTypeId;
    }

    /**
     * Sets the leadTypeId property value
     * @param int $leadTypeId
     *
     * @return $this
     */
    public function setLeadTypeId($leadTypeId)
    {
        $this->leadTypeId = $leadTypeId;
        return $this;
    }

    /**
     * Returns the assignedToEmployeeId property value
     *
     * @return int
     */
    public function getAssignedToEmployeeId()
    {
        return $this->assignedToEmployeeId;
    }

    /**
     * Sets the assignedToEmployeeId property value
     * @param int $assignedToEmployeeId
     *
     * @return $this
     */
    public function setAssignedToEmployeeId($assignedToEmployeeId)
    {
        $this->assignedToEmployeeId = $assignedToEmployeeId;
        return $this;
    }

    /**
     * Returns the employer property value
     *
     * @return string
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * Sets the employer property value
     * @param string $employer
     *
     * @return $this
     */
    public function setEmployer($employer)
    {
        $this->employer = $employer;
        return $this;
    }

    /**
     * Returns the occupation property value
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Sets the occupation property value
     * @param string $occupation
     *
     * @return $this
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
        return $this;
    }

    /**
     * Returns the birthDate property value
     *
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Sets the birthDate property value
     * @param string $birthDate
     *
     * @return $this
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * Returns the gender property value
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender property value
     * @param string $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Returns the customerStatusId property value
     *
     * @return int
     */
    public function getCustomerStatusId()
    {
        return $this->customerStatusId;
    }

    /**
     * Sets the customerStatusId property value
     * @param int $customerStatusId
     *
     * @return $this
     */
    public function setCustomerStatusId($customerStatusId)
    {
        $this->customerStatusId = $customerStatusId;
        return $this;
    }

    /**
     * Returns the allowLogin property value
     *
     * @return boolean
     */
    public function getAllowLogin()
    {
        return $this->allowLogin;
    }

    /**
     * Sets the allowLogin property value
     * @param boolean $allowLogin
     *
     * @return $this
     */
    public function setAllowLogin($allowLogin)
    {
        $this->allowLogin = $allowLogin;
        return $this;
    }

    /**
     * Returns the dropDownTypeId1 property value
     *
     * @return int
     */
    public function getDropDownTypeId1()
    {
        return $this->dropDownTypeId1;
    }

    /**
     * Sets the dropDownTypeId1 property value
     * @param int $dropDownTypeId1
     *
     * @return $this
     */
    public function setDropDownTypeId1($dropDownTypeId1)
    {
        $this->dropDownTypeId1 = $dropDownTypeId1;
        return $this;
    }

    /**
     * Returns the dropDownTypeId2 property value
     *
     * @return int
     */
    public function getDropDownTypeId2()
    {
        return $this->dropDownTypeId2;
    }

    /**
     * Sets the dropDownTypeId2 property value
     * @param int $dropDownTypeId2
     *
     * @return $this
     */
    public function setDropDownTypeId2($dropDownTypeId2)
    {
        $this->dropDownTypeId2 = $dropDownTypeId2;
        return $this;
    }

    /**
     * Returns the dropDownTypeId3 property value
     *
     * @return int
     */
    public function getDropDownTypeId3()
    {
        return $this->dropDownTypeId3;
    }

    /**
     * Sets the dropDownTypeId3 property value
     * @param int $dropDownTypeId3
     *
     * @return $this
     */
    public function setDropDownTypeId3($dropDownTypeId3)
    {
        $this->dropDownTypeId3 = $dropDownTypeId3;
        return $this;
    }

    /**
     * Returns the accountNumber property value
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Sets the accountNumber property value
     * @param string $accountNumber
     *
     * @return $this
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    /**
     * Returns the specialNeeds property value
     *
     * @return string
     */
    public function getSpecialNeeds()
    {
        return $this->specialNeeds;
    }

    /**
     * Sets the specialNeeds property value
     * @param string $specialNeeds
     *
     * @return $this
     */
    public function setSpecialNeeds($specialNeeds)
    {
        $this->specialNeeds = $specialNeeds;
        return $this;
    }

    /**
     * Returns the prefix property value
     *
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Sets the prefix property value
     * @param string $prefix
     *
     * @return $this
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * Returns the primaryInsurance property value
     *
     * @return string
     */
    public function getPrimaryInsurance()
    {
        return $this->primaryInsurance;
    }

    /**
     * Sets the primaryInsurance property value
     * @param string $primaryInsurance
     *
     * @return $this
     */
    public function setPrimaryInsurance($primaryInsurance)
    {
        $this->primaryInsurance = $primaryInsurance;
        return $this;
    }

    /**
     * Returns the primaryInsured property value
     *
     * @return string
     */
    public function getPrimaryInsured()
    {
        return $this->primaryInsured;
    }

    /**
     * Sets the primaryInsured property value
     * @param string $primaryInsured
     *
     * @return $this
     */
    public function setPrimaryInsured($primaryInsured)
    {
        $this->primaryInsured = $primaryInsured;
        return $this;
    }

    /**
     * Returns the secondaryInsurance property value
     *
     * @return string
     */
    public function getSecondaryInsurance()
    {
        return $this->secondaryInsurance;
    }

    /**
     * Sets the secondaryInsurance property value
     * @param string $secondaryInsurance
     *
     * @return $this
     */
    public function setSecondaryInsurance($secondaryInsurance)
    {
        $this->secondaryInsurance = $secondaryInsurance;
        return $this;
    }

    /**
     * Returns the insuranceType property value
     *
     * @return string
     */
    public function getInsuranceType()
    {
        return $this->insuranceType;
    }

    /**
     * Sets the insuranceType property value
     * @param string $insuranceType
     *
     * @return $this
     */
    public function setInsuranceType($insuranceType)
    {
        $this->insuranceType = $insuranceType;
        return $this;
    }

    /**
     * Returns the insurancePlanNumber property value
     *
     * @return string
     */
    public function getInsurancePlanNumber()
    {
        return $this->insurancePlanNumber;
    }

    /**
     * Sets the insurancePlanNumber property value
     * @param string $insurancePlanNumber
     *
     * @return $this
     */
    public function setInsurancePlanNumber($insurancePlanNumber)
    {
        $this->insurancePlanNumber = $insurancePlanNumber;
        return $this;
    }

    /**
     * Returns the insuranceGroupNumber property value
     *
     * @return string
     */
    public function getInsuranceGroupNumber()
    {
        return $this->insuranceGroupNumber;
    }

    /**
     * Sets the insuranceGroupNumber property value
     * @param string $insuranceGroupNumber
     *
     * @return $this
     */
    public function setInsuranceGroupNumber($insuranceGroupNumber)
    {
        $this->insuranceGroupNumber = $insuranceGroupNumber;
        return $this;
    }

    /**
     * Returns the ssn property value
     *
     * @return string
     */
    public function getSsn()
    {
        return $this->ssn;
    }

    /**
     * Sets the ssn property value
     * @param string $ssn
     *
     * @return $this
     */
    public function setSsn($ssn)
    {
        $this->ssn = $ssn;
        return $this;
    }

    /**
     * Returns the referredBy property value
     *
     * @return string
     */
    public function getReferredBy()
    {
        return $this->referredBy;
    }

    /**
     * Sets the referredBy property value
     * @param string $referredBy
     *
     * @return $this
     */
    public function setReferredBy($referredBy)
    {
        $this->referredBy = $referredBy;
        return $this;
    }

    /**
     * Returns the internalAlertMessage property value
     *
     * @return string
     */
    public function getInternalAlertMessage()
    {
        return $this->internalAlertMessage;
    }

    /**
     * Sets the internalAlertMessage property value
     * @param string $internalAlertMessage
     *
     * @return $this
     */
    public function setInternalAlertMessage($internalAlertMessage)
    {
        $this->internalAlertMessage = $internalAlertMessage;
        return $this;
    }

    /**
     * Returns the mobileCarrierId property value
     *
     * @return int
     */
    public function getMobileCarrierId()
    {
        return $this->mobileCarrierId;
    }

    /**
     * Sets the mobileCarrierId property value
     * @param int $mobileCarrierId
     *
     * @return $this
     */
    public function setMobileCarrierId($mobileCarrierId)
    {
        $this->mobileCarrierId = $mobileCarrierId;
        return $this;
    }

    /**
     * Returns the timeZoneId property value
     *
     * @return int
     */
    public function getTimeZoneId()
    {
        return $this->timeZoneId;
    }

    /**
     * Sets the timeZoneId property value
     * @param int $timeZoneId
     *
     * @return $this
     */
    public function setTimeZoneId($timeZoneId)
    {
        $this->timeZoneId = $timeZoneId;
        return $this;
    }

    /**
     * Returns the loginChangeDate property value
     *
     * @return string
     */
    public function getLoginChangeDate()
    {
        return $this->loginChangeDate;
    }

    /**
     * Sets the loginChangeDate property value
     * @param string $loginChangeDate
     *
     * @return $this
     */
    public function setLoginChangeDate($loginChangeDate)
    {
        $this->loginChangeDate = $loginChangeDate;
        return $this;
    }

    /**
     * Returns the passwordChangeDate property value
     *
     * @return string
     */
    public function getPasswordChangeDate()
    {
        return $this->passwordChangeDate;
    }

    /**
     * Sets the passwordChangeDate property value
     * @param string $passwordChangeDate
     *
     * @return $this
     */
    public function setPasswordChangeDate($passwordChangeDate)
    {
        $this->passwordChangeDate = $passwordChangeDate;
        return $this;
    }

    /**
     * Returns the updateInformationPromptDate property value
     *
     * @return string
     */
    public function getUpdateInformationPromptDate()
    {
        return $this->updateInformationPromptDate;
    }

    /**
     * Sets the updateInformationPromptDate property value
     * @param string $updateInformationPromptDate
     *
     * @return $this
     */
    public function setUpdateInformationPromptDate($updateInformationPromptDate)
    {
        $this->updateInformationPromptDate = $updateInformationPromptDate;
        return $this;
    }
}