<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:30 AM
 */

namespace apptlibrary\resources\customers;

/**
 * Class CustomerLeadTypes
 * @package apptlibrary\resources\customers
 */
class CustomerLeadTypes
{
    /**
     * The ID of the customer lead type.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The ID of the client for the customer lead type.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The description of the customer lead type.
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }
}