<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:31 AM
 */

namespace apptlibrary\resources\customers;

use \Exception;

/**
 * Class Customers
 * @package apptlibrary\resources\customers
 */
class Customers
{
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $detailType;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var CustomerDetails
     */
    protected $customerDetails;
    /**
     * Child customer objects
     * @access protected
     * @var array
     */
    protected $customers;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the detailType property value
     *
     * @return string
     */
    public function getDetailType()
    {
        return $this->detailType;
    }

    /**
     * Sets the detailType property value
     * @param string $detailType
     *
     * @return $this
     */
    public function setDetailType($detailType)
    {
        $this->detailType = $detailType;
        return $this;
    }

    /**
     * Returns the customerDetails property value
     *
     * @return CustomerDetails
     */
    public function getCustomerDetails()
    {
        return $this->customerDetails;
    }

    /**
     * Sets the customerDetails property value
     * @param CustomerDetails $customerDetails
     *
     * @return $this
     */
    public function setCustomerDetails(CustomerDetails $customerDetails)
    {
        $this->customerDetails = $customerDetails;
        return $this;
    }

    /**
     * Returns the customers property value
     *
     * @return array
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Sets the customers property value
     * @param array $customers
     *
     * @return $this
     * @throws Exception
     */
    public function setCustomers(array $customers)
    {
        if (!empty($customers))
        {
            foreach ($customers as $customer)
            {
                if ($customer instanceof self)
                {
                    $this->addCustomer($customer);
                }
                else
                {
                    throw new Exception('Array passed is not a valid object (Expected a Customer instance)');
                }
            }
        }
        return $this;
    }

    /**
     * Adds a customer to the customers array
     * @param Customers $customer
     * @return $this
     */
    public function addCustomer(Customers $customer)
    {
        $this->customers[] = $customer;
        return $this;
    }
}