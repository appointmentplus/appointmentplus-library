<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:20 AM
 */

namespace apptlibrary\resources\customers;

use \Exception;

/**
 * Class CustomerFieldSet
 * @package apptlibrary\resources\customers
 */
class CustomerFieldSet
{
    /**
     * Value for dropdown field type
     * TODO: Confirm description
     */
    const DROPDOWN = 'dropdown';
    /**
     * Value for textfield field type
     * TODO: Confirm description
     */
    const TEXTFIELD = 'textfield';
    /**
     * Value for password field type
     * TODO: Confirm description
     */
    const PASSWORD = 'password';
    /**
     * Value for yesno field type
     * TODO: Confirm description
     */
    const YESNO = 'yesno';
    /**
     * Value for date field type
     * TODO: Confirm description
     */
    const DATE = 'DATE';
    /**
     * The ID of the customer field set.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The name of the field.
     * @access protected
     * @var string
     */
    protected $fieldName;
    /**
     * The alternative name of the field.
     * @access protected
     * @var string
     */
    protected $alternativeFieldName;
    /**
     * The display name of the field.
     * @access protected
     * @var string
     */
    protected $displayName;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $customerViewRegistration;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $adminCustomerView;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $adminCreateAppointment;
    /**
     * The type of field. (dropdown, textfield, password, yesno, date)
     * @access protected
     * @var string
     */
    protected $fieldType;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the fieldName property value
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Sets the fieldName property value
     * @param string $fieldName
     *
     * @return $this
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * Returns the alternativeFieldName property value
     *
     * @return string
     */
    public function getAlternativeFieldName()
    {
        return $this->alternativeFieldName;
    }

    /**
     * Sets the alternativeFieldName property value
     * @param string $alternativeFieldName
     *
     * @return $this
     */
    public function setAlternativeFieldName($alternativeFieldName)
    {
        $this->alternativeFieldName = $alternativeFieldName;
        return $this;
    }

    /**
     * Returns the displayName property value
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Sets the displayName property value
     * @param string $displayName
     *
     * @return $this
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * Returns the customerViewRegistration property value
     *
     * @return string
     */
    public function getCustomerViewRegistration()
    {
        return $this->customerViewRegistration;
    }

    /**
     * Sets the customerViewRegistration property value
     * @param string $customerViewRegistration
     *
     * @return $this
     */
    public function setCustomerViewRegistration($customerViewRegistration)
    {
        $this->customerViewRegistration = $customerViewRegistration;
        return $this;
    }

    /**
     * Returns the adminCustomerView property value
     *
     * @return string
     */
    public function getAdminCustomerView()
    {
        return $this->adminCustomerView;
    }

    /**
     * Sets the adminCustomerView property value
     * @param string $adminCustomerView
     *
     * @return $this
     */
    public function setAdminCustomerView($adminCustomerView)
    {
        $this->adminCustomerView = $adminCustomerView;
        return $this;
    }

    /**
     * Returns the adminCreateAppointment property value
     *
     * @return string
     */
    public function getAdminCreateAppointment()
    {
        return $this->adminCreateAppointment;
    }

    /**
     * Sets the adminCreateAppointment property value
     * @param string $adminCreateAppointment
     *
     * @return $this
     */
    public function setAdminCreateAppointment($adminCreateAppointment)
    {
        $this->adminCreateAppointment = $adminCreateAppointment;
        return $this;
    }

    /**
     * Returns the fieldType property value
     *
     * @return string
     */
    public function getFieldType()
    {
        return $this->fieldType;
    }

    /**
     * Sets the fieldType property value
     * @param string $fieldType
     *
     * @return $this
     * @throws Exception
     */
    public function setFieldType($fieldType)
    {
        if (in_array($fieldType, array(self::DATE, self::DROPDOWN, self::PASSWORD, self::TEXTFIELD, self::YESNO)))
        {
            $this->fieldType = $fieldType;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::DATE, self::DROPDOWN, self::PASSWORD, self::TEXTFIELD or self::YESNO');
        }
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }
}