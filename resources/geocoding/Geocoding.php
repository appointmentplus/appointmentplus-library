<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 9:17 PM
 */

namespace apptlibrary\resources\geocoding;

/**
 * Class Geocoding
 * @package apptlibrary\resources\geocoding
 */
class Geocoding
{
    /**
     * The ID of the geocoding record.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The search query for the record
     * @access protected
     * @var string
     */
    protected $searchQuery;
    /**
     * The latitude of the request.
     * @access protected
     * @var float
     */
    protected $latitude;
    /**
     * The longitude of the request.
     * @access protected
     * @var float
     */
    protected $longitude;
    /**
     * The dat/time the record was added.
     * @access protected
     * @var string
     */
    protected $whenAdded;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the searchQuery property value
     *
     * @return string
     */
    public function getSearchQuery()
    {
        return $this->searchQuery;
    }

    /**
     * Sets the searchQuery property value
     * @param string $searchQuery
     *
     * @return $this
     */
    public function setSearchQuery($searchQuery)
    {
        $this->searchQuery = $searchQuery;
        return $this;
    }

    /**
     * Returns the latitude property value
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Sets the latitude property value
     * @param float $latitude
     *
     * @return $this
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    /**
     * Returns the longitude property value
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Sets the longitude property value
     * @param float $longitude
     *
     * @return $this
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * Returns the whenAdded property value
     *
     * @return string
     */
    public function getWhenAdded()
    {
        return $this->whenAdded;
    }

    /**
     * Sets the whenAdded property value
     * @param string $whenAdded
     *
     * @return $this
     */
    public function setWhenAdded($whenAdded)
    {
        $this->whenAdded = $whenAdded;
        return $this;
    }
}