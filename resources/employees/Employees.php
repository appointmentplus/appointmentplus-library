<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 2:03 AM
 */

namespace apptlibrary\resources\employees;

use \Exception;

/**
 * Class Employees
 * @package apptlibrary\resources\employees
 */
class Employees
{
    /**
     * Value used to disable displaying to customer
     * TODO: Confirm description
     */
    const NO = 0;
    /**
     * Value used to enable displaying to customer
     * TODO: Confirm description
     */
    const YES = 1;
    /**
     * ID of the employee
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * ID of client
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * the last name of the employee
     * @access protected
     * @var string
     */
    protected $lastName;
    /**
     * the first name of the employee
     * @access protected
     * @var string
     */
    protected $firstName;
    /**
     * the middle name of the employee
     * @access protected
     * @var string
     */
    protected $middleName;
    /**
     * the display name of the employee
     * @access protected
     * @var string
     */
    protected $displayName;
    /**
     * employee's company name
     * @access protected
     * @var string
     */
    protected $company;
    /**
     * bio of the employee
     * @access protected
     * @var string
     */
    protected $bio;
    /**
     * notes for the employee
     * @access protected
     * @var string
     */
    protected $notes;
    /**
     * active, inactive, deleted
     * @access protected
     * @var string
     */
    protected $status;
    /**
     * email address of employee
     * @access protected
     * @var string
     */
    protected $email;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $typeId;
    /**
     * Whether or not to display the employee to customer. (0 = no, 1= yes)
     * @access protected
     * @var integer
     */
    protected $displayToCustomer;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * The employee's home number.
     * @access protected
     * @var string
     */
    protected $homePhone;
    /**
     * The employee's work number.
     * @access protected
     * @var string
     */
    protected $workPhone;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $mobilePhone;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $fax;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $salesPersonId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $taxIdNumber;
    /**
     * The employee's mailing address.
     * @access protected
     * @var string
     */
    protected $address;
    /**
     * The employee's mailing city.
     * @access protected
     * @var string
     */
    protected $city;
    /**
     * The employee's mailing state.
     * @access protected
     * @var string
     */
    protected $state;
    /**
     * The employee's mailing postal code.
     * @access protected
     * @var string
     */
    protected $postalCode;
    /**
     * The employee's mobile carrier ID. See mobile carriers
     * @access protected
     * @var integer
     */
    protected $mobileCarrierId;
    /**
     * The employee's time zone ID. See time zones
     * @access protected
     * @var integer
     */
    protected $timeZoneId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var float
     */
    protected $appointmentLeadTime;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var array
     */
    protected $employeeScheduleObjects;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var array
     */
    protected $employeeServiceScheduleObjects;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the lastName property value
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName property value
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * Returns the firstName property value
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName property value
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * Returns the middleName property value
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Sets the middleName property value
     * @param string $middleName
     *
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * Returns the displayName property value
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Sets the displayName property value
     * @param string $displayName
     *
     * @return $this
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * Returns the company property value
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company property value
     * @param string $company
     *
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * Returns the bio property value
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Sets the bio property value
     * @param string $bio
     *
     * @return $this
     */
    public function setBio($bio)
    {
        $this->bio = $bio;
        return $this;
    }

    /**
     * Returns the notes property value
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Sets the notes property value
     * @param string $notes
     *
     * @return $this
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Returns the status property value
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status property value
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Returns the email property value
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email property value
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Returns the typeId property value
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Sets the typeId property value
     * @param int $typeId
     *
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * Returns the displayToCustomer property value
     *
     * @return int
     */
    public function getDisplayToCustomer()
    {
        return $this->displayToCustomer;
    }

    /**
     * Sets the displayToCustomer property value
     * @param int $displayToCustomer
     *
     * @return $this
     * @throws Exception
     */
    public function setDisplayToCustomer($displayToCustomer)
    {
        if (in_array($displayToCustomer, array(self::NO, self::YES)))
        {
            $this->displayToCustomer = $displayToCustomer;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::NO or self::YES');
        }
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the homePhone property value
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Sets the homePhone property value
     * @param string $homePhone
     *
     * @return $this
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;
        return $this;
    }

    /**
     * Returns the workPhone property value
     *
     * @return string
     */
    public function getWorkPhone()
    {
        return $this->workPhone;
    }

    /**
     * Sets the workPhone property value
     * @param string $workPhone
     *
     * @return $this
     */
    public function setWorkPhone($workPhone)
    {
        $this->workPhone = $workPhone;
        return $this;
    }

    /**
     * Returns the mobilePhone property value
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Sets the mobilePhone property value
     * @param string $mobilePhone
     *
     * @return $this
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
        return $this;
    }

    /**
     * Returns the fax property value
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax property value
     * @param string $fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * Returns the salesPersonId property value
     *
     * @return int
     */
    public function getSalesPersonId()
    {
        return $this->salesPersonId;
    }

    /**
     * Sets the salesPersonId property value
     * @param int $salesPersonId
     *
     * @return $this
     */
    public function setSalesPersonId($salesPersonId)
    {
        $this->salesPersonId = $salesPersonId;
        return $this;
    }

    /**
     * Returns the taxIdNumber property value
     *
     * @return int
     */
    public function getTaxIdNumber()
    {
        return $this->taxIdNumber;
    }

    /**
     * Sets the taxIdNumber property value
     * @param int $taxIdNumber
     *
     * @return $this
     */
    public function setTaxIdNumber($taxIdNumber)
    {
        $this->taxIdNumber = $taxIdNumber;
        return $this;
    }

    /**
     * Returns the address property value
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address property value
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Returns the city property value
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city property value
     * @param string $city
     *
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * Returns the state property value
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets the state property value
     * @param string $state
     *
     * @return $this
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * Returns the postalCode property value
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Sets the postalCode property value
     * @param string $postalCode
     *
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * Returns the mobileCarrierId property value
     *
     * @return int
     */
    public function getMobileCarrierId()
    {
        return $this->mobileCarrierId;
    }

    /**
     * Sets the mobileCarrierId property value
     * @param int $mobileCarrierId
     *
     * @return $this
     */
    public function setMobileCarrierId($mobileCarrierId)
    {
        $this->mobileCarrierId = $mobileCarrierId;
        return $this;
    }

    /**
     * Returns the timeZoneId property value
     *
     * @return int
     */
    public function getTimeZoneId()
    {
        return $this->timeZoneId;
    }

    /**
     * Sets the timeZoneId property value
     * @param int $timeZoneId
     *
     * @return $this
     */
    public function setTimeZoneId($timeZoneId)
    {
        $this->timeZoneId = $timeZoneId;
        return $this;
    }

    /**
     * Returns the appointmentLeadTime property value
     *
     * @return float
     */
    public function getAppointmentLeadTime()
    {
        return $this->appointmentLeadTime;
    }

    /**
     * Sets the appointmentLeadTime property value
     * @param float $appointmentLeadTime
     *
     * @return $this
     */
    public function setAppointmentLeadTime($appointmentLeadTime)
    {
        $this->appointmentLeadTime = $appointmentLeadTime;
        return $this;
    }

    /**
     * Returns the employeeScheduleObjects property value
     *
     * @return array
     */
    public function getEmployeeScheduleObjects()
    {
        return $this->employeeScheduleObjects;
    }

    /**
     * Sets the employeeScheduleObjects property value
     * @param array $employeeScheduleObjects
     *
     * @return $this
     * @throws Exception
     */
    public function setEmployeeScheduleObjects(array $employeeScheduleObjects)
    {
        if (!empty($employeeScheduleObjects))
        {
            $aux = 0;
            foreach ($employeeScheduleObjects as $employeeScheduleObject)
            {
                if ($employeeScheduleObject instanceof EmployeeSchedules)
                {
                    $this->addEmployeeScheduleObject($employeeScheduleObject);
                }
                else
                {
                    throw new Exception('Expecting array element ' . $aux . ' to be an instance of EmployeeSchedules');
                }
                $aux++;
            }
        }
        return $this;
    }

    /**
     * Adds a new element to the array of employee schedules
     * @param EmployeeSchedules $employeeSchedules
     * @return $this
     */
    public function addEmployeeScheduleObject(EmployeeSchedules $employeeSchedules)
    {
        $this->employeeScheduleObjects[] = $employeeSchedules;
        return $this;
    }

    /**
     * Returns the employeeServiceScheduleObjects property value
     *
     * @return array
     */
    public function getEmployeeServiceScheduleObjects()
    {
        return $this->employeeServiceScheduleObjects;
    }

    /**
     * Sets the employeeServiceScheduleObjects property value
     * @param array $employeeServiceScheduleObjects
     *
     * @return $this
     * @throws Exception
     */
    public function setEmployeeServiceScheduleObjects(array $employeeServiceScheduleObjects)
    {
        if (!empty($employeeServiceScheduleObjects))
        {
            $aux = 0;
            foreach ($employeeServiceScheduleObjects as $employeeServiceScheduleObject)
            {
                if ($employeeServiceScheduleObject instanceof EmployeeServiceSchedules)
                {
                    $this->addEmployeeServiceScheduleObjects($employeeServiceScheduleObject);
                }
                else
                {
                    throw new Exception('Expecting array element ' . $aux . ' to be an instance of EmployeeServiceSchedules');
                }
                $aux++;
            }
        }
        return $this;
    }

    /**
     * Adds a new element to the array of employee service schedules
     * @param EmployeeServiceSchedules $employeeServiceSchedules
     * @return $this
     */
    public function addEmployeeServiceScheduleObjects(EmployeeServiceSchedules $employeeServiceSchedules)
    {
        $this->employeeServiceScheduleObjects[] = $employeeServiceSchedules;
        return $this;
    }
}