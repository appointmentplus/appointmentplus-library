<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 6:17 PM
 */

namespace apptlibrary\resources\employees;

use \Exception;

/**
 * Class EmployeeServiceParameters
 * @package apptlibrary\resources\employees
 */
class EmployeeServiceParameters
{
    /**
     * Min value for a schedule
     */
    const MIN_RANGE = 1;
    /**
     * Max value for a schedule
     */
    const MAX_RANGE = 2359;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $serviceId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $employeeId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $duration;
    /**
     * The start time for Monday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleMondayStartTime;
    /**
     * The end time for Monday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleMondayEndTime;
    /**
     * The start time for Tuesday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleTuesdayStartTime;
    /**
     * The end time for Tuesday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleTuesdayEndTime;
    /**
     * The start time for Wednesday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleWednesdayStartTime;
    /**
     * The end time for Wednesday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleWednesdayEndTime;
    /**
     * The start time for Thursday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleThursdayStartTime;
    /**
     * The end time for Thursday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleThursdayEndTime;
    /**
     * The start time for Friday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleFridayStartTime;
    /**
     * The end time for Friday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleFridayEndTime;
    /**
     * The start time for Saturday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleSaturdayStartTime;
    /**
     * The end time for Saturday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleSaturdayEndTime;
    /**
     * The start time for Sunday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleSundayStartTime;
    /**
     * The end time for Sunday's schedule. (Valid range, 0001-2359)
     * @access protected
     * @var integer
     */
    protected $scheduleSundayEndTime;

    /**
     * Checks if a value is within the range of defined values
     * @access protected
     * @param $value
     * @return bool
     */
    protected function withinRange($value)
    {
        if (($value >= self::MIN_RANGE) && ($value <= self::MAX_RANGE))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the serviceId property value
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Sets the serviceId property value
     * @param int $serviceId
     *
     * @return $this
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    /**
     * Returns the employeeId property value
     *
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Sets the employeeId property value
     * @param int $employeeId
     *
     * @return $this
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Returns the duration property value
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Sets the duration property value
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * Returns the scheduleMondayStartTime property value
     *
     * @return int
     */
    public function getScheduleMondayStartTime()
    {
        return $this->scheduleMondayStartTime;
    }

    /**
     * Sets the scheduleMondayStartTime property value
     * @param int $scheduleMondayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleMondayStartTime($scheduleMondayStartTime)
    {
        $value = $scheduleMondayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleMondayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleMondayEndTime property value
     *
     * @return int
     */
    public function getScheduleMondayEndTime()
    {
        return $this->scheduleMondayEndTime;
    }

    /**
     * Sets the scheduleMondayEndTime property value
     * @param int $scheduleMondayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleMondayEndTime($scheduleMondayEndTime)
    {
        $value = $scheduleMondayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleMondayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleTuesdayStartTime property value
     *
     * @return int
     */
    public function getScheduleTuesdayStartTime()
    {
        return $this->scheduleTuesdayStartTime;
    }

    /**
     * Sets the scheduleTuesdayStartTime property value
     * @param int $scheduleTuesdayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleTuesdayStartTime($scheduleTuesdayStartTime)
    {
        $value = $scheduleTuesdayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleTuesdayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleTuesdayEndTime property value
     *
     * @return int
     */
    public function getScheduleTuesdayEndTime()
    {
        return $this->scheduleTuesdayEndTime;
    }

    /**
     * Sets the scheduleTuesdayEndTime property value
     * @param int $scheduleTuesdayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleTuesdayEndTime($scheduleTuesdayEndTime)
    {
        $value = $scheduleTuesdayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleTuesdayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleWednesdayStartTime property value
     *
     * @return int
     */
    public function getScheduleWednesdayStartTime()
    {
        return $this->scheduleWednesdayStartTime;
    }

    /**
     * Sets the scheduleWednesdayStartTime property value
     * @param int $scheduleWednesdayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleWednesdayStartTime($scheduleWednesdayStartTime)
    {
        $value = $scheduleWednesdayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleWednesdayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleWednesdayEndTime property value
     *
     * @return int
     */
    public function getScheduleWednesdayEndTime()
    {
        return $this->scheduleWednesdayEndTime;
    }

    /**
     * Sets the scheduleWednesdayEndTime property value
     * @param int $scheduleWednesdayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleWednesdayEndTime($scheduleWednesdayEndTime)
    {
        $value = $scheduleWednesdayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleWednesdayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleThursdayStartTime property value
     *
     * @return int
     */
    public function getScheduleThursdayStartTime()
    {
        return $this->scheduleThursdayStartTime;
    }

    /**
     * Sets the scheduleThursdayStartTime property value
     * @param int $scheduleThursdayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleThursdayStartTime($scheduleThursdayStartTime)
    {
        $value = $scheduleThursdayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleThursdayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleThursdayEndTime property value
     *
     * @return int
     */
    public function getScheduleThursdayEndTime()
    {
        return $this->scheduleThursdayEndTime;
    }

    /**
     * Sets the scheduleThursdayEndTime property value
     * @param int $scheduleThursdayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleThursdayEndTime($scheduleThursdayEndTime)
    {
        $value = $scheduleThursdayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleThursdayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleFridayStartTime property value
     *
     * @return int
     */
    public function getScheduleFridayStartTime()
    {
        return $this->scheduleFridayStartTime;
    }

    /**
     * Sets the scheduleFridayStartTime property value
     * @param int $scheduleFridayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleFridayStartTime($scheduleFridayStartTime)
    {
        $value = $scheduleFridayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleFridayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleFridayEndTime property value
     *
     * @return int
     */
    public function getScheduleFridayEndTime()
    {
        return $this->scheduleFridayEndTime;
    }

    /**
     * Sets the scheduleFridayEndTime property value
     * @param int $scheduleFridayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleFridayEndTime($scheduleFridayEndTime)
    {
        $value = $scheduleFridayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleFridayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleSaturdayStartTime property value
     *
     * @return int
     */
    public function getScheduleSaturdayStartTime()
    {
        return $this->scheduleSaturdayStartTime;
    }

    /**
     * Sets the scheduleSaturdayStartTime property value
     * @param int $scheduleSaturdayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleSaturdayStartTime($scheduleSaturdayStartTime)
    {
        $value = $scheduleSaturdayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleSaturdayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleSaturdayEndTime property value
     *
     * @return int
     */
    public function getScheduleSaturdayEndTime()
    {
        return $this->scheduleSaturdayEndTime;
    }

    /**
     * Sets the scheduleSaturdayEndTime property value
     * @param int $scheduleSaturdayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleSaturdayEndTime($scheduleSaturdayEndTime)
    {
        $value = $scheduleSaturdayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleSaturdayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleSundayStartTime property value
     *
     * @return int
     */
    public function getScheduleSundayStartTime()
    {
        return $this->scheduleSundayStartTime;
    }

    /**
     * Sets the scheduleSundayStartTime property value
     * @param int $scheduleSundayStartTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleSundayStartTime($scheduleSundayStartTime)
    {
        $value = $scheduleSundayStartTime;
        if ($this->withinRange($value))
        {
            $this->scheduleSundayStartTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }

    /**
     * Returns the scheduleSundayEndTime property value
     *
     * @return int
     */
    public function getScheduleSundayEndTime()
    {
        return $this->scheduleSundayEndTime;
    }

    /**
     * Sets the scheduleSundayEndTime property value
     * @param int $scheduleSundayEndTime
     *
     * @return $this
     * @throws Exception
     */
    public function setScheduleSundayEndTime($scheduleSundayEndTime)
    {
        $value = $scheduleSundayEndTime;
        if ($this->withinRange($value))
        {
            $this->scheduleSundayEndTime = $value;
        }
        else
        {
            throw new Exception('Value is out of range: ' . $value . '. Expected value to be between ' . self::MIN_RANGE . ' and ' . self::MAX_RANGE);
        }
        return $this;
    }
}