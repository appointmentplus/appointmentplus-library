<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 2:01 AM
 */

namespace apptlibrary\resources\employees;

use \Exception;

/**
 * Class EmployeeActiveSchedule
 * @package apptlibrary\resources\employees
 */
class EmployeeActiveSchedule
{
    /**
     * The ID of the client for the employee active schedule.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The employee ID for the active schedule
     * @access protected
     * @var integer
     */
    protected $employeeId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $startTimestamp;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $endTimestamp;
    /**
     * The list of slot objects in active schedule.
     * @access protected
     * @var array
     */
    protected $slots;

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the employeeId property value
     *
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Sets the employeeId property value
     * @param int $employeeId
     *
     * @return $this
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Returns the startTimestamp property value
     *
     * @return string
     */
    public function getStartTimestamp()
    {
        return $this->startTimestamp;
    }

    /**
     * Sets the startTimestamp property value
     * @param string $startTimestamp
     *
     * @return $this
     */
    public function setStartTimestamp($startTimestamp)
    {
        $this->startTimestamp = $startTimestamp;
        return $this;
    }

    /**
     * Returns the endTimestamp property value
     *
     * @return string
     */
    public function getEndTimestamp()
    {
        return $this->endTimestamp;
    }

    /**
     * Sets the endTimestamp property value
     * @param string $endTimestamp
     *
     * @return $this
     */
    public function setEndTimestamp($endTimestamp)
    {
        $this->endTimestamp = $endTimestamp;
        return $this;
    }

    /**
     * Returns the slots property value
     *
     * @return array
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Sets the slots property value
     * @param array $slots
     *
     * @return $this
     * @throws Exception
     */
    public function setSlots($slots)
    {
        if (!empty($slots))
        {
            foreach ($slots as $slot)
            {
                if ($slot instanceof Slot)
                {
                    $this->addSlot($slot);
                }
                else
                {
                    throw new Exception('Array passed is not a valid object (Expected a Slot instance)');
                }
            }
        }
        return $this;
    }

    /**
     * Adds a slot to the slots array
     * @param Slot $slot
     * @return $this
     */
    public function addSlot(Slot $slot)
    {
        $this->slots[] = $slot;
        return $this;
    }
}