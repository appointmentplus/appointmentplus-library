<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 5:34 PM
 */

namespace apptlibrary\resources;

/**
 * Interface ISchedules
 * @package apptlibrary\resources
 */
interface ISchedules
{
    /**
     * Min value for a schedule
     */
    const MIN_RANGE = 1;
    /**
     * Max value for a schedule
     */
    const MAX_RANGE = 2359;
}