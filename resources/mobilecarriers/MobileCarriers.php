<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 9:19 PM
 */

namespace apptlibrary\resources\mobilecarriers;

/**
 * Class MobileCarriers
 * @package apptlibrary\resources\mobilecarriers
 */
class MobileCarriers
{
    /**
     * The ID of the mobile carrier
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The description of the mobile carrier.
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $carrierAddress;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $countryId;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the carrierAddress property value
     *
     * @return string
     */
    public function getCarrierAddress()
    {
        return $this->carrierAddress;
    }

    /**
     * Sets the carrierAddress property value
     * @param string $carrierAddress
     *
     * @return $this
     */
    public function setCarrierAddress($carrierAddress)
    {
        $this->carrierAddress = $carrierAddress;
        return $this;
    }

    /**
     * Returns the countryId property value
     *
     * @return int
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Sets the countryId property value
     * @param int $countryId
     *
     * @return $this
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
        return $this;
    }
}