<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 9:20 PM
 */

namespace apptlibrary\resources\packages;

use \Exception;

/**
 * Class Packages
 * @package apptlibrary\resources\packages
 */
class Packages
{
    /**
     * Active value for status
     */
    const ACTIVE = 1;
    /**
     * Inactive value for status
     */
    const INACTIVE = 2;
    /**
     * Deleted value for status
     */
    const DELETED = 3;
    /**
     * package type according to number of sessions
     */
    const TYPE_NUM_SESSIONS = 1;
    /**
     * package type according to dollar amount
     */
    const TYPE_DOLLAR_AMNT = 2;
    /**
     * package type according to a monthly fee
     */
    const TYPE_MONTH_FEE = 3;
    /**
     * package type according to number of days
     */
    const TYPE_NUM_DAYS = 4;
    /**
     * The ID of the package
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The ID of the customer assigned the package
     * @access protected
     * @var integer
     */
    protected $customerId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $name;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var float
     */
    protected $price;
    /**
     * 1 = active, 2 = inactive, 3 = deleted
     * @access protected
     * @var integer
     */
    protected $status;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var float
     */
    protected $costPerSession;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var float
     */
    protected $monthlyFee;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var float
     */
    protected $numberOfSessions;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $numberOfDays;
    /**
     * 1 = number of sessions, 2 = dollar amount, 3 = monthly fee, 4 = number of days
     * @access protected
     * @var integer
     */
    protected $packageTypeId;
    /**
     * float
     * @access protected
     * @var string
     */
    protected $enrollmentFee;
    /**
     * float
     * @access protected
     * @var string
     */
    protected $downPayment;
    /**
     * The list of package status IDs.
     * @access protected
     * @var array
     */
    protected $packageStatusIds;
    /**
     * The list of package service IDs.
     * @access protected
     * @var array
     */
    protected $packageServiceIds;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the customerId property value
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Sets the customerId property value
     * @param int $customerId
     *
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * Returns the name property value
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name property value
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Returns the price property value
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price property value
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Returns the status property value
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status property value
     * @param int $status
     *
     * @return $this
     * @throws Exception
     */
    public function setStatus($status)
    {
        if (in_array($status, array(self::ACTIVE, self::DELETED, self::INACTIVE)))
        {
            $this->status = $status;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::ACTIVE, self::INACTIVE or self::DELETED');
        }
        return $this;
    }

    /**
     * Returns the costPerSession property value
     *
     * @return float
     */
    public function getCostPerSession()
    {
        return $this->costPerSession;
    }

    /**
     * Sets the costPerSession property value
     * @param float $costPerSession
     *
     * @return $this
     */
    public function setCostPerSession($costPerSession)
    {
        $this->costPerSession = $costPerSession;
        return $this;
    }

    /**
     * Returns the monthlyFee property value
     *
     * @return float
     */
    public function getMonthlyFee()
    {
        return $this->monthlyFee;
    }

    /**
     * Sets the monthlyFee property value
     * @param float $monthlyFee
     *
     * @return $this
     */
    public function setMonthlyFee($monthlyFee)
    {
        $this->monthlyFee = $monthlyFee;
        return $this;
    }

    /**
     * Returns the numberOfSessions property value
     *
     * @return float
     */
    public function getNumberOfSessions()
    {
        return $this->numberOfSessions;
    }

    /**
     * Sets the numberOfSessions property value
     * @param float $numberOfSessions
     *
     * @return $this
     */
    public function setNumberOfSessions($numberOfSessions)
    {
        $this->numberOfSessions = $numberOfSessions;
        return $this;
    }

    /**
     * Returns the numberOfDays property value
     *
     * @return int
     */
    public function getNumberOfDays()
    {
        return $this->numberOfDays;
    }

    /**
     * Sets the numberOfDays property value
     * @param int $numberOfDays
     *
     * @return $this
     */
    public function setNumberOfDays($numberOfDays)
    {
        $this->numberOfDays = $numberOfDays;
        return $this;
    }

    /**
     * Returns the packageTypeId property value
     *
     * @return int
     */
    public function getPackageTypeId()
    {
        return $this->packageTypeId;
    }

    /**
     * Sets the packageTypeId property value
     * @param int $packageTypeId
     *
     * @return $this
     * @throws Exception
     */
    public function setPackageTypeId($packageTypeId)
    {
        if (in_array($packageTypeId, array(self::TYPE_DOLLAR_AMNT, self::TYPE_MONTH_FEE, self::TYPE_NUM_DAYS, self::TYPE_NUM_SESSIONS)))
        {
            $this->packageTypeId = $packageTypeId;
        }
        else
        {
            throw new Exception('Value is not valid, expecting: self::TYPE_DOLLAR_AMNT, self::TYPE_MONTH_FEE, self::TYPE_NUM_DAYS or self::TYPE_NUM_SESSIONS');
        }
        return $this;
    }

    /**
     * Returns the enrollmentFee property value
     *
     * @return string
     */
    public function getEnrollmentFee()
    {
        return $this->enrollmentFee;
    }

    /**
     * Sets the enrollmentFee property value
     * @param string $enrollmentFee
     *
     * @return $this
     */
    public function setEnrollmentFee($enrollmentFee)
    {
        $this->enrollmentFee = $enrollmentFee;
        return $this;
    }

    /**
     * Returns the downPayment property value
     *
     * @return string
     */
    public function getDownPayment()
    {
        return $this->downPayment;
    }

    /**
     * Sets the downPayment property value
     * @param string $downPayment
     *
     * @return $this
     */
    public function setDownPayment($downPayment)
    {
        $this->downPayment = $downPayment;
        return $this;
    }

    /**
     * Returns the packageStatusIds property value
     *
     * @return array
     */
    public function getPackageStatusIds()
    {
        return $this->packageStatusIds;
    }

    /**
     * Sets the packageStatusIds property value
     * @param array $packageStatusIds
     *
     * @return $this
     */
    public function setPackageStatusIds(array $packageStatusIds)
    {
        $this->packageStatusIds = $packageStatusIds;
        return $this;
    }

    /**
     * Returns the packageServiceIds property value
     *
     * @return array
     */
    public function getPackageServiceIds()
    {
        return $this->packageServiceIds;
    }

    /**
     * Sets the packageServiceIds property value
     * @param array $packageServiceIds
     *
     * @return $this
     */
    public function setPackageServiceIds(array $packageServiceIds)
    {
        $this->packageServiceIds = $packageServiceIds;
        return $this;
    }
}