<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:08 AM
 */

namespace apptlibrary\resources\rooms;

use apptlibrary\resources\services\Services;

/**
 * Class RoomEmployeeServiceSchedules
 * @package apptlibrary\resources\rooms
 */
class RoomEmployeeServiceSchedules
{
    /**
     * The client ID for the record.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The room ID for the record.
     * @access protected
     * @var integer
     */
    protected $roomId;
    /**
     * The employee ID for the record.
     * @access protected
     * @var integer
     */
    protected $employeeId;
    /**
     * The service ID for the record.
     * @access protected
     * @var integer
     */
    protected $serviceId;
    /**
     * The service object by associated room ID.
     * @access protected
     * @var Rooms
     */
    protected $roomObject;
    /**
     * The service object by associated service ID.
     * @access protected
     * @var Services
     */
    protected $serviceObject;

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the roomId property value
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Sets the roomId property value
     * @param int $roomId
     *
     * @return $this
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;
        return $this;
    }

    /**
     * Returns the employeeId property value
     *
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Sets the employeeId property value
     * @param int $employeeId
     *
     * @return $this
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Returns the serviceId property value
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Sets the serviceId property value
     * @param int $serviceId
     *
     * @return $this
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
        return $this;
    }

    /**
     * Returns the roomObject property value
     *
     * @return Rooms
     */
    public function getRoomObject()
    {
        return $this->roomObject;
    }

    /**
     * Sets the roomObject property value
     * @param Rooms $roomObject
     *
     * @return $this
     */
    public function setRoomObject(Rooms $roomObject)
    {
        $this->roomObject = $roomObject;
        return $this;
    }

    /**
     * Returns the serviceObject property value
     *
     * @return Services
     */
    public function getServiceObject()
    {
        return $this->serviceObject;
    }

    /**
     * Sets the serviceObject property value
     * @param Services $serviceObject
     *
     * @return $this
     */
    public function setServiceObject(Services $serviceObject)
    {
        $this->serviceObject = $serviceObject;
        return $this;
    }
}