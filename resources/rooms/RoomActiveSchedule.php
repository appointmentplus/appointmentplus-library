<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:06 AM
 */

namespace apptlibrary\resources\rooms;

/**
 * Class RoomActiveSchedule
 * @package apptlibrary\resources\rooms
 */
class RoomActiveSchedule
{
    /**
     * The client ID for the room active schedule.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The room ID for the room active schedule.
     * @access protected
     * @var integer
     */
    protected $roomId;
    /**
     * The date/time for the start of the schedule.
     * @access protected
     * @var string
     */
    protected $startTimestamp;
    /**
     * The date/time for the end of the schedule.
     * @access protected
     * @var string
     */
    protected $endTimestamp;
    /**
     * The list of slot objects for active schedule.
     * @access protected
     * @var array
     */
    protected $slots;

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the roomId property value
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Sets the roomId property value
     * @param int $roomId
     *
     * @return $this
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;
        return $this;
    }

    /**
     * Returns the startTimestamp property value
     *
     * @return string
     */
    public function getStartTimestamp()
    {
        return $this->startTimestamp;
    }

    /**
     * Sets the startTimestamp property value
     * @param string $startTimestamp
     *
     * @return $this
     */
    public function setStartTimestamp($startTimestamp)
    {
        $this->startTimestamp = $startTimestamp;
        return $this;
    }

    /**
     * Returns the endTimestamp property value
     *
     * @return string
     */
    public function getEndTimestamp()
    {
        return $this->endTimestamp;
    }

    /**
     * Sets the endTimestamp property value
     * @param string $endTimestamp
     *
     * @return $this
     */
    public function setEndTimestamp($endTimestamp)
    {
        $this->endTimestamp = $endTimestamp;
        return $this;
    }

    /**
     * Returns the slots property value
     *
     * @return array
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Sets the slots property value
     * @param array $slots
     *
     * @return $this
     * @throws Exception
     */
    public function setSlots(array $slots)
    {
        if (!empty($slots))
        {
            foreach ($slots as $slot)
            {
                if ($slot instanceof Slot)
                {
                    $this->addSlot($slot);
                }
                else
                {
                    throw new Exception('Array passed is not a valid object (Expected a Slot instance)');
                }
            }
        }
        return $this;
    }

    /**
     * Adds a slot to the slots array
     * @param Slot $slot
     * @return $this
     */
    public function addSlot(Slot $slot)
    {
        $this->slots[] = $slot;
        return $this;
    }
}