<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/8/2015
 * Time: 4:21 PM
 */

namespace apptlibrary\resources\appointments;

use \Exception;

/**
 * Class AppointmentGrid
 * @package apptlibrary\resources\appointments
 */
class AppointmentGrid
{
    /**
     * The starting date/time for the appointment grid range.
     * @access protected
     * @var string
     */
    protected $startTimestamp;
    /**
     * The ending date/time for the appointment grid range.
     * @access protected
     * @var string
     */
    protected $endTimestamp;
    /**
     * ItemObj containing a list of SlotObj
     * @var array
     * @access protected
     */
    protected $slots;

    /**
     * Returns the startTimestamp property value
     *
     * @return string
     */
    public function getStartTimestamp()
    {
        return $this->startTimestamp;
    }

    /**
     * Sets the startTimestamp property value
     * @param string $startTimestamp
     *
     * @return $this
     */
    public function setStartTimestamp($startTimestamp)
    {
        $this->startTimestamp = $startTimestamp;
        return $this;
    }

    /**
     * Returns the endTimestamp property value
     *
     * @return string
     */
    public function getEndTimestamp()
    {
        return $this->endTimestamp;
    }

    /**
     * Sets the endTimestamp property value
     * @param string $endTimestamp
     *
     * @return $this
     */
    public function setEndTimestamp($endTimestamp)
    {
        $this->endTimestamp = $endTimestamp;
        return $this;
    }

    /**
     * Returns the slots property value
     *
     * @return array
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Sets the slots property value
     * @param array $slots
     *
     * @return $this
     * @throws Exception
     */
    public function setSlots(array $slots)
    {
        if (!empty($slots))
        {
            foreach ($slots as $slot)
            {
                if ($slot instanceof Slot)
                {
                    $this->addSlot($slot);
                }
                else
                {
                    throw new Exception('Array passed is not a valid object (Expected a Slot instance)');
                }
            }
        }
        return $this;
    }

    /**
     * Adds a slot to the slots array
     * @param Slot $slot
     * @return $this
     */
    public function addSlot(Slot $slot)
    {
        $this->slots[] = $slot;
        return $this;
    }
}