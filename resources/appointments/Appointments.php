<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 12:07 AM
 */

namespace apptlibrary\resources\appointments;

/**
 * Class Appointments
 * @package apptlibrary\resources\appointments
 */
class Appointments
{
    /**
     * The ID of the appointment.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The ID of the client/location for the appointment.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The ID of the customer for the appointment.
     * @access protected
     * @var integer
     */
    protected $customerId;
    /**
     * The employee ID assigned to the appointment.
     * @access protected
     * @var integer
     */
    protected $employeeId;
    /**
     * The room ID for the appointment.
     * @access protected
     * @var integer
     */
    protected $roomId;
    /**
     * The date/time the appointment begins.
     * @access protected
     * @var string
     */
    protected $startTimestamp;
    /**
     * The date/time the appointment ends.
     * @access protected
     * @var string
     */
    protected $endTimestamp;
    /**
     * Service ID's for the appointment.
     * @access protected
     * @var integer[]
     */
    protected $services;
    /**
     * Customer related notes for the appointment.
     * @access protected
     * @var string
     */
    protected $customerNotes;
    /**
     * Employee related notes for the appointment.
     * @access protected
     * @var string
     */
    protected $employeeNotes;
    /**
     * Status ID for the appointment. See appointment status
     * @access protected
     * @var integer
     */
    protected $statusId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $paymentEmployeeId;
    /**
     * The cost of the appointment.
     * @access protected
     * @var float
     */
    protected $costAmount;
    /**
     * The tip amount for the appointment.
     * @access protected
     * @var float
     */
    protected $tipAmount;
    /**
     * The payment ID for the appointment.
     * @access protected
     * @var integer
     */
    protected $paymentTypeId;
    /**
     * The coupon ID for the appointment.
     * @access protected
     * @var integer
     */
    protected $couponId;
    /**
     * The type ID for the appointment. See appointment types
     * @access protected
     * @var integer
     */
    protected $typeId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $numberInGroup;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $recurringId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $reserveDescription;
    /**
     * The reason for cancelling an appointment.
     * @access protected
     * @var string
     */
    protected $cancellationReason;
    /**
     * The date/time the appointment record was created.
     * @access protected
     * @var string
     */
    protected $whenAdded;
    /**
     * The date/time the appointment was updated.
     * @access protected
     * @var string
     */
    protected $whenUpdated;
    /**
     * The ID of the employee who created the appointment.
     * @access protected
     * @var integer
     */
    protected $createdByEmployeeId;
    /**
     * The ID of the employee who last updated the appointment.
     * @access protected
     * @var integer
     */
    protected $lastUpdatedByEmployeeId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $customerPackageId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $durationId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $associatedId;
    /**
     * The ID of the make of the vehicle. See vehicle makes
     * @access protected
     * @var integer
     */
    protected $vehicleMakeId;
    /**
     * The ID of the model of the vehicle. See vehicle models
     * @access protected
     * @var integer
     */
    protected $vehicleModelId;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $vehicleOther;
    /**
     * The ID of the model year of the vehicle.
     * @access protected
     * @var integer
     */
    protected $modelYear;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $certNumber;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $certState;
    /**
     * The vin number for the vehicle.
     * @access protected
     * @var string
     */
    protected $vin;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $dealerName;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $dealerAddress;
    /**
     * The reason for the salvaged title.
     * @access protected
     * @var string
     */
    protected $salvageReason;
    /**
     * The vehicle odometer reading.
     * @access protected
     * @var string
     */
    protected $odometer;
    /**
     * PO number for the appointment.
     * @access protected
     * @var string
     */
    protected $poNumber;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var integer
     */
    protected $gender;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $followUpDate;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the customerId property value
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Sets the customerId property value
     * @param int $customerId
     *
     * @return $this
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * Returns the employeeId property value
     *
     * @return int
     */
    public function getEmployeeId()
    {
        return $this->employeeId;
    }

    /**
     * Sets the employeeId property value
     * @param int $employeeId
     *
     * @return $this
     */
    public function setEmployeeId($employeeId)
    {
        $this->employeeId = $employeeId;
        return $this;
    }

    /**
     * Returns the roomId property value
     *
     * @return int
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Sets the roomId property value
     * @param int $roomId
     *
     * @return $this
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;
        return $this;
    }

    /**
     * Returns the startTimestamp property value
     *
     * @return string
     */
    public function getStartTimestamp()
    {
        return $this->startTimestamp;
    }

    /**
     * Sets the startTimestamp property value
     * @param string $startTimestamp
     *
     * @return $this
     */
    public function setStartTimestamp($startTimestamp)
    {
        $this->startTimestamp = $startTimestamp;
        return $this;
    }

    /**
     * Returns the endTimestamp property value
     *
     * @return string
     */
    public function getEndTimestamp()
    {
        return $this->endTimestamp;
    }

    /**
     * Sets the endTimestamp property value
     * @param string $endTimestamp
     *
     * @return $this
     */
    public function setEndTimestamp($endTimestamp)
    {
        $this->endTimestamp = $endTimestamp;
        return $this;
    }

    /**
     * Returns the services property value
     *
     * @return \integer[]
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Sets the services property value
     * @param \integer[] $services
     *
     * @return $this
     */
    public function setServices($services)
    {
        $this->services = $services;
        return $this;
    }

    /**
     * Returns the customerNotes property value
     *
     * @return string
     */
    public function getCustomerNotes()
    {
        return $this->customerNotes;
    }

    /**
     * Sets the customerNotes property value
     * @param string $customerNotes
     *
     * @return $this
     */
    public function setCustomerNotes($customerNotes)
    {
        $this->customerNotes = $customerNotes;
        return $this;
    }

    /**
     * Returns the employeeNotes property value
     *
     * @return string
     */
    public function getEmployeeNotes()
    {
        return $this->employeeNotes;
    }

    /**
     * Sets the employeeNotes property value
     * @param string $employeeNotes
     *
     * @return $this
     */
    public function setEmployeeNotes($employeeNotes)
    {
        $this->employeeNotes = $employeeNotes;
        return $this;
    }

    /**
     * Returns the statusId property value
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Sets the statusId property value
     * @param int $statusId
     *
     * @return $this
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;
        return $this;
    }

    /**
     * Returns the paymentEmployeeId property value
     *
     * @return int
     */
    public function getPaymentEmployeeId()
    {
        return $this->paymentEmployeeId;
    }

    /**
     * Sets the paymentEmployeeId property value
     * @param int $paymentEmployeeId
     *
     * @return $this
     */
    public function setPaymentEmployeeId($paymentEmployeeId)
    {
        $this->paymentEmployeeId = $paymentEmployeeId;
        return $this;
    }

    /**
     * Returns the costAmount property value
     *
     * @return float
     */
    public function getCostAmount()
    {
        return $this->costAmount;
    }

    /**
     * Sets the costAmount property value
     * @param float $costAmount
     *
     * @return $this
     */
    public function setCostAmount($costAmount)
    {
        $this->costAmount = $costAmount;
        return $this;
    }

    /**
     * Returns the tipAmount property value
     *
     * @return float
     */
    public function getTipAmount()
    {
        return $this->tipAmount;
    }

    /**
     * Sets the tipAmount property value
     * @param float $tipAmount
     *
     * @return $this
     */
    public function setTipAmount($tipAmount)
    {
        $this->tipAmount = $tipAmount;
        return $this;
    }

    /**
     * Returns the paymentTypeId property value
     *
     * @return int
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * Sets the paymentTypeId property value
     * @param int $paymentTypeId
     *
     * @return $this
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->paymentTypeId = $paymentTypeId;
        return $this;
    }

    /**
     * Returns the couponId property value
     *
     * @return int
     */
    public function getCouponId()
    {
        return $this->couponId;
    }

    /**
     * Sets the couponId property value
     * @param int $couponId
     *
     * @return $this
     */
    public function setCouponId($couponId)
    {
        $this->couponId = $couponId;
        return $this;
    }

    /**
     * Returns the typeId property value
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Sets the typeId property value
     * @param int $typeId
     *
     * @return $this
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * Returns the numberInGroup property value
     *
     * @return int
     */
    public function getNumberInGroup()
    {
        return $this->numberInGroup;
    }

    /**
     * Sets the numberInGroup property value
     * @param int $numberInGroup
     *
     * @return $this
     */
    public function setNumberInGroup($numberInGroup)
    {
        $this->numberInGroup = $numberInGroup;
        return $this;
    }

    /**
     * Returns the recurringId property value
     *
     * @return int
     */
    public function getRecurringId()
    {
        return $this->recurringId;
    }

    /**
     * Sets the recurringId property value
     * @param int $recurringId
     *
     * @return $this
     */
    public function setRecurringId($recurringId)
    {
        $this->recurringId = $recurringId;
        return $this;
    }

    /**
     * Returns the reserveDescription property value
     *
     * @return string
     */
    public function getReserveDescription()
    {
        return $this->reserveDescription;
    }

    /**
     * Sets the reserveDescription property value
     * @param string $reserveDescription
     *
     * @return $this
     */
    public function setReserveDescription($reserveDescription)
    {
        $this->reserveDescription = $reserveDescription;
        return $this;
    }

    /**
     * Returns the cancellationReason property value
     *
     * @return string
     */
    public function getCancellationReason()
    {
        return $this->cancellationReason;
    }

    /**
     * Sets the cancellationReason property value
     * @param string $cancellationReason
     *
     * @return $this
     */
    public function setCancellationReason($cancellationReason)
    {
        $this->cancellationReason = $cancellationReason;
        return $this;
    }

    /**
     * Returns the whenAdded property value
     *
     * @return string
     */
    public function getWhenAdded()
    {
        return $this->whenAdded;
    }

    /**
     * Sets the whenAdded property value
     * @param string $whenAdded
     *
     * @return $this
     */
    public function setWhenAdded($whenAdded)
    {
        $this->whenAdded = $whenAdded;
        return $this;
    }

    /**
     * Returns the whenUpdated property value
     *
     * @return string
     */
    public function getWhenUpdated()
    {
        return $this->whenUpdated;
    }

    /**
     * Sets the whenUpdated property value
     * @param string $whenUpdated
     *
     * @return $this
     */
    public function setWhenUpdated($whenUpdated)
    {
        $this->whenUpdated = $whenUpdated;
        return $this;
    }

    /**
     * Returns the createdByEmployeeId property value
     *
     * @return int
     */
    public function getCreatedByEmployeeId()
    {
        return $this->createdByEmployeeId;
    }

    /**
     * Sets the createdByEmployeeId property value
     * @param int $createdByEmployeeId
     *
     * @return $this
     */
    public function setCreatedByEmployeeId($createdByEmployeeId)
    {
        $this->createdByEmployeeId = $createdByEmployeeId;
        return $this;
    }

    /**
     * Returns the lastUpdatedByEmployeeId property value
     *
     * @return int
     */
    public function getLastUpdatedByEmployeeId()
    {
        return $this->lastUpdatedByEmployeeId;
    }

    /**
     * Sets the lastUpdatedByEmployeeId property value
     * @param int $lastUpdatedByEmployeeId
     *
     * @return $this
     */
    public function setLastUpdatedByEmployeeId($lastUpdatedByEmployeeId)
    {
        $this->lastUpdatedByEmployeeId = $lastUpdatedByEmployeeId;
        return $this;
    }

    /**
     * Returns the customerPackageId property value
     *
     * @return int
     */
    public function getCustomerPackageId()
    {
        return $this->customerPackageId;
    }

    /**
     * Sets the customerPackageId property value
     * @param int $customerPackageId
     *
     * @return $this
     */
    public function setCustomerPackageId($customerPackageId)
    {
        $this->customerPackageId = $customerPackageId;
        return $this;
    }

    /**
     * Returns the durationId property value
     *
     * @return int
     */
    public function getDurationId()
    {
        return $this->durationId;
    }

    /**
     * Sets the durationId property value
     * @param int $durationId
     *
     * @return $this
     */
    public function setDurationId($durationId)
    {
        $this->durationId = $durationId;
        return $this;
    }

    /**
     * Returns the associatedId property value
     *
     * @return int
     */
    public function getAssociatedId()
    {
        return $this->associatedId;
    }

    /**
     * Sets the associatedId property value
     * @param int $associatedId
     *
     * @return $this
     */
    public function setAssociatedId($associatedId)
    {
        $this->associatedId = $associatedId;
        return $this;
    }

    /**
     * Returns the vehicleMakeId property value
     *
     * @return int
     */
    public function getVehicleMakeId()
    {
        return $this->vehicleMakeId;
    }

    /**
     * Sets the vehicleMakeId property value
     * @param int $vehicleMakeId
     *
     * @return $this
     */
    public function setVehicleMakeId($vehicleMakeId)
    {
        $this->vehicleMakeId = $vehicleMakeId;
        return $this;
    }

    /**
     * Returns the vehicleModelId property value
     *
     * @return int
     */
    public function getVehicleModelId()
    {
        return $this->vehicleModelId;
    }

    /**
     * Sets the vehicleModelId property value
     * @param int $vehicleModelId
     *
     * @return $this
     */
    public function setVehicleModelId($vehicleModelId)
    {
        $this->vehicleModelId = $vehicleModelId;
        return $this;
    }

    /**
     * Returns the vehicleOther property value
     *
     * @return string
     */
    public function getVehicleOther()
    {
        return $this->vehicleOther;
    }

    /**
     * Sets the vehicleOther property value
     * @param string $vehicleOther
     *
     * @return $this
     */
    public function setVehicleOther($vehicleOther)
    {
        $this->vehicleOther = $vehicleOther;
        return $this;
    }

    /**
     * Returns the modelYear property value
     *
     * @return int
     */
    public function getModelYear()
    {
        return $this->modelYear;
    }

    /**
     * Sets the modelYear property value
     * @param int $modelYear
     *
     * @return $this
     */
    public function setModelYear($modelYear)
    {
        $this->modelYear = $modelYear;
        return $this;
    }

    /**
     * Returns the certNumber property value
     *
     * @return string
     */
    public function getCertNumber()
    {
        return $this->certNumber;
    }

    /**
     * Sets the certNumber property value
     * @param string $certNumber
     *
     * @return $this
     */
    public function setCertNumber($certNumber)
    {
        $this->certNumber = $certNumber;
        return $this;
    }

    /**
     * Returns the certState property value
     *
     * @return string
     */
    public function getCertState()
    {
        return $this->certState;
    }

    /**
     * Sets the certState property value
     * @param string $certState
     *
     * @return $this
     */
    public function setCertState($certState)
    {
        $this->certState = $certState;
        return $this;
    }

    /**
     * Returns the vin property value
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Sets the vin property value
     * @param string $vin
     *
     * @return $this
     */
    public function setVin($vin)
    {
        $this->vin = $vin;
        return $this;
    }

    /**
     * Returns the dealerName property value
     *
     * @return string
     */
    public function getDealerName()
    {
        return $this->dealerName;
    }

    /**
     * Sets the dealerName property value
     * @param string $dealerName
     *
     * @return $this
     */
    public function setDealerName($dealerName)
    {
        $this->dealerName = $dealerName;
        return $this;
    }

    /**
     * Returns the dealerAddress property value
     *
     * @return string
     */
    public function getDealerAddress()
    {
        return $this->dealerAddress;
    }

    /**
     * Sets the dealerAddress property value
     * @param string $dealerAddress
     *
     * @return $this
     */
    public function setDealerAddress($dealerAddress)
    {
        $this->dealerAddress = $dealerAddress;
        return $this;
    }

    /**
     * Returns the salvageReason property value
     *
     * @return string
     */
    public function getSalvageReason()
    {
        return $this->salvageReason;
    }

    /**
     * Sets the salvageReason property value
     * @param string $salvageReason
     *
     * @return $this
     */
    public function setSalvageReason($salvageReason)
    {
        $this->salvageReason = $salvageReason;
        return $this;
    }

    /**
     * Returns the odometer property value
     *
     * @return string
     */
    public function getOdometer()
    {
        return $this->odometer;
    }

    /**
     * Sets the odometer property value
     * @param string $odometer
     *
     * @return $this
     */
    public function setOdometer($odometer)
    {
        $this->odometer = $odometer;
        return $this;
    }

    /**
     * Returns the poNumber property value
     *
     * @return string
     */
    public function getPoNumber()
    {
        return $this->poNumber;
    }

    /**
     * Sets the poNumber property value
     * @param string $poNumber
     *
     * @return $this
     */
    public function setPoNumber($poNumber)
    {
        $this->poNumber = $poNumber;
        return $this;
    }

    /**
     * Returns the gender property value
     *
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender property value
     * @param int $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * Returns the followUpDate property value
     *
     * @return string
     */
    public function getFollowUpDate()
    {
        return $this->followUpDate;
    }

    /**
     * Sets the followUpDate property value
     * @param string $followUpDate
     *
     * @return $this
     */
    public function setFollowUpDate($followUpDate)
    {
        $this->followUpDate = $followUpDate;
        return $this;
    }
}
