<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/8/2015
 * Time: 3:42 PM
 */

namespace apptlibrary\resources\designpatterns;

/**
 * Interface ISingleton
 * Defines standards to all singleton implementations
 * @package apptlibrary\resources\designpatterns
 */
interface ISingleton
{
    /**
     * Gets the singleton instance
     * @static
     * @return ISingleton
     */
    public static function getInstance();
}