<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 5:17 PM
 */

namespace apptlibrary\resources\designpatterns;


/**
 * Interface IFactory
 * @package apptlibrary\resources\designpatterns
 */
interface IFactory
{
    /**
     * Namespace separator character
     */
    const NAMESPACE_SEPARATOR = '\\';

    /**
     * Builds the desired object
     * @param $class
     * @return object
     * @throws \Exception
     */
    public static function build($class);
}