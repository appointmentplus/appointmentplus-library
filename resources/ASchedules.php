<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 5:33 PM
 */

namespace apptlibrary\resources;

/**
 * Class ASchedules
 * Schedules abstract class for all schedules
 * @package apptlibrary\resources
 */
abstract class ASchedules implements ISchedules
{
    /**
     * Checks if a value is within the range of defined values
     * @access protected
     * @param $value
     * @return bool
     */
    protected function withinRange($value)
    {
        if (($value >= self::MIN_RANGE) && ($value <= self::MAX_RANGE))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}