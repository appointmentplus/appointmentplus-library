<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/9/2015
 * Time: 1:15 AM
 */

namespace apptlibrary\resources\coupons;

/**
 * Class Coupons
 * @package apptlibrary\resources\coupons
 */
class Coupons
{
    /**
     * The ID of the coupon record.
     * @access protected
     * @var integer
     */
    protected $id;
    /**
     * The ID of the client for the coupon.
     * @access protected
     * @var integer
     */
    protected $clientId;
    /**
     * The description of the coupon.
     * @access protected
     * @var string
     */
    protected $description;
    /**
     * TODO: Find what does this mean
     * @access protected
     * @var string
     */
    protected $type;
    /**
     * The amount of the coupon.
     * @access protected
     * @var float
     */
    protected $amount;
    /**
     * The date/time the coupon was created.
     * @access protected
     * @var string
     */
    protected $whenAdded;
    /**
     * The date/time the coupon expired.
     * @access protected
     * @var string
     */
    protected $whenExpired;
    /**
     * The sort order for the record.
     * @access protected
     * @var integer
     */
    protected $sortOrder;
    /**
     * The coupon code.
     * @access protected
     * @var string
     */
    protected $code;

    /**
     * Returns the id property value
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the id property value
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Returns the clientId property value
     *
     * @return int
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the clientId property value
     * @param int $clientId
     *
     * @return $this
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * Returns the description property value
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description property value
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Returns the type property value
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type property value
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Returns the amount property value
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Sets the amount property value
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Returns the whenAdded property value
     *
     * @return string
     */
    public function getWhenAdded()
    {
        return $this->whenAdded;
    }

    /**
     * Sets the whenAdded property value
     * @param string $whenAdded
     *
     * @return $this
     */
    public function setWhenAdded($whenAdded)
    {
        $this->whenAdded = $whenAdded;
        return $this;
    }

    /**
     * Returns the whenExpired property value
     *
     * @return string
     */
    public function getWhenExpired()
    {
        return $this->whenExpired;
    }

    /**
     * Sets the whenExpired property value
     * @param string $whenExpired
     *
     * @return $this
     */
    public function setWhenExpired($whenExpired)
    {
        $this->whenExpired = $whenExpired;
        return $this;
    }

    /**
     * Returns the sortOrder property value
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Sets the sortOrder property value
     * @param int $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Returns the code property value
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets the code property value
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
}
