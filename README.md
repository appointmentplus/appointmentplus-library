# ApiLibrary

ApiLibrary is a wrapper library to consume the AppointmentPlus REST API with the following goals and advantages.

## Goals

* Single base code for all projects.
* Easy to implement.
* Allow flexibility.
* Follow best coding practices.
* Easy to understand.

## Advantages

* Scalable.
* Highly configurable.
* Unit tested.
* Ease the use of calls to API.
* Enforces documentation values.
* Well documented.
* Up to date with the API documentation.
* Speed up future developments.
* Clear errors.
* Easy response handling.
* Mantainable.

As we keep using it we will be adding new functionalities, correcting bugs and enhancing it.
Please note the file at the root of the library called:
test.php
It has a couple of working examples on how to configure, send information, do a request and handle the responses.
Please also check the unit test scenarios as they may provide further info on what each part of the code does (except for DAO's and DTO's.... because they are pretty simple).

