<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/20/2015
 * Time: 4:22 PM
 */

use \apptlibrary\api\Api;
use \apptlibrary\api\Factory as APIFactory;
use \apptlibrary\request\rest\Rest;
use \apptlibrary\response\responsecodes\Factory as RCFactory;
use \apptlibrary\response\Response;
use apptlibrary\response\responsecodes\IResponse;

/**
 * @param $class Class name to be loaded
 */
function autoload($class)
{
    $pathParts = explode('\\', $class);
    array_shift($pathParts);
    $correctPath = implode(DIRECTORY_SEPARATOR, $pathParts) . '.php';
    require_once($correctPath);
}

spl_autoload_register('autoload');

/**
 * Prepare config values
 */
$config = array(
    'username' => 'mesparza@tiempodevelopment.com',
    'password' => 'Retrolancer99',
    'employeeMasterId' => 7337,
    'clientMasterId' => 391911,
    'clientId' => 1657,
    'interface' => '038e648f69b23b2a59daf262d8122a24',
    'restURL' => 'https://development.appointment-plus.com/mobileapi/v1/Rest',
);

/**
 * Create the data object to be sent to the request
 */
$dataObj = new stdClass();
$dataObj->username = $config['username'];
$dataObj->password = $config['password'];
$dataObj->clientMasterId = $config['clientMasterId'];
$data = json_encode($dataObj); //Data will go in the format specified by the callType parameter

/**
 * Set the parameters array to be used in the request
 */
$parametersArray = array(
    'callType' => Rest::CALL_TYPE_JSON,
    'interface' => $config['interface'],
    'data' => $data,
);

/**
 * Use of factory to create the API instance
 */
/**
 * @var $api apptlibrary\api\Api
 */
$api = APIFactory::build(Rest::WS_REST_TYPE, array('url' => $config['restURL']));

/**
 * Successful request
 */
$responseData = $api->getAuthenticationToken($parametersArray);
$response = new Response($responseData);
$responseCode = RCFactory::build($response->getStatus());
$token = NULL;
if ($response->getStatus() == IResponse::STATUS_SUCCESS)
{
    $token = $response->getDetails()->token;
    $api->setToken($token);
}
var_dump('Factory Success Request ***********', $response, $responseCode->getResponseCodeMessage());


/**
 * Error request
 * Since the call type and the data are not in the same format the request will return an error
 */
$wrongParametersArray = $parametersArray;
$wrongParametersArray['callType'] = Rest::CALL_TYPE_XML;
$responseData = $api->getAuthenticationToken($wrongParametersArray);
$response = new Response($responseData);
$responseCode = RCFactory::build($response->getStatus());
var_dump('Factory Error Request ***********', $response, $responseCode->getResponseCodeMessage());

/**
 * API created form scratch
 */
$request = new Rest();
$request->setURL('https://development.appointment-plus.com/mobileapi/v1/Rest');
$api = new Api();
$api->setRequestEngine($request);
$responseData = $api->getAuthenticationToken($parametersArray);
$response = new Response($responseData);
var_dump('API from scratch ***********', $responseData, $response->getDetails());

/**
 * Request created from scratch
 */
$request = new Rest();
$request->setURL('https://development.appointment-plus.com/mobileapi/v1/Rest');
$request->setAction('GetAuthenticationToken');
$request->setParameters($parametersArray);
$responseData = $request->request();
$response = new Response($responseData);
var_dump('Request from scratch ***********', $responseData, $response->getDetails());
 