<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/24/2015
 * Time: 2:04 PM
 */

use apptlibrary\resources\coupons\Coupons;

class CouponsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $coupons = new Coupons();
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $coupons);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 978542;
        $setResult = $coupons->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 980653;
        $setResult = $coupons->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 'sldkfjnoernvr';
        $setResult = $coupons->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Type setter and getter
     */
    public function testTypeSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = ' fxunwuo';
        $setResult = $coupons->setType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Amount setter and getter
     */
    public function testAmountSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 29.4;
        $setResult = $coupons->setAmount($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getAmount();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenAdded setter and getter
     */
    public function testWhenAddedSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 'mavlce9ywl8dw';
        $setResult = $coupons->setWhenAdded($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getWhenAdded();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenExpired setter and getter
     */
    public function testWhenExpiredSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 'iub1lpoqsaf';
        $setResult = $coupons->setWhenExpired($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getWhenExpired();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 60324;
        $setResult = $coupons->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Code setter and getter
     */
    public function testCodeSetterAndGetter()
    {
        $coupons = new Coupons();
        $expectedValue = 'u2rb 00y7rs';
        $setResult = $coupons->setCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\coupons\Coupons', $setResult);
        $getResult = $coupons->getCode();
        $this->assertEquals($expectedValue, $getResult);
    }
}