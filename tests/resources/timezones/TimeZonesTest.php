<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 4:09 PM
 */

use apptlibrary\resources\timezones\TimeZones;

/**
 * Class TimeZonesTest
 */
class TimeZonesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $timeZones = new TimeZones();
        $this->assertInstanceOf('apptlibrary\resources\timezones\TimeZones', $timeZones);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $timeZones = new TimeZones();
        $expectedValue = 1254;
        $setResult = $timeZones->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\timezones\TimeZones', $setResult);
        $getResult = $timeZones->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TimeZone setter and getter
     */
    public function testTimeZoneSetterAndGetter()
    {
        $timeZones = new TimeZones();
        $expectedValue = 'g ijvrkip1ss';
        $setResult = $timeZones->setTimeZone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\timezones\TimeZones', $setResult);
        $getResult = $timeZones->getTimeZone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Offset setter and getter
     */
    public function testOffsetSetterAndGetter()
    {
        $timeZones = new TimeZones();
        $expectedValue = 'p1s44ha';
        $setResult = $timeZones->setOffset($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\timezones\TimeZones', $setResult);
        $getResult = $timeZones->getOffset();
        $this->assertEquals($expectedValue, $getResult);
    }
}