<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 10:50 AM
 */

use apptlibrary\resources\customers\CustomerFieldSet;

/**
 * Class CustomerFieldSetTest
 */
class CustomerFieldSetTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customerFieldSet = new CustomerFieldSet();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $customerFieldSet);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 149153;
        $setResult = $customerFieldSet->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FieldName setter and getter
     */
    public function testFieldNameSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 'j6mjzbxuui2';
        $setResult = $customerFieldSet->setFieldName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getFieldName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AlternativeFieldName setter and getter
     */
    public function testAlternativeFieldNameSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 'l5yg0i hfgks2fg';
        $setResult = $customerFieldSet->setAlternativeFieldName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getAlternativeFieldName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DisplayName setter and getter
     */
    public function testDisplayNameSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = '7l hzf8dlac0';
        $setResult = $customerFieldSet->setDisplayName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getDisplayName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerViewRegistration setter and getter
     */
    public function testCustomerViewRegistrationSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 'bs2sko';
        $setResult = $customerFieldSet->setCustomerViewRegistration($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getCustomerViewRegistration();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AdminCustomerView setter and getter
     */
    public function testAdminCustomerViewSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 'ral5kkbaua t fk';
        $setResult = $customerFieldSet->setAdminCustomerView($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getAdminCustomerView();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AdminCreateAppointment setter and getter
     */
    public function testAdminCreateAppointmentSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 'nhbwdm j l0ll';
        $setResult = $customerFieldSet->setAdminCreateAppointment($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getAdminCreateAppointment();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FieldType setter and getter
     */
    public function testFieldTypeSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = CustomerFieldSet::TEXTFIELD;
        $setResult = $customerFieldSet->setFieldType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getFieldType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $customerFieldSet = new CustomerFieldSet();
        $expectedValue = 266058;
        $setResult = $customerFieldSet->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerFieldSet', $setResult);
        $getResult = $customerFieldSet->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }
}