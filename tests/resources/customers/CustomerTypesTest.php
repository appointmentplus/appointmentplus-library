<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 3:13 PM
 */

use apptlibrary\resources\customers\CustomerTypes;

/**
 * Class CustomerTypesTest
 */
class CustomerTypesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customerTypes = new CustomerTypes();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerTypes', $customerTypes);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customerTypes = new CustomerTypes();
        $expectedValue = 594057;
        $setResult = $customerTypes->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerTypes', $setResult);
        $getResult = $customerTypes->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $customerTypes = new CustomerTypes();
        $expectedValue = 399804;
        $setResult = $customerTypes->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerTypes', $setResult);
        $getResult = $customerTypes->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $customerTypes = new CustomerTypes();
        $expectedValue = '631zq2hla3o hyv';
        $setResult = $customerTypes->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerTypes', $setResult);
        $getResult = $customerTypes->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $customerTypes = new CustomerTypes();
        $expectedValue = 37655;
        $setResult = $customerTypes->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerTypes', $setResult);
        $getResult = $customerTypes->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }
}