<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/24/2015
 * Time: 2:14 PM
 */

use apptlibrary\resources\customers\CustomerDetails;

/**
 * Class CustomerDetailsTest
 */
class CustomerDetailsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customerDetails = new CustomerDetails();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $customerDetails);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 316852;
        $setResult = $customerDetails->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FirstName setter and getter
     */
    public function testFirstNameSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',vxyy 1siv';
        $setResult = $customerDetails->setFirstName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getFirstName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MiddleName setter and getter
     */
    public function testMiddleNameSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',9tm0q6kx5wp';
        $setResult = $customerDetails->setMiddleName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getMiddleName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastName setter and getter
     */
    public function testLastNameSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',lr4ypbbc';
        $setResult = $customerDetails->setLastName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLastName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SignupDate setter and getter
     */
    public function testSignupDateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',shogd7';
        $setResult = $customerDetails->setSignupDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getSignupDate();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 618963;
        $setResult = $customerDetails->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LocationId setter and getter
     */
    public function testLocationIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 671316;
        $setResult = $customerDetails->setLocationId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLocationId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 154488;
        $setResult = $customerDetails->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ParentId setter and getter
     */
    public function testParentIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 381076;
        $setResult = $customerDetails->setParentId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getParentId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Address1 setter and getter
     */
    public function testAddress1SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',atsba11';
        $setResult = $customerDetails->setAddress1($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getAddress1();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Address2 setter and getter
     */
    public function testAddress2SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',6t 8egtg3nine';
        $setResult = $customerDetails->setAddress2($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getAddress2();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the City setter and getter
     */
    public function testCitySetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',e09v2ssmzhgax0';
        $setResult = $customerDetails->setCity($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getCity();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the State setter and getter
     */
    public function testStateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',viv0hem';
        $setResult = $customerDetails->setState($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getState();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PostalCode setter and getter
     */
    public function testPostalCodeSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',yywamjipp';
        $setResult = $customerDetails->setPostalCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPostalCode();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DaytimePhone setter and getter
     */
    public function testDaytimePhoneSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',4dryk6';
        $setResult = $customerDetails->setDaytimePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getDaytimePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NighttimePhone setter and getter
     */
    public function testNighttimePhoneSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',0qfcoz1l';
        $setResult = $customerDetails->setNighttimePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getNighttimePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MobilePhone setter and getter
     */
    public function testMobilePhoneSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',mkw4n3 8cnh';
        $setResult = $customerDetails->setMobilePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getMobilePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Fax setter and getter
     */
    public function testFaxSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',rzhkt88rwf';
        $setResult = $customerDetails->setFax($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getFax();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Email setter and getter
     */
    public function testEmailSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',9yt2p6oleh';
        $setResult = $customerDetails->setEmail($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getEmail();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherAddress1 setter and getter
     */
    public function testOtherAddress1SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',2paiuj';
        $setResult = $customerDetails->setOtherAddress1($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherAddress1();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherAddress2 setter and getter
     */
    public function testOtherAddress2SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',x2omr9pwiag1e';
        $setResult = $customerDetails->setOtherAddress2($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherAddress2();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherCity setter and getter
     */
    public function testOtherCitySetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',9a0kjbdn87sht';
        $setResult = $customerDetails->setOtherCity($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherCity();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherState setter and getter
     */
    public function testOtherStateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',497wpc89wigio x';
        $setResult = $customerDetails->setOtherState($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherState();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherPostalCode setter and getter
     */
    public function testOtherPostalCodeSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',j10w8rriq';
        $setResult = $customerDetails->setOtherPostalCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherPostalCode();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherPhone1 setter and getter
     */
    public function testOtherPhone1SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',ru9 5iq2j5yz';
        $setResult = $customerDetails->setOtherPhone1($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherPhone1();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OtherPhone2 setter and getter
     */
    public function testOtherPhone2SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',2l5o9ws3';
        $setResult = $customerDetails->setOtherPhone2($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOtherPhone2();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Login setter and getter
     */
    public function testLoginSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',zj3gycfwv';
        $setResult = $customerDetails->setLogin($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLogin();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Password setter and getter
     */
    public function testPasswordSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ', z07z5g';
        $setResult = $customerDetails->setPassword($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPassword();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TemporaryPassword setter and getter
     */
    public function testTemporaryPasswordSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',10btvb4sopf39';
        $setResult = $customerDetails->setTemporaryPassword($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getTemporaryPassword();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerTypeId setter and getter
     */
    public function testCustomerTypeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 998372;
        $setResult = $customerDetails->setCustomerTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getCustomerTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OkayToContact setter and getter
     */
    public function testOkayToContactSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = true;
        $setResult = $customerDetails->setOkayToContact($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOkayToContact();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OkayToCall setter and getter
     */
    public function testOkayToCallSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = true;
        $setResult = $customerDetails->setOkayToCall($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOkayToCall();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OkayToEmail setter and getter
     */
    public function testOkayToEmailSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = false;
        $setResult = $customerDetails->setOkayToEmail($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOkayToEmail();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the OkayToMail setter and getter
     */
    public function testOkayToMailSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = true;
        $setResult = $customerDetails->setOkayToMail($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOkayToMail();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PaymentTypeId setter and getter
     */
    public function testPaymentTypeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 309174;
        $setResult = $customerDetails->setPaymentTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPaymentTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastFourOnCard setter and getter
     */
    public function testLastFourOnCardSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 215911;
        $setResult = $customerDetails->setLastFourOnCard($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLastFourOnCard();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NameOnCard setter and getter
     */
    public function testNameOnCardSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',bmnoew2iu';
        $setResult = $customerDetails->setNameOnCard($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getNameOnCard();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MerchantToken setter and getter
     */
    public function testMerchantTokenSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',8i3 d9e68jx';
        $setResult = $customerDetails->setMerchantToken($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getMerchantToken();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastUpdateEmployeeId setter and getter
     */
    public function testLastUpdateEmployeeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 898186;
        $setResult = $customerDetails->setLastUpdateEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLastUpdateEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastUpdateTimestamp setter and getter
     */
    public function testLastUpdateTimestampSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',7ew9w7xzdzhn4 p';
        $setResult = $customerDetails->setLastUpdateTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLastUpdateTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LeadTypeId setter and getter
     */
    public function testLeadTypeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 128428;
        $setResult = $customerDetails->setLeadTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLeadTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AssignedToEmployeeId setter and getter
     */
    public function testAssignedToEmployeeIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 862882;
        $setResult = $customerDetails->setAssignedToEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getAssignedToEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Employer setter and getter
     */
    public function testEmployerSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',j3f1evqa8jfr';
        $setResult = $customerDetails->setEmployer($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getEmployer();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Occupation setter and getter
     */
    public function testOccupationSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',dbb99rub';
        $setResult = $customerDetails->setOccupation($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getOccupation();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the BirthDate setter and getter
     */
    public function testBirthDateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',svy qg4x6mx8';
        $setResult = $customerDetails->setBirthDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getBirthDate();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Gender setter and getter
     */
    public function testGenderSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',twf5dr6 hmr';
        $setResult = $customerDetails->setGender($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getGender();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerStatusId setter and getter
     */
    public function testCustomerStatusIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 187246;
        $setResult = $customerDetails->setCustomerStatusId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getCustomerStatusId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AllowLogin setter and getter
     */
    public function testAllowLoginSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = true;
        $setResult = $customerDetails->setAllowLogin($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getAllowLogin();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DropDownTypeId1 setter and getter
     */
    public function testDropDownTypeId1SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 890127;
        $setResult = $customerDetails->setDropDownTypeId1($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getDropDownTypeId1();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DropDownTypeId2 setter and getter
     */
    public function testDropDownTypeId2SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 843139;
        $setResult = $customerDetails->setDropDownTypeId2($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getDropDownTypeId2();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DropDownTypeId3 setter and getter
     */
    public function testDropDownTypeId3SetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 780640;
        $setResult = $customerDetails->setDropDownTypeId3($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getDropDownTypeId3();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AccountNumber setter and getter
     */
    public function testAccountNumberSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',qufu3kx5y';
        $setResult = $customerDetails->setAccountNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getAccountNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SpecialNeeds setter and getter
     */
    public function testSpecialNeedsSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',xuuh8lkrbplad5';
        $setResult = $customerDetails->setSpecialNeeds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getSpecialNeeds();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Prefix setter and getter
     */
    public function testPrefixSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',98b8ov8kldm8ep';
        $setResult = $customerDetails->setPrefix($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPrefix();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PrimaryInsurance setter and getter
     */
    public function testPrimaryInsuranceSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',3jdvavw';
        $setResult = $customerDetails->setPrimaryInsurance($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPrimaryInsurance();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PrimaryInsured setter and getter
     */
    public function testPrimaryInsuredSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',r2q1gkwi';
        $setResult = $customerDetails->setPrimaryInsured($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPrimaryInsured();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SecondaryInsurance setter and getter
     */
    public function testSecondaryInsuranceSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',w6oyggkx7xhq ';
        $setResult = $customerDetails->setSecondaryInsurance($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getSecondaryInsurance();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the InsuranceType setter and getter
     */
    public function testInsuranceTypeSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',4vedkszqkedh';
        $setResult = $customerDetails->setInsuranceType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getInsuranceType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the InsurancePlanNumber setter and getter
     */
    public function testInsurancePlanNumberSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',u68wrwqiy8b';
        $setResult = $customerDetails->setInsurancePlanNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getInsurancePlanNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the InsuranceGroupNumber setter and getter
     */
    public function testInsuranceGroupNumberSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',g00u3l3xapzrvvm';
        $setResult = $customerDetails->setInsuranceGroupNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getInsuranceGroupNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Ssn setter and getter
     */
    public function testSsnSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',snbs r34';
        $setResult = $customerDetails->setSsn($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getSsn();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ReferredBy setter and getter
     */
    public function testReferredBySetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',jgqo6fut';
        $setResult = $customerDetails->setReferredBy($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getReferredBy();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the InternalAlertMessage setter and getter
     */
    public function testInternalAlertMessageSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',3r9hlr2';
        $setResult = $customerDetails->setInternalAlertMessage($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getInternalAlertMessage();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MobileCarrierId setter and getter
     */
    public function testMobileCarrierIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 990015;
        $setResult = $customerDetails->setMobileCarrierId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getMobileCarrierId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TimeZoneId setter and getter
     */
    public function testTimeZoneIdSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = 241953;
        $setResult = $customerDetails->setTimeZoneId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getTimeZoneId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LoginChangeDate setter and getter
     */
    public function testLoginChangeDateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',k kgify6gx4';
        $setResult = $customerDetails->setLoginChangeDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getLoginChangeDate();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PasswordChangeDate setter and getter
     */
    public function testPasswordChangeDateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',jd3eiz5ab';
        $setResult = $customerDetails->setPasswordChangeDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getPasswordChangeDate();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the UpdateInformationPromptDate setter and getter
     */
    public function testUpdateInformationPromptDateSetterAndGetter()
    {
        $customerDetails = new CustomerDetails();
        $expectedValue = ',fan50h0l4wnwqs';
        $setResult = $customerDetails->setUpdateInformationPromptDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $setResult);
        $getResult = $customerDetails->getUpdateInformationPromptDate();
        $this->assertEquals($expectedValue, $getResult);
    }

}