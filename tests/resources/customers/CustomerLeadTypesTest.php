<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 11:53 AM
 */

use apptlibrary\resources\customers\CustomerLeadTypes;

/**
 * Class CustomerLeadTypesTest
 */
class CustomerLeadTypesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customerLeadTypes = new CustomerLeadTypes();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerLeadTypes', $customerLeadTypes);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customerLeadTypes = new CustomerLeadTypes();
        $expectedValue = 656920;
        $setResult = $customerLeadTypes->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerLeadTypes', $setResult);
        $getResult = $customerLeadTypes->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $customerLeadTypes = new CustomerLeadTypes();
        $expectedValue = 647853;
        $setResult = $customerLeadTypes->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerLeadTypes', $setResult);
        $getResult = $customerLeadTypes->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $customerLeadTypes = new CustomerLeadTypes();
        $expectedValue = 'lqxxcqh2t9';
        $setResult = $customerLeadTypes->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerLeadTypes', $setResult);
        $getResult = $customerLeadTypes->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $customerLeadTypes = new CustomerLeadTypes();
        $expectedValue = 470845;
        $setResult = $customerLeadTypes->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerLeadTypes', $setResult);
        $getResult = $customerLeadTypes->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }
}