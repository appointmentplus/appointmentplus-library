<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 12:02 PM
 */

use apptlibrary\resources\customers\Customers;
use apptlibrary\resources\customers\CustomerDetails;

/**
 * Class CustomersTest
 */
class CustomersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customers = new Customers();
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $customers);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customers = new Customers();
        $expectedValue = 156706;
        $setResult = $customers->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $setResult);
        $getResult = $customers->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DetailType setter and getter
     */
    public function testDetailTypeSetterAndGetter()
    {
        $customers = new Customers();
        $expectedValue = '47t4s6sqjjbvkhn';
        $setResult = $customers->setDetailType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $setResult);
        $getResult = $customers->getDetailType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerDetails setter and getter
     */
    public function testCustomerDetailsSetterAndGetter()
    {
        $customers = new Customers();
        $expectedValue = new CustomerDetails();
        $setResult = $customers->setCustomerDetails($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $setResult);
        $getResult = $customers->getCustomerDetails();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerDetails', $getResult);
    }

    /**
     * Test the Customers setter and getter
     */
    public function testCustomersSetterAndGetter()
    {
        $customers = new Customers();
        $expectedValue = array(new Customers());
        $setResult = $customers->setCustomers($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $setResult);
        $getResult = $customers->getCustomers();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new Customers();
        $addResult = $customers->addCustomer($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\Customers', $addResult);
        $getResult = $customers->getCustomers();
        $this->assertCount(2, $getResult);
    }
}