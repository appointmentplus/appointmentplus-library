<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 2:59 PM
 */

use apptlibrary\resources\customers\CustomerStatus;

/**
 * Class CustomerStatusTest
 */
class CustomerStatusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $customerStatus = new CustomerStatus();
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $customerStatus);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $customerStatus = new CustomerStatus();
        $expectedValue = 290513;
        $setResult = $customerStatus->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $setResult);
        $getResult = $customerStatus->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $customerStatus = new CustomerStatus();
        $expectedValue = 881510;
        $setResult = $customerStatus->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $setResult);
        $getResult = $customerStatus->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $customerStatus = new CustomerStatus();
        $expectedValue = '8uk0995u';
        $setResult = $customerStatus->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $setResult);
        $getResult = $customerStatus->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $customerStatus = new CustomerStatus();
        $expectedValue = 79808;
        $setResult = $customerStatus->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $setResult);
        $getResult = $customerStatus->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StatusType setter and getter
     */
    public function testStatusTypeSetterAndGetter()
    {
        $customerStatus = new CustomerStatus();
        $expectedValue = CustomerStatus::NEW_TYPE;
        $setResult = $customerStatus->setStatusType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\customers\CustomerStatus', $setResult);
        $getResult = $customerStatus->getStatusType();
        $this->assertEquals($expectedValue, $getResult);
    }
}