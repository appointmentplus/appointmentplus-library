<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 12:17
 */

use apptlibrary\resources\employees\EmployeeServiceSchedules;
use apptlibrary\resources\employees\Employees;
use apptlibrary\resources\services\Services;

/**
 * Class EmployeeServiceSchedulesTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class EmployeeServiceSchedulesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $employeeServiceSchedules);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1767;
        $setResult = $employeeServiceSchedules->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 679;
        $setResult = $employeeServiceSchedules->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceId setter and getter
     */
    public function testServiceIdSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 930;
        $setResult = $employeeServiceSchedules->setServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1295;
        $setResult = $employeeServiceSchedules->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Duration setter and getter
     */
    public function testDurationSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 299;
        $setResult = $employeeServiceSchedules->setDuration($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getDuration();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeObject setter and getter
     */
    public function testEmployeeObjectSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = new Employees();
        $setResult = $employeeServiceSchedules->setEmployeeObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getEmployeeObject();
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $getResult);
    }

    /**
     * Test the ServiceObject setter and getter
     */
    public function testServiceObjectSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = new Services();
        $setResult = $employeeServiceSchedules->setServiceObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getServiceObject();
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $getResult);
    }

    /**
     * Test the ScheduleMondayStartTime setter and getter
     */
    public function testScheduleMondayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1672;
        $setResult = $employeeServiceSchedules->setScheduleMondayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleMondayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayEndTime setter and getter
     */
    public function testScheduleMondayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 143;
        $setResult = $employeeServiceSchedules->setScheduleMondayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleMondayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayStartTime setter and getter
     */
    public function testScheduleTuesdayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 502;
        $setResult = $employeeServiceSchedules->setScheduleTuesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleTuesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayEndTime setter and getter
     */
    public function testScheduleTuesdayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 891;
        $setResult = $employeeServiceSchedules->setScheduleTuesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleTuesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayStartTime setter and getter
     */
    public function testScheduleWednesdayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1013;
        $setResult = $employeeServiceSchedules->setScheduleWednesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleWednesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayEndTime setter and getter
     */
    public function testScheduleWednesdayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 278;
        $setResult = $employeeServiceSchedules->setScheduleWednesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleWednesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayStartTime setter and getter
     */
    public function testScheduleThursdayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 740;
        $setResult = $employeeServiceSchedules->setScheduleThursdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleThursdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayEndTime setter and getter
     */
    public function testScheduleThursdayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 763;
        $setResult = $employeeServiceSchedules->setScheduleThursdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleThursdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayStartTime setter and getter
     */
    public function testScheduleFridayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1095;
        $setResult = $employeeServiceSchedules->setScheduleFridayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleFridayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayEndTime setter and getter
     */
    public function testScheduleFridayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 926;
        $setResult = $employeeServiceSchedules->setScheduleFridayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleFridayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayStartTime setter and getter
     */
    public function testScheduleSaturdayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 101;
        $setResult = $employeeServiceSchedules->setScheduleSaturdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleSaturdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayEndTime setter and getter
     */
    public function testScheduleSaturdayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 972;
        $setResult = $employeeServiceSchedules->setScheduleSaturdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleSaturdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayStartTime setter and getter
     */
    public function testScheduleSundayStartTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 355;
        $setResult = $employeeServiceSchedules->setScheduleSundayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleSundayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayEndTime setter and getter
     */
    public function testScheduleSundayEndTimeSetterAndGetter()
    {
        $employeeServiceSchedules = new EmployeeServiceSchedules();
        $expectedValue = 1716;
        $setResult = $employeeServiceSchedules->setScheduleSundayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceSchedules', $setResult);
        $getResult = $employeeServiceSchedules->getScheduleSundayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }
}