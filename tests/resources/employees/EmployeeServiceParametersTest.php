<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 11:54
 */

use apptlibrary\resources\employees\EmployeeServiceParameters;

class EmployeeServiceParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $employeeServiceParameters);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 316;
        $setResult = $employeeServiceParameters->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1117;
        $setResult = $employeeServiceParameters->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceId setter and getter
     */
    public function testServiceIdSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1124;
        $setResult = $employeeServiceParameters->setServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1500;
        $setResult = $employeeServiceParameters->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Duration setter and getter
     */
    public function testDurationSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 334;
        $setResult = $employeeServiceParameters->setDuration($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getDuration();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayStartTime setter and getter
     */
    public function testScheduleMondayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1650;
        $setResult = $employeeServiceParameters->setScheduleMondayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleMondayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayEndTime setter and getter
     */
    public function testScheduleMondayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 2159;
        $setResult = $employeeServiceParameters->setScheduleMondayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleMondayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayStartTime setter and getter
     */
    public function testScheduleTuesdayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1537;
        $setResult = $employeeServiceParameters->setScheduleTuesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleTuesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayEndTime setter and getter
     */
    public function testScheduleTuesdayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 2052;
        $setResult = $employeeServiceParameters->setScheduleTuesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleTuesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayStartTime setter and getter
     */
    public function testScheduleWednesdayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 653;
        $setResult = $employeeServiceParameters->setScheduleWednesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleWednesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayEndTime setter and getter
     */
    public function testScheduleWednesdayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 173;
        $setResult = $employeeServiceParameters->setScheduleWednesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleWednesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayStartTime setter and getter
     */
    public function testScheduleThursdayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1622;
        $setResult = $employeeServiceParameters->setScheduleThursdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleThursdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayEndTime setter and getter
     */
    public function testScheduleThursdayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 568;
        $setResult = $employeeServiceParameters->setScheduleThursdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleThursdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayStartTime setter and getter
     */
    public function testScheduleFridayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 2070;
        $setResult = $employeeServiceParameters->setScheduleFridayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleFridayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayEndTime setter and getter
     */
    public function testScheduleFridayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1836;
        $setResult = $employeeServiceParameters->setScheduleFridayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleFridayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayStartTime setter and getter
     */
    public function testScheduleSaturdayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1008;
        $setResult = $employeeServiceParameters->setScheduleSaturdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleSaturdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayEndTime setter and getter
     */
    public function testScheduleSaturdayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1105;
        $setResult = $employeeServiceParameters->setScheduleSaturdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleSaturdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayStartTime setter and getter
     */
    public function testScheduleSundayStartTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1028;
        $setResult = $employeeServiceParameters->setScheduleSundayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleSundayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayEndTime setter and getter
     */
    public function testScheduleSundayEndTimeSetterAndGetter()
    {
        $employeeServiceParameters = new EmployeeServiceParameters();
        $expectedValue = 1909;
        $setResult = $employeeServiceParameters->setScheduleSundayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeServiceParameters', $setResult);
        $getResult = $employeeServiceParameters->getScheduleSundayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }
}