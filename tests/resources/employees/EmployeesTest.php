<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 5:01 PM
 */

use apptlibrary\resources\employees\Employees;
use apptlibrary\resources\employees\EmployeeSchedules;
use apptlibrary\resources\employees\EmployeeServiceSchedules;

/**
 * Class EmployeesTest
 */
class EmployeesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $employees = new Employees();
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $employees);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 280917;
        $setResult = $employees->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 316487;
        $setResult = $employees->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastName setter and getter
     */
    public function testLastNameSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = '8c4paz5b41nt0';
        $setResult = $employees->setLastName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getLastName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FirstName setter and getter
     */
    public function testFirstNameSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'bejlf6823 ';
        $setResult = $employees->setFirstName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getFirstName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MiddleName setter and getter
     */
    public function testMiddleNameSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 's0d0qw9g1bif';
        $setResult = $employees->setMiddleName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getMiddleName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DisplayName setter and getter
     */
    public function testDisplayNameSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 't1w0ll';
        $setResult = $employees->setDisplayName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getDisplayName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Company setter and getter
     */
    public function testCompanySetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'c5t18c0kb';
        $setResult = $employees->setCompany($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getCompany();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Bio setter and getter
     */
    public function testBioSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'mroil4liw l9';
        $setResult = $employees->setBio($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getBio();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Notes setter and getter
     */
    public function testNotesSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'x37k8a';
        $setResult = $employees->setNotes($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getNotes();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Status setter and getter
     */
    public function testStatusSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = '2clhdpsmaiq ';
        $setResult = $employees->setStatus($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getStatus();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Email setter and getter
     */
    public function testEmailSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = '5kmsjdgcv72i';
        $setResult = $employees->setEmail($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getEmail();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TypeId setter and getter
     */
    public function testTypeIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 430738;
        $setResult = $employees->setTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DisplayToCustomer setter and getter
     */
    public function testDisplayToCustomerSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = Employees::YES;
        $setResult = $employees->setDisplayToCustomer($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getDisplayToCustomer();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 437233;
        $setResult = $employees->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the HomePhone setter and getter
     */
    public function testHomePhoneSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'mk5adw01zf';
        $setResult = $employees->setHomePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getHomePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WorkPhone setter and getter
     */
    public function testWorkPhoneSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'rjavnb9q8r';
        $setResult = $employees->setWorkPhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getWorkPhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MobilePhone setter and getter
     */
    public function testMobilePhoneSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = '1pei0gn';
        $setResult = $employees->setMobilePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getMobilePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Fax setter and getter
     */
    public function testFaxSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'xb96p1bm3s';
        $setResult = $employees->setFax($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getFax();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SalesPersonId setter and getter
     */
    public function testSalesPersonIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 672043;
        $setResult = $employees->setSalesPersonId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getSalesPersonId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TaxIdNumber setter and getter
     */
    public function testTaxIdNumberSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 560041;
        $setResult = $employees->setTaxIdNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getTaxIdNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Address setter and getter
     */
    public function testAddressSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'huztxlrd00r8';
        $setResult = $employees->setAddress($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getAddress();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the City setter and getter
     */
    public function testCitySetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'x5rag0';
        $setResult = $employees->setCity($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getCity();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the State setter and getter
     */
    public function testStateSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 'p11 i4x';
        $setResult = $employees->setState($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getState();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PostalCode setter and getter
     */
    public function testPostalCodeSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = '4nej lohq';
        $setResult = $employees->setPostalCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getPostalCode();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MobileCarrierId setter and getter
     */
    public function testMobileCarrierIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 344813;
        $setResult = $employees->setMobileCarrierId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getMobileCarrierId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TimeZoneId setter and getter
     */
    public function testTimeZoneIdSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 555115;
        $setResult = $employees->setTimeZoneId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getTimeZoneId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AppointmentLeadTime setter and getter
     */
    public function testAppointmentLeadTimeSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = 51.64;
        $setResult = $employees->setAppointmentLeadTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getAppointmentLeadTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeScheduleObjects setter and getter
     */
    public function testEmployeeScheduleObjectsSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = array(new EmployeeSchedules());
        $setResult = $employees->setEmployeeScheduleObjects($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getEmployeeScheduleObjects();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new EmployeeSchedules();
        $addResult = $employees->addEmployeeScheduleObject($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $addResult);
        $getResult = $employees->getEmployeeScheduleObjects();
        $this->assertCount(2, $getResult);
    }

    /**
     * Test the EmployeeServiceScheduleObjects setter and getter
     */
    public function testEmployeeServiceScheduleObjectsSetterAndGetter()
    {
        $employees = new Employees();
        $expectedValue = array(new EmployeeServiceSchedules());
        $setResult = $employees->setEmployeeServiceScheduleObjects($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $setResult);
        $getResult = $employees->getEmployeeServiceScheduleObjects();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new EmployeeServiceSchedules();
        $addResult = $employees->addEmployeeServiceScheduleObjects($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\Employees', $addResult);
        $getResult = $employees->getEmployeeServiceScheduleObjects();
        $this->assertCount(2, $getResult);
    }
}