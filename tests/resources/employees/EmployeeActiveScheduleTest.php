<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/27/2015
 * Time: 4:18 PM
 */

use apptlibrary\resources\employees\EmployeeActiveSchedule;

/**
 * Class EmployeeActiveScheduleTest
 */
class EmployeeActiveScheduleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $employeeActiveSchedule);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $expectedValue = 434268;
        $setResult = $employeeActiveSchedule->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $setResult);
        $getResult = $employeeActiveSchedule->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $expectedValue = 454832;
        $setResult = $employeeActiveSchedule->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $setResult);
        $getResult = $employeeActiveSchedule->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StartTimestamp setter and getter
     */
    public function testStartTimestampSetterAndGetter()
    {
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $expectedValue = 'gabugmf';
        $setResult = $employeeActiveSchedule->setStartTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $setResult);
        $getResult = $employeeActiveSchedule->getStartTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EndTimestamp setter and getter
     */
    public function testEndTimestampSetterAndGetter()
    {
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $expectedValue = 'afb5mhgo98';
        $setResult = $employeeActiveSchedule->setEndTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $setResult);
        $getResult = $employeeActiveSchedule->getEndTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Slots setter and getter
     */
    public function testSlotsSetterAndGetter()
    {
        $this->markTestSkipped('UNDEFINED: Slot object hasn\'t been defined');
        $employeeActiveSchedule = new EmployeeActiveSchedule();
        $expectedValue = new Slots();
        $setResult = $employeeActiveSchedule->setSlots($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $setResult);
        $getResult = $employeeActiveSchedule->getSlots();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new Slot();
        $addResult = $employeeActiveSchedule->addSlot($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeActiveSchedule', $addResult);
        $getResult = $employeeActiveSchedule->getSlots();
        $this->assertCount(2, $getResult);
    }
}