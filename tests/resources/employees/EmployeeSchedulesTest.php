<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 11:16
 */

use apptlibrary\resources\employees\EmployeeSchedules;

class EmployeeSchedulesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $employeeSchedules = new EmployeeSchedules();
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $employeeSchedules);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 2107;
        $setResult = $employeeSchedules->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1937;
        $setResult = $employeeSchedules->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1233;
        $setResult = $employeeSchedules->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayStartTime setter and getter
     */
    public function testScheduleMondayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 2006;
        $setResult = $employeeSchedules->setScheduleMondayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleMondayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayEndTime setter and getter
     */
    public function testScheduleMondayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 630;
        $setResult = $employeeSchedules->setScheduleMondayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleMondayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayStartTime setter and getter
     */
    public function testScheduleTuesdayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 572;
        $setResult = $employeeSchedules->setScheduleTuesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleTuesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayEndTime setter and getter
     */
    public function testScheduleTuesdayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 197;
        $setResult = $employeeSchedules->setScheduleTuesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleTuesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayStartTime setter and getter
     */
    public function testScheduleWednesdayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1735;
        $setResult = $employeeSchedules->setScheduleWednesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleWednesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayEndTime setter and getter
     */
    public function testScheduleWednesdayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 739;
        $setResult = $employeeSchedules->setScheduleWednesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleWednesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayStartTime setter and getter
     */
    public function testScheduleThursdayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1653;
        $setResult = $employeeSchedules->setScheduleThursdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleThursdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayEndTime setter and getter
     */
    public function testScheduleThursdayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 945;
        $setResult = $employeeSchedules->setScheduleThursdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleThursdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayStartTime setter and getter
     */
    public function testScheduleFridayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1982;
        $setResult = $employeeSchedules->setScheduleFridayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleFridayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayEndTime setter and getter
     */
    public function testScheduleFridayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1252;
        $setResult = $employeeSchedules->setScheduleFridayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleFridayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayStartTime setter and getter
     */
    public function testScheduleSaturdayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 2066;
        $setResult = $employeeSchedules->setScheduleSaturdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleSaturdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayEndTime setter and getter
     */
    public function testScheduleSaturdayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 969;
        $setResult = $employeeSchedules->setScheduleSaturdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleSaturdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayStartTime setter and getter
     */
    public function testScheduleSundayStartTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 165;
        $setResult = $employeeSchedules->setScheduleSundayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleSundayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayEndTime setter and getter
     */
    public function testScheduleSundayEndTimeSetterAndGetter()
    {
        $employeeSchedules = new EmployeeSchedules();
        $expectedValue = 1696;
        $setResult = $employeeSchedules->setScheduleSundayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\employees\EmployeeSchedules', $setResult);
        $getResult = $employeeSchedules->getScheduleSundayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }
}