<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 10:08 AM
 */

use apptlibrary\resources\pets\PetFactors;

/**
 * Class PetFactorsTest
 */
class PetFactorsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $petFactors = new PetFactors();
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $petFactors);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $petFactors = new PetFactors();
        $expectedValue = 834275;
        $setResult = $petFactors->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $setResult);
        $getResult = $petFactors->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $petFactors = new PetFactors();
        $expectedValue = 685034;
        $setResult = $petFactors->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $setResult);
        $getResult = $petFactors->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $petFactors = new PetFactors();
        $expectedValue = 'zh8syuua3pm';
        $setResult = $petFactors->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $setResult);
        $getResult = $petFactors->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $petFactors = new PetFactors();
        $expectedValue = 751515;
        $setResult = $petFactors->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $setResult);
        $getResult = $petFactors->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }
}