<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 16:06
 */


use apptlibrary\resources\pets\PetDetailFactors;
use apptlibrary\resources\pets\PetFactors;

/**
 * Class PetDetailFactorsTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class PetDetailFactorsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $petDetailFactors = new PetDetailFactors();
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $petDetailFactors);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $petDetailFactors = new PetDetailFactors();
        $expectedValue = 748980;
        $setResult = $petDetailFactors->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $setResult);
        $getResult = $petDetailFactors->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $petDetailFactors = new PetDetailFactors();
        $expectedValue = 926022;
        $setResult = $petDetailFactors->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $setResult);
        $getResult = $petDetailFactors->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PetId setter and getter
     */
    public function testPetIdSetterAndGetter()
    {
        $petDetailFactors = new PetDetailFactors();
        $expectedValue = 906534;
        $setResult = $petDetailFactors->setPetId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $setResult);
        $getResult = $petDetailFactors->getPetId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FactorId setter and getter
     */
    public function testFactorIdSetterAndGetter()
    {
        $petDetailFactors = new PetDetailFactors();
        $expectedValue = 600954;
        $setResult = $petDetailFactors->setFactorId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $setResult);
        $getResult = $petDetailFactors->getFactorId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PetFactorObject setter and getter
     */
    public function testPetFactorObjectSetterAndGetter()
    {
        $petDetailFactors = new PetDetailFactors();
        $expectedValue = new PetFactors();
        $setResult = $petDetailFactors->setPetFactorObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\pets\PetDetailFactors', $setResult);
        $getResult = $petDetailFactors->getPetFactorObject();
        $this->assertInstanceOf('apptlibrary\resources\pets\PetFactors', $getResult);
    }
}