<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/24/2015
 * Time: 10:51 AM
 */

use apptlibrary\resources\clients\ClientPaymentTypes;

/**
 * Class ClientPaymentTypesTest
 */
class ClientPaymentTypesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $clientPaymentType = new ClientPaymentTypes();
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $clientPaymentType);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = 10149;
        $setResult = $clientPaymentTypes->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = 45361;
        $setResult = $clientPaymentTypes->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = '6n165l8wltl0z4xk ttsiyfytkshvavc oq1yjzmqw usouo';
        $setResult = $clientPaymentTypes->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = 767258;
        $setResult = $clientPaymentTypes->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Display setter and getter
     */
    public function testDisplaySetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = ClientPaymentTypes::NO;
        $setResult = $clientPaymentTypes->setDisplay($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getDisplay();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TypeId setter and getter
     */
    public function testTypeIdSetterAndGetter()
    {
        $clientPaymentTypes = new ClientPaymentTypes();
        $expectedValue = ClientPaymentTypes::GIFT_CERTIFICATE;
        $setResult = $clientPaymentTypes->setTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\ClientPaymentTypes', $setResult);
        $getResult = $clientPaymentTypes->getTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }
}