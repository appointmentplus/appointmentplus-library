<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/24/2015
 * Time: 12:14 PM
 */

use apptlibrary\resources\clients\Clients;

class ClientsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $clients = new Clients();
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $clients);
    }

    /**
     * Test the Status setter and getter
     */
    public function testStatusSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 792288;
        $setResult = $clients->setStatus($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getStatus();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 30069;
        $setResult = $clients->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientMasterId setter and getter
     */
    public function testClientMasterIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 321261;
        $setResult = $clients->setClientMasterId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getClientMasterId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Name setter and getter
     */
    public function testNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'e5ak77d12277sg';
        $setResult = $clients->setName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ContactFirstName setter and getter
     */
    public function testContactFirstNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '78qc1w3c3v2aj6v';
        $setResult = $clients->setContactFirstName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getContactFirstName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ContactLastName setter and getter
     */
    public function testContactLastNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '95yzjjo';
        $setResult = $clients->setContactLastName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getContactLastName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Address1 setter and getter
     */
    public function testAddress1SetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '2g333j8vg2ns';
        $setResult = $clients->setAddress1($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAddress1();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Address2 setter and getter
     */
    public function testAddress2SetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'dr8h0yyb3 j253g';
        $setResult = $clients->setAddress2($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAddress2();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the City setter and getter
     */
    public function testCitySetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'lnrur0';
        $setResult = $clients->setCity($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getCity();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the State setter and getter
     */
    public function testStateSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'v0gjgy6jy2i9h';
        $setResult = $clients->setState($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getState();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PostalCode setter and getter
     */
    public function testPostalCodeSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '5 e4qfnla';
        $setResult = $clients->setPostalCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getPostalCode();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CountryId setter and getter
     */
    public function testCountryIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 384645;
        $setResult = $clients->setCountryId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getCountryId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Phone setter and getter
     */
    public function testPhoneSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'jipku1t';
        $setResult = $clients->setPhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getPhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AlternativePhone setter and getter
     */
    public function testAlternativePhoneSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 't5wr6zkx';
        $setResult = $clients->setAlternativePhone($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAlternativePhone();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Fax setter and getter
     */
    public function testFaxSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '8ni93j55120vf';
        $setResult = $clients->setFax($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getFax();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Email setter and getter
     */
    public function testEmailSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '4ugcvptq25pi zq';
        $setResult = $clients->setEmail($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getEmail();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StoreNumber setter and getter
     */
    public function testStoreNumberSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '6c 988';
        $setResult = $clients->setStoreNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getStoreNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SiteType setter and getter
     */
    public function testSiteTypeSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 422192;
        $setResult = $clients->setSiteType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSiteType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the BusinessType setter and getter
     */
    public function testBusinessTypeSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 408387;
        $setResult = $clients->setBusinessType($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getBusinessType();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Website setter and getter
     */
    public function testWebsiteSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'jsdgimqpk0h3hu';
        $setResult = $clients->setWebsite($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getWebsite();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AbsolutePath setter and getter
     */
    public function testAbsolutePathSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'bu0sf03jfb';
        $setResult = $clients->setAbsolutePath($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAbsolutePath();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FrontEndFolderName setter and getter
     */
    public function testFrontEndFolderNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'or6rod';
        $setResult = $clients->setFrontEndFolderName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getFrontEndFolderName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SecureUrl setter and getter
     */
    public function testSecureUrlSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'ydztslw9h a';
        $setResult = $clients->setSecureUrl($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSecureUrl();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AppointmentMethod setter and getter
     */
    public function testAppointmentMethodSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 133230;
        $setResult = $clients->setAppointmentMethod($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAppointmentMethod();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DatabaseName setter and getter
     */
    public function testDatabaseNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'cia9sfhl';
        $setResult = $clients->setDatabaseName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getDatabaseName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenAdded setter and getter
     */
    public function testWhenAddedSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '7cegekr';
        $setResult = $clients->setWhenAdded($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getWhenAdded();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SetupComplete setter and getter
     */
    public function testSetupCompleteSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 239963;
        $setResult = $clients->setSetupComplete($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSetupComplete();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ParentId setter and getter
     */
    public function testParentIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 913386;
        $setResult = $clients->setParentId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getParentId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LocationName setter and getter
     */
    public function testLocationNameSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'p9wyxz2lfhx13';
        $setResult = $clients->setLocationName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getLocationName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ShareParentPreferences setter and getter
     */
    public function testShareParentPreferencesSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 13945;
        $setResult = $clients->setShareParentPreferences($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getShareParentPreferences();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ResellerId setter and getter
     */
    public function testResellerIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 494383;
        $setResult = $clients->setResellerId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getResellerId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Directions setter and getter
     */
    public function testDirectionsSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'mfec5vyl7h4';
        $setResult = $clients->setDirections($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getDirections();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LocationNameAbbreviation setter and getter
     */
    public function testLocationNameAbbreviationSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'kj3jialm8n';
        $setResult = $clients->setLocationNameAbbreviation($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getLocationNameAbbreviation();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientBillingStatusId setter and getter
     */
    public function testClientBillingStatusIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 740093;
        $setResult = $clients->setClientBillingStatusId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getClientBillingStatusId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SalesManagerId setter and getter
     */
    public function testSalesManagerIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 841792;
        $setResult = $clients->setSalesManagerId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSalesManagerId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SalesPersonId setter and getter
     */
    public function testSalesPersonIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 166951;
        $setResult = $clients->setSalesPersonId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSalesPersonId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TerritoryId setter and getter
     */
    public function testTerritoryIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 94400;
        $setResult = $clients->setTerritoryId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getTerritoryId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the InternalSalesPersonId setter and getter
     */
    public function testInternalSalesPersonIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 180581;
        $setResult = $clients->setInternalSalesPersonId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getInternalSalesPersonId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SendWelcomeEmails setter and getter
     */
    public function testSendWelcomeEmailsSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 186300;
        $setResult = $clients->setSendWelcomeEmails($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSendWelcomeEmails();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SendEndOfTrialEmails setter and getter
     */
    public function testSendEndOfTrialEmailsSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 579691;
        $setResult = $clients->setSendEndOfTrialEmails($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSendEndOfTrialEmails();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ResellerNotes setter and getter
     */
    public function testResellerNotesSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'rwq59mdmg6t ';
        $setResult = $clients->setResellerNotes($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getResellerNotes();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ShowWelcomeBanner setter and getter
     */
    public function testShowWelcomeBannerSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 338589;
        $setResult = $clients->setShowWelcomeBanner($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getShowWelcomeBanner();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TierId setter and getter
     */
    public function testTierIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 89099;
        $setResult = $clients->setTierId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getTierId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the BrandId setter and getter
     */
    public function testBrandIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 737839;
        $setResult = $clients->setBrandId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getBrandId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TimeZoneId setter and getter
     */
    public function testTimeZoneIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 212172;
        $setResult = $clients->setTimeZoneId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getTimeZoneId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ReferrerDomain setter and getter
     */
    public function testReferrerDomainSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '7n t7p9jb ';
        $setResult = $clients->setReferrerDomain($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getReferrerDomain();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ReferrerKeyword setter and getter
     */
    public function testReferrerKeywordSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'dnwc4r2';
        $setResult = $clients->setReferrerKeyword($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getReferrerKeyword();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AccountNumber setter and getter
     */
    public function testAccountNumberSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'nmh lqdye1urgc';
        $setResult = $clients->setAccountNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getAccountNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PayPerClickCampaign setter and getter
     */
    public function testPayPerClickCampaignSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = '6omhqqpuup5u1g';
        $setResult = $clients->setPayPerClickCampaign($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getPayPerClickCampaign();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenUpdated setter and getter
     */
    public function testWhenUpdatedSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 'eps0vohusbrqi0q';
        $setResult = $clients->setWhenUpdated($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getWhenUpdated();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientSizeId setter and getter
     */
    public function testClientSizeIdSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 530002;
        $setResult = $clients->setClientSizeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getClientSizeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $clients = new Clients();
        $expectedValue = 54801;
        $setResult = $clients->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientScheduleObject setter and getter
     */
    public function testClientScheduleObjectSetterAndGetter()
    {
        $this->markTestSkipped('UNDEFINED: ClientSchedule object hasn\'t been defined');
        $clients = new Clients();
        $expectedValue = new ClientSchedule();
        $setResult = $clients->setClientSchedule($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\clients\Clients', $setResult);
        $getResult = $clients->getClientSchedule();
        $this->assertEquals($expectedValue, $getResult);
    }
}