<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 10:26 AM
 */

use apptlibrary\resources\rooms\RoomActiveSchedule;

/**
 * Class RoomActiveScheduleTest
 */
class RoomActiveScheduleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $roomActiveSchedule = new RoomActiveSchedule();
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $roomActiveSchedule);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $roomActiveSchedule = new RoomActiveSchedule();
        $expectedValue = 908376;
        $setResult = $roomActiveSchedule->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $setResult);
        $getResult = $roomActiveSchedule->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the RoomId setter and getter
     */
    public function testRoomIdSetterAndGetter()
    {
        $roomActiveSchedule = new RoomActiveSchedule();
        $expectedValue = 867988;
        $setResult = $roomActiveSchedule->setRoomId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $setResult);
        $getResult = $roomActiveSchedule->getRoomId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StartTimestamp setter and getter
     */
    public function testStartTimestampSetterAndGetter()
    {
        $roomActiveSchedule = new RoomActiveSchedule();
        $expectedValue = 'yn1tdop4y9i4y8';
        $setResult = $roomActiveSchedule->setStartTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $setResult);
        $getResult = $roomActiveSchedule->getStartTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EndTimestamp setter and getter
     */
    public function testEndTimestampSetterAndGetter()
    {
        $roomActiveSchedule = new RoomActiveSchedule();
        $expectedValue = '8jc6t59aj26pms1';
        $setResult = $roomActiveSchedule->setEndTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $setResult);
        $getResult = $roomActiveSchedule->getEndTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Slots setter and getter
     */
    public function testSlotsSetterAndGetter()
    {
        $this->markTestSkipped('UNDEFINED: Slot object hasn\'t been defined');
        $roomActiveSchedule = new RoomActiveSchedule();
        $expectedValue = array(new Slot());
        $setResult = $roomActiveSchedule->setSlots($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $setResult);
        $getResult = $roomActiveSchedule->getSlots();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new Slot();
        $addResult = $roomActiveSchedule->addSlot($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomActiveSchedule', $addResult);
        $getResult = $roomActiveSchedule->getSlots();
        $this->assertCount(2, $getResult);
    }
}