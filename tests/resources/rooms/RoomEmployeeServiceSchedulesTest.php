<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 10:44 AM
 */

use apptlibrary\resources\rooms\RoomEmployeeServiceSchedules;
use apptlibrary\resources\rooms\Rooms;
use apptlibrary\resources\services\Services;

/**
 * Class RoomEmployeeServiceSchedulesTest
 */
class RoomEmployeeServiceSchedulesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $roomActiveSchedule = new RoomEmployeeServiceSchedules();
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $roomActiveSchedule);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = 601367;
        $setResult = $roomEmployeeServiceSchedules->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the RoomId setter and getter
     */
    public function testRoomIdSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = 529785;
        $setResult = $roomEmployeeServiceSchedules->setRoomId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getRoomId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = 229990;
        $setResult = $roomEmployeeServiceSchedules->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceId setter and getter
     */
    public function testServiceIdSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = 316816;
        $setResult = $roomEmployeeServiceSchedules->setServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the RoomObject setter and getter
     */
    public function testRoomObjectSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = new Rooms();
        $setResult = $roomEmployeeServiceSchedules->setRoomObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getRoomObject();
        $this->assertInstanceOf('apptlibrary\resources\rooms\Rooms', $getResult);
    }

    /**
     * Test the ServiceObject setter and getter
     */
    public function testServiceObjectSetterAndGetter()
    {
        $roomEmployeeServiceSchedules = new RoomEmployeeServiceSchedules();
        $expectedValue = new Services();
        $setResult = $roomEmployeeServiceSchedules->setServiceObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\rooms\RoomEmployeeServiceSchedules', $setResult);
        $getResult = $roomEmployeeServiceSchedules->getServiceObject();
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $getResult);
    }
}