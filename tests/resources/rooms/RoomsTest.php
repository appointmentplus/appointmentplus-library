<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 12:08 PM
 */

use apptlibrary\resources\rooms\Rooms;

/**
 * Class RoomsTest
 */
class RoomsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $rooms = new Rooms();
        $this->assertInstanceOf('apptlibrary\resources\rooms\Rooms', $rooms);
    }
}