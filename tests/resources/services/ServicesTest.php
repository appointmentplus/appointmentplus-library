<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 12:53 PM
 */

use apptlibrary\resources\services\Services;
use apptlibrary\resources\services\ServiceSchedules;

/**
 * Class ServicesTest
 */
class ServicesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $services = new Services();
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $services);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 806276;
        $setResult = $services->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 34347;
        $setResult = $services->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Title setter and getter
     */
    public function testTitleSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = '3mek8v 17hdw7aq';
        $setResult = $services->setTitle($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getTitle();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 'a6rb62uir2g6sy';
        $setResult = $services->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Duration setter and getter
     */
    public function testDurationSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 404268;
        $setResult = $services->setDuration($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getDuration();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Cost setter and getter
     */
    public function testCostSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 81.26;
        $setResult = $services->setCost($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getCost();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TypeId setter and getter
     */
    public function testTypeIdSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 271778;
        $setResult = $services->setTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 563795;
        $setResult = $services->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DisplayToCustomer setter and getter
     */
    public function testDisplayToCustomerSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = false;
        $setResult = $services->setDisplayToCustomer($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getDisplayToCustomer();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberOfSpots setter and getter
     */
    public function testNumberOfSpotsSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 484535;
        $setResult = $services->setNumberOfSpots($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getNumberOfSpots();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ProductCode setter and getter
     */
    public function testProductCodeSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 's15ewhei';
        $setResult = $services->setProductCode($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getProductCode();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Taxable setter and getter
     */
    public function testTaxableSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = true;
        $setResult = $services->setTaxable($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getTaxable();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ProductCategoryId setter and getter
     */
    public function testProductCategoryIdSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = 451873;
        $setResult = $services->setProductCategoryId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getProductCategoryId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CalculateServiceSpots setter and getter
     */
    public function testCalculateServiceSpotsSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = true;
        $setResult = $services->setCalculateServiceSpots($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getCalculateServiceSpots();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceScheduleObjects setter and getter
     */
    public function testServiceScheduleObjectsSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = array(new ServiceSchedules());
        $setResult = $services->setServiceScheduleObjects($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getServiceScheduleObjects();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new ServiceSchedules();
        $addResult = $services->addServiceScheduleObjects($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $addResult);
        $getResult = $services->getServiceScheduleObjects();
        $this->assertCount(2, $getResult);
    }

    /**
     * Test the AssignedServiceIds setter and getter
     */
    public function testAssignedServiceIdsSetterAndGetter()
    {
        $services = new Services();
        $expectedValue = array();
        $setResult = $services->setAssignedServiceIds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $setResult);
        $getResult = $services->getAssignedServiceIds();
        $this->assertEquals($expectedValue, $getResult);
    }
}