<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 12:10 PM
 */

use apptlibrary\resources\services\ServiceRelationships;
use apptlibrary\resources\services\Services;

/**
 * Class ServiceRelationshipsTest
 */
class ServiceRelationshipsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $serviceRelationships = new ServiceRelationships();
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $serviceRelationships);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $serviceRelationships = new ServiceRelationships();
        $expectedValue = 977079;
        $setResult = $serviceRelationships->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $setResult);
        $getResult = $serviceRelationships->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceId setter and getter
     */
    public function testServiceIdSetterAndGetter()
    {
        $serviceRelationships = new ServiceRelationships();
        $expectedValue = 451043;
        $setResult = $serviceRelationships->setServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $setResult);
        $getResult = $serviceRelationships->getServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SubServiceId setter and getter
     */
    public function testSubServiceIdSetterAndGetter()
    {
        $serviceRelationships = new ServiceRelationships();
        $expectedValue = 320326;
        $setResult = $serviceRelationships->setSubServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $setResult);
        $getResult = $serviceRelationships->getSubServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceObject setter and getter
     */
    public function testServiceObjectSetterAndGetter()
    {
        $serviceRelationships = new ServiceRelationships();
        $expectedValue = new Services();
        $setResult = $serviceRelationships->setServiceObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $setResult);
        $getResult = $serviceRelationships->getServiceObject();
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $getResult);
    }

    /**
     * Test the SubServiceObject setter and getter
     */
    public function testSubServiceObjectSetterAndGetter()
    {
        $serviceRelationships = new ServiceRelationships();
        $expectedValue = new Services();
        $setResult = $serviceRelationships->setSubServiceObject($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceRelationships', $setResult);
        $getResult = $serviceRelationships->getSubServiceObject();
        $this->assertInstanceOf('apptlibrary\resources\services\Services', $getResult);
    }
}