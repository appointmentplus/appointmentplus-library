<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 2:48 PM
 */

use apptlibrary\resources\services\ServiceSchedules;

/**
 * Class ServiceSchedulesTest
 */
class ServiceSchedulesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $serviceSchedules = new ServiceSchedules();
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $serviceSchedules);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 1091;
        $setResult = $serviceSchedules->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ServiceId setter and getter
     */
    public function testServiceIdSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 2323;
        $setResult = $serviceSchedules->setServiceId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getServiceId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayStartTime setter and getter
     */
    public function testScheduleMondayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 454;
        $setResult = $serviceSchedules->setScheduleMondayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleMondayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleMondayEndTime setter and getter
     */
    public function testScheduleMondayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 678;
        $setResult = $serviceSchedules->setScheduleMondayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleMondayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayStartTime setter and getter
     */
    public function testScheduleTuesdayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 303;
        $setResult = $serviceSchedules->setScheduleTuesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleTuesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleTuesdayEndTime setter and getter
     */
    public function testScheduleTuesdayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 2348;
        $setResult = $serviceSchedules->setScheduleTuesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleTuesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayStartTime setter and getter
     */
    public function testScheduleWednesdayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 900;
        $setResult = $serviceSchedules->setScheduleWednesdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleWednesdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleWednesdayEndTime setter and getter
     */
    public function testScheduleWednesdayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 2308;
        $setResult = $serviceSchedules->setScheduleWednesdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleWednesdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayStartTime setter and getter
     */
    public function testScheduleThursdayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 2348;
        $setResult = $serviceSchedules->setScheduleThursdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleThursdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleThursdayEndTime setter and getter
     */
    public function testScheduleThursdayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 425;
        $setResult = $serviceSchedules->setScheduleThursdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleThursdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayStartTime setter and getter
     */
    public function testScheduleFridayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 293;
        $setResult = $serviceSchedules->setScheduleFridayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleFridayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleFridayEndTime setter and getter
     */
    public function testScheduleFridayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 273;
        $setResult = $serviceSchedules->setScheduleFridayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleFridayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayStartTime setter and getter
     */
    public function testScheduleSaturdayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 42;
        $setResult = $serviceSchedules->setScheduleSaturdayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleSaturdayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSaturdayEndTime setter and getter
     */
    public function testScheduleSaturdayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 1804;
        $setResult = $serviceSchedules->setScheduleSaturdayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleSaturdayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayStartTime setter and getter
     */
    public function testScheduleSundayStartTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 94;
        $setResult = $serviceSchedules->setScheduleSundayStartTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleSundayStartTime();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ScheduleSundayEndTime setter and getter
     */
    public function testScheduleSundayEndTimeSetterAndGetter()
    {
        $serviceSchedules = new ServiceSchedules();
        $expectedValue = 1023;
        $setResult = $serviceSchedules->setScheduleSundayEndTime($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\services\ServiceSchedules', $setResult);
        $getResult = $serviceSchedules->getScheduleSundayEndTime();
        $this->assertEquals($expectedValue, $getResult);
    }
}