<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 14:08
 */

use apptlibrary\resources\mobilecarriers\MobileCarriers;

/**
 * Class MobileCarriersTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class MobileCarriersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $mobileCarriers = new MobileCarriers();
        $this->assertInstanceOf('apptlibrary\resources\mobilecarriers\MobileCarriers', $mobileCarriers);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $mobileCarriers = new MobileCarriers();
        $expectedValue = 835001;
        $setResult = $mobileCarriers->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\mobilecarriers\MobileCarriers', $setResult);
        $getResult = $mobileCarriers->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Description setter and getter
     */
    public function testDescriptionSetterAndGetter()
    {
        $mobileCarriers = new MobileCarriers();
        $expectedValue = 'txwg5 l1yjed';
        $setResult = $mobileCarriers->setDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\mobilecarriers\MobileCarriers', $setResult);
        $getResult = $mobileCarriers->getDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CarrierAddress setter and getter
     */
    public function testCarrierAddressSetterAndGetter()
    {
        $mobileCarriers = new MobileCarriers();
        $expectedValue = 'jfv0ytqo5t';
        $setResult = $mobileCarriers->setCarrierAddress($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\mobilecarriers\MobileCarriers', $setResult);
        $getResult = $mobileCarriers->getCarrierAddress();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CountryId setter and getter
     */
    public function testCountryIdSetterAndGetter()
    {
        $mobileCarriers = new MobileCarriers();
        $expectedValue = 327612;
        $setResult = $mobileCarriers->setCountryId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\mobilecarriers\MobileCarriers', $setResult);
        $getResult = $mobileCarriers->getCountryId();
        $this->assertEquals($expectedValue, $getResult);
    }
}