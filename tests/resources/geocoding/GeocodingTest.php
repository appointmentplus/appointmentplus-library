<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 14:01
 */

use apptlibrary\resources\geocoding\Geocoding;

/**
 * Class GeocodingTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class GeocodingTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $geocoding = new Geocoding();
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $geocoding);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $geocoding = new Geocoding();
        $expectedValue = 263497;
        $setResult = $geocoding->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $setResult);
        $getResult = $geocoding->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SearchQuery setter and getter
     */
    public function testSearchQuerySetterAndGetter()
    {
        $geocoding = new Geocoding();
        $expectedValue = '0viwqxes3z9d';
        $setResult = $geocoding->setSearchQuery($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $setResult);
        $getResult = $geocoding->getSearchQuery();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Latitude setter and getter
     */
    public function testLatitudeSetterAndGetter()
    {
        $geocoding = new Geocoding();
        $expectedValue = 51.29;
        $setResult = $geocoding->setLatitude($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $setResult);
        $getResult = $geocoding->getLatitude();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Longitude setter and getter
     */
    public function testLongitudeSetterAndGetter()
    {
        $geocoding = new Geocoding();
        $expectedValue = 90.96;
        $setResult = $geocoding->setLongitude($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $setResult);
        $getResult = $geocoding->getLongitude();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenAdded setter and getter
     */
    public function testWhenAddedSetterAndGetter()
    {
        $geocoding = new Geocoding();
        $expectedValue = 'wrwt2eoll1wm8d ';
        $setResult = $geocoding->setWhenAdded($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\geocoding\Geocoding', $setResult);
        $getResult = $geocoding->getWhenAdded();
        $this->assertEquals($expectedValue, $getResult);
    }
}