<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 15:52
 */

use apptlibrary\resources\packages\PackageTemplates;

/**
 * Class PackageTemplatesTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class PackageTemplatesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $packageTemplates = new PackageTemplates();
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $packageTemplates);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 888327;
        $setResult = $packageTemplates->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 293790;
        $setResult = $packageTemplates->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Name setter and getter
     */
    public function testNameSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = '4gne ktdiztrc';
        $setResult = $packageTemplates->setName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Price setter and getter
     */
    public function testPriceSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 7.87;
        $setResult = $packageTemplates->setPrice($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getPrice();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Status setter and getter
     */
    public function testStatusSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = PackageTemplates::ACTIVE;
        $setResult = $packageTemplates->setStatus($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getStatus();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CostPerSession setter and getter
     */
    public function testCostPerSessionSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 42.46;
        $setResult = $packageTemplates->setCostPerSession($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getCostPerSession();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MonthlyFee setter and getter
     */
    public function testMonthlyFeeSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 39.70;
        $setResult = $packageTemplates->setMonthlyFee($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getMonthlyFee();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberOfSessions setter and getter
     */
    public function testNumberOfSessionsSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 1.82;
        $setResult = $packageTemplates->setNumberOfSessions($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getNumberOfSessions();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberOfDays setter and getter
     */
    public function testNumberOfDaysSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 91706;
        $setResult = $packageTemplates->setNumberOfDays($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getNumberOfDays();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TypeId setter and getter
     */
    public function testTypeIdSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = PackageTemplates::TYPE_DOLLAR_AMNT;
        $setResult = $packageTemplates->setTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EnrollmentFee setter and getter
     */
    public function testEnrollmentFeeSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = '0v1dn4';
        $setResult = $packageTemplates->setEnrollmentFee($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getEnrollmentFee();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DownPayment setter and getter
     */
    public function testDownPaymentSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = 38.72;
        $setResult = $packageTemplates->setDownPayment($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getDownPayment();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PackageServiceIds setter and getter
     */
    public function testPackageServiceIdsSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = array();
        $setResult = $packageTemplates->setPackageServiceIds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getPackageServiceIds();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PackageStatusIds setter and getter
     */
    public function testPackageStatusIdsSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = array();
        $setResult = $packageTemplates->setPackageStatusIds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getPackageStatusIds();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Taxable setter and getter
     */
    public function testTaxableSetterAndGetter()
    {
        $packageTemplates = new PackageTemplates();
        $expectedValue = false;
        $setResult = $packageTemplates->setTaxable($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\PackageTemplates', $setResult);
        $getResult = $packageTemplates->getTaxable();
        $this->assertEquals($expectedValue, $getResult);
    }
}