<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 28/04/15
 * Time: 14:21
 */

use apptlibrary\resources\packages\Packages;

/**
 * Class PackagesTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class PackagesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $packages = new Packages();
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $packages);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 219750;
        $setResult = $packages->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerId setter and getter
     */
    public function testCustomerIdSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 806094;
        $setResult = $packages->setCustomerId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getCustomerId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Name setter and getter
     */
    public function testNameSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 'xolmu n1w2vy';
        $setResult = $packages->setName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Price setter and getter
     */
    public function testPriceSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 84.3;
        $setResult = $packages->setPrice($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getPrice();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Status setter and getter
     */
    public function testStatusSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = Packages::DELETED;
        $setResult = $packages->setStatus($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getStatus();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CostPerSession setter and getter
     */
    public function testCostPerSessionSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 20.91;
        $setResult = $packages->setCostPerSession($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getCostPerSession();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the MonthlyFee setter and getter
     */
    public function testMonthlyFeeSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 90.83;
        $setResult = $packages->setMonthlyFee($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getMonthlyFee();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberOfSessions setter and getter
     */
    public function testNumberOfSessionsSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 31.41;
        $setResult = $packages->setNumberOfSessions($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getNumberOfSessions();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberOfDays setter and getter
     */
    public function testNumberOfDaysSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 501229;
        $setResult = $packages->setNumberOfDays($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getNumberOfDays();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PackageTypeId setter and getter
     */
    public function testPackageTypeIdSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = Packages::TYPE_DOLLAR_AMNT;
        $setResult = $packages->setPackageTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getPackageTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EnrollmentFee setter and getter
     */
    public function testEnrollmentFeeSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 83.4;
        $setResult = $packages->setEnrollmentFee($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getEnrollmentFee();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DownPayment setter and getter
     */
    public function testDownPaymentSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = 90.24;
        $setResult = $packages->setDownPayment($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getDownPayment();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PackageStatusIds setter and getter
     */
    public function testPackageStatusIdsSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = array();
        $setResult = $packages->setPackageStatusIds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getPackageStatusIds();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PackageServiceIds setter and getter
     */
    public function testPackageServiceIdsSetterAndGetter()
    {
        $packages = new Packages();
        $expectedValue = array();
        $setResult = $packages->setPackageServiceIds($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\packages\Packages', $setResult);
        $getResult = $packages->getPackageServiceIds();
        $this->assertEquals($expectedValue, $getResult);
    }
}