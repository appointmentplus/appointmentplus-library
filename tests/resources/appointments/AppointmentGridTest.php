<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/22/2015
 * Time: 4:19 PM
 */

use apptlibrary\resources\appointments\AppointmentGrid;

/**
 * Class AppointmentGridTest
 */
class AppointmentGridTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $appointmentGrid = new AppointmentGrid();
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $appointmentGrid);
    }

    /**
     * Test the start timestamp setter and getter
     */
    public function testStartTimestampSetterAndGetter()
    {
        $appointmentGrid = new AppointmentGrid();
        $expectedValue = '1212018372.3366';
        $setResult = $appointmentGrid->setStartTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $setResult);
        $getResult = $appointmentGrid->getStartTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the end timestamp setter and getter
     */
    public function testEndTimestampSetterAndGetter()
    {
        $appointmentGrid = new AppointmentGrid();
        $expectedValue = '1212018372.3366';
        $setResult = $appointmentGrid->setEndTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $setResult);
        $getResult = $appointmentGrid->getEndTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the slots setter and getter
     */
    public function testSlotsSetterAndGetter()
    {
        $this->markTestSkipped('UNDEFINED: Slot object hasn\'t been defined');
        $appointmentGrid = new AppointmentGrid();
        $expectedValue = array(new Slot());
        $setResult = $appointmentGrid->setSlots($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $setResult);
        $getResult = $appointmentGrid->getSlots();
        $this->assertEquals($expectedValue, $getResult);
        $expectedAddedValue = new Slot();
        $addResult = $appointmentGrid->addSlot($expectedAddedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\AppointmentGrid', $addResult);
        $getResult = $appointmentGrid->getSlots();
        $this->assertCount(2, $getResult);
    }
}