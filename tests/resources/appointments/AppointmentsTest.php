<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/23/2015
 * Time: 4:20 PM
 */

use apptlibrary\resources\appointments\Appointments;

/**
 * Class AppointmentsTest
 */
class AppointmentsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test class instantiation
     */
    public function testCreateTests()
    {
        $appointments = new Appointments();
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $appointments);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 958248;
        $setResult = $appointments->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ClientId setter and getter
     */
    public function testClientIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 114886;
        $setResult = $appointments->setClientId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getClientId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerId setter and getter
     */
    public function testCustomerIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 324775;
        $setResult = $appointments->setCustomerId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCustomerId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeId setter and getter
     */
    public function testEmployeeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 599670;
        $setResult = $appointments->setEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the RoomId setter and getter
     */
    public function testRoomIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 955225;
        $setResult = $appointments->setRoomId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getRoomId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StartTimestamp setter and getter
     */
    public function testStartTimestampSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'epr9rodt8if';
        $setResult = $appointments->setStartTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getStartTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EndTimestamp setter and getter
     */
    public function testEndTimestampSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = '2hg0wwl';
        $setResult = $appointments->setEndTimestamp($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getEndTimestamp();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Services setter and getter
     */
    public function testServicesSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = array();
        $setResult = $appointments->setServices($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getServices();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerNotes setter and getter
     */
    public function testCustomerNotesSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'jzdeosd47vlplp';
        $setResult = $appointments->setCustomerNotes($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCustomerNotes();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the EmployeeNotes setter and getter
     */
    public function testEmployeeNotesSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'e9hmv0pwy9hw';
        $setResult = $appointments->setEmployeeNotes($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getEmployeeNotes();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the StatusId setter and getter
     */
    public function testStatusIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 830116;
        $setResult = $appointments->setStatusId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getStatusId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PaymentEmployeeId setter and getter
     */
    public function testPaymentEmployeeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 314538;
        $setResult = $appointments->setPaymentEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getPaymentEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CostAmount setter and getter
     */
    public function testCostAmountSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 38.45;
        $setResult = $appointments->setCostAmount($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCostAmount();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TipAmount setter and getter
     */
    public function testTipAmountSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 57.31;
        $setResult = $appointments->setTipAmount($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getTipAmount();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PaymentTypeId setter and getter
     */
    public function testPaymentTypeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 190233;
        $setResult = $appointments->setPaymentTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getPaymentTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CouponId setter and getter
     */
    public function testCouponIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 482186;
        $setResult = $appointments->setCouponId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCouponId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the TypeId setter and getter
     */
    public function testTypeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 44875;
        $setResult = $appointments->setTypeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getTypeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the NumberInGroup setter and getter
     */
    public function testNumberInGroupSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 130758;
        $setResult = $appointments->setNumberInGroup($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getNumberInGroup();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the RecurringId setter and getter
     */
    public function testRecurringIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 529665;
        $setResult = $appointments->setRecurringId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getRecurringId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ReserveDescription setter and getter
     */
    public function testReserveDescriptionSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'srn9ymsgi2';
        $setResult = $appointments->setReserveDescription($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getReserveDescription();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CancellationReason setter and getter
     */
    public function testCancellationReasonSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'pd1ajmw9';
        $setResult = $appointments->setCancellationReason($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCancellationReason();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenAdded setter and getter
     */
    public function testWhenAddedSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'ibfiv4i8o1x ro';
        $setResult = $appointments->setWhenAdded($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getWhenAdded();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the WhenUpdated setter and getter
     */
    public function testWhenUpdatedSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = '88y1n  ';
        $setResult = $appointments->setWhenUpdated($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getWhenUpdated();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CreatedByEmployeeId setter and getter
     */
    public function testCreatedByEmployeeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 684720;
        $setResult = $appointments->setCreatedByEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCreatedByEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LastUpdatedByEmployeeId setter and getter
     */
    public function testLastUpdatedByEmployeeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 576536;
        $setResult = $appointments->setLastUpdatedByEmployeeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getLastUpdatedByEmployeeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CustomerPackageId setter and getter
     */
    public function testCustomerPackageIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 161271;
        $setResult = $appointments->setCustomerPackageId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCustomerPackageId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DurationId setter and getter
     */
    public function testDurationIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 103170;
        $setResult = $appointments->setDurationId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getDurationId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the AssociatedId setter and getter
     */
    public function testAssociatedIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 869755;
        $setResult = $appointments->setAssociatedId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getAssociatedId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the VehicleMakeId setter and getter
     */
    public function testVehicleMakeIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 999033;
        $setResult = $appointments->setVehicleMakeId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getVehicleMakeId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the VehicleModelId setter and getter
     */
    public function testVehicleModelIdSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 280525;
        $setResult = $appointments->setVehicleModelId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getVehicleModelId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the VehicleOther setter and getter
     */
    public function testVehicleOtherSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'bti zhocj3s7uk2';
        $setResult = $appointments->setVehicleOther($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getVehicleOther();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ModelYear setter and getter
     */
    public function testModelYearSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 645854;
        $setResult = $appointments->setModelYear($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getModelYear();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CertNumber setter and getter
     */
    public function testCertNumberSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 't6 5jiyhl';
        $setResult = $appointments->setCertNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCertNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the CertState setter and getter
     */
    public function testCertStateSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = '4uux286 ';
        $setResult = $appointments->setCertState($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getCertState();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Vin setter and getter
     */
    public function testVinSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'ofpce54';
        $setResult = $appointments->setVin($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getVin();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DealerName setter and getter
     */
    public function testDealerNameSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'agssss09a';
        $setResult = $appointments->setDealerName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getDealerName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the DealerAddress setter and getter
     */
    public function testDealerAddressSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'g w9xd9sn26u46r';
        $setResult = $appointments->setDealerAddress($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getDealerAddress();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SalvageReason setter and getter
     */
    public function testSalvageReasonSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'der7isn8bb ';
        $setResult = $appointments->setSalvageReason($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getSalvageReason();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Odometer setter and getter
     */
    public function testOdometerSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'a6 c76u8wfgkj';
        $setResult = $appointments->setOdometer($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getOdometer();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the PoNumber setter and getter
     */
    public function testPoNumberSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'sk9tovrco6o';
        $setResult = $appointments->setPoNumber($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getPoNumber();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the Gender setter and getter
     */
    public function testGenderSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 143778;
        $setResult = $appointments->setGender($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getGender();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the FollowUpDate setter and getter
     */
    public function testFollowUpDateSetterAndGetter()
    {
        $appointments = new Appointments();
        $expectedValue = 'bdzi imyfm8y9p1';
        $setResult = $appointments->setFollowUpDate($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\appointments\Appointments', $setResult);
        $getResult = $appointments->getFollowUpDate();
        $this->assertEquals($expectedValue, $getResult);
    }

}