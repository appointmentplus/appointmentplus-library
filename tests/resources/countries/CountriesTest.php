<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/24/2015
 * Time: 1:20 PM
 */

use apptlibrary\resources\countries\Countries;

class CountriesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $countries = new Countries();
        $this->assertInstanceOf('apptlibrary\resources\countries\Countries', $countries);
    }

    /**
     * Test the Id setter and getter
     */
    public function testIdSetterAndGetter()
    {
        $countries = new Countries();
        $expectedValue = 571546;
        $setResult = $countries->setId($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\countries\Countries', $setResult);
        $getResult = $countries->getId();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the ShortName setter and getter
     */
    public function testShortNameSetterAndGetter()
    {
        $countries = new Countries();
        $expectedValue = '4ewli9judektxbv';
        $setResult = $countries->setShortName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\countries\Countries', $setResult);
        $getResult = $countries->getShortName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the LongName setter and getter
     */
    public function testLongNameSetterAndGetter()
    {
        $countries = new Countries();
        $expectedValue = '5sgknk';
        $setResult = $countries->setLongName($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\countries\Countries', $setResult);
        $getResult = $countries->getLongName();
        $this->assertEquals($expectedValue, $getResult);
    }

    /**
     * Test the SortOrder setter and getter
     */
    public function testSortOrderSetterAndGetter()
    {
        $countries = new Countries();
        $expectedValue = 992000;
        $setResult = $countries->setSortOrder($expectedValue);
        $this->assertInstanceOf('apptlibrary\resources\countries\Countries', $setResult);
        $getResult = $countries->getSortOrder();
        $this->assertEquals($expectedValue, $getResult);
    }
}