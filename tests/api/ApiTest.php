<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 5/4/2015
 * Time: 12:11 PM
 */

use apptlibrary\api\Api;


/**
 * Class ApiTest
 */
class ApiTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Create a mock for the request engine
     * @return PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequestEngineMock()
    {
        $reqEngine = $this->getMockBuilder('\apptlibrary\request\IRequest')
            ->setMethods(array('request', 'setParameters', 'setAction'))
            ->getMock();
        return $reqEngine;
    }

    protected function getRequestEngineMockWithWantedResponse($expectedResult)
    {
        $reqEngMock = $this->getRequestEngineMock();
        $reqEngMock->expects($this->once())
            ->method('request')
            ->will($this->returnValue($expectedResult));
        return $reqEngMock;
    }

    /**
     * Test the API constructor with and without parameters
     */
    public function testConstructor()
    {
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMock();
        $api2 = new Api($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $api2);
        $expectedReqEngineClass = '\apptlibrary\request\IRequest';
        $reqEngine = $api2->getRequestEngine();
        $this->assertInstanceOf($expectedReqEngineClass, $reqEngine);
    }

    /**
     * Tests the request engine setter and getter
     */
    public function testRequestEngineSetterAndGetter()
    {
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngine = $api->getRequestEngine();
        $this->assertEquals(NULL, $reqEngine);
        $reqEngMock = $this->getRequestEngineMock();
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $expectedReqEngineClass = '\apptlibrary\request\IRequest';
        $reqEngine = $api->getRequestEngine();
        $this->assertInstanceOf($expectedReqEngineClass, $reqEngine);
    }

    /**
     * Test the token setter and getter
     */
    public function testTokenSetterAndGetter()
    {
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $returnedToken = $api->getToken();
        $this->assertEquals(NULL, $returnedToken);
        $expectedToken = 'e465a5cce5aa67bc2d89d109a4020e44b1851f3b83786d9d828f1aff085609ef';
        $returnedValue = $api->setToken($expectedToken);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $returnedToken = $api->getToken();
        $this->assertEquals($expectedToken, $returnedToken);
    }

    /**
     * Test the sendRequest method
     */
    public function testSendRequest()
    {
        $expectedResult = 'Success';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->sendRequest('myAction', array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the sendRequest method
     */
    public function testGetVersion()
    {
        $expectedResult = 'GetVersion';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->getVersion(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the getClientInterfaces method
     */
    public function testGetClientInterfaces()
    {
        $expectedResult = 'GetClientInterfaces';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->getClientInterfaces(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the getAuthenticationToken method
     */
    public function testGetAuthenticationToken()
    {
        $expectedResult = 'GetAuthenticationToken';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->getAuthenticationToken(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the expireAuthenticationToken method
     */
    public function testExpireAuthenticationToken()
    {
        $expectedResult = 'ExpireAuthenticationToken';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->expireAuthenticationToken(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the keepAlive method
     */
    public function testKeepAlive()
    {
        $expectedResult = 'KeepAlive';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->KeepAlive(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the getClientInterfaces method
     */
    public function testGetItem()
    {
        $expectedResult = 'GetItem';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->getItem(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the getItemsByFilter method
     */
    public function testGetItemsByFilter()
    {
        $expectedResult = 'GetItemsByFilter';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->getItemsByFilter(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the createItem method
     */
    public function testCreateItem()
    {
        $expectedResult = 'CreateItem';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->createItem(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the updateItem method
     */
    public function testUpdateItem()
    {
        $expectedResult = 'UpdateItem';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->updateItem(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the deleteItem method
     */
    public function testDeleteItem()
    {
        $expectedResult = 'DeleteItem';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->deleteItem(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Test the aggregate method
     */
    public function testAggregate()
    {
        $expectedResult = 'Aggregate';
        $api = new Api();
        $this->assertInstanceOf('\apptlibrary\api\Api', $api);
        $reqEngMock = $this->getRequestEngineMockWithWantedResponse($expectedResult);
        $returnedValue = $api->setRequestEngine($reqEngMock);
        $this->assertInstanceOf('\apptlibrary\api\Api', $returnedValue);
        $result = $api->aggregate(array('param1' => 'value1'));
        $this->assertEquals($expectedResult, $result);
    }
}