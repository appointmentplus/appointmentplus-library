<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 5/4/2015
 * Time: 11:49 AM
 */

use apptlibrary\api\Factory;
use apptlibrary\request\rest\Rest;

/**
 * Class FactoryTest
 */
class APIFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $expectedClass = '\apptlibrary\api\Factory';
        $reflectionClass = new ReflectionClass($expectedClass);
        $isInstantiable = $reflectionClass->isInstantiable();
        $this->assertEquals(false, $isInstantiable);
        $classInstance = Factory::getInstance();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $this->assertInstanceOf('\apptlibrary\resources\designpatterns\IFactory', $classInstance);
    }

    /**
     * Test the builder fpr a REST request
     * @expectedException \Exception
     * @expectedExceptionMessage 'url' option is required when request building of REST
     */
    public function testRestBuilder()
    {
        $classInstance = Factory::build(Rest::WS_REST_TYPE, array('url' => 'https://development.appointment-plus.com/mobileapi/v1/Rest'));
        $expectedClass = '\apptlibrary\api\Api';
        $this->assertInstanceOf($expectedClass, $classInstance);
        $classInstance2 = Factory::build(Rest::WS_REST_TYPE);
    }
}