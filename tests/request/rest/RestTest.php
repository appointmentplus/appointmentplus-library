<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 5/4/2015
 * Time: 6:40 PM
 */

use apptlibrary\request\rest\Rest;
use \apptlibrary\request\IRequest;
use \Exception;

/**
 * Class RestTest
 */
class RestTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the constructor
     * @expectedException \Exception
     * @expectedExceptionMessage Provided URL is not valid (http//invalidurl)
     */
    public function testConstructor()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $object2 = new Rest('http//invalidurl');
    }

    /**
     * Test the parameters setter and getter
     */
    public function testParametersSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = array('param' => 'value');
        $returnedObject = $object->setParameters($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $parameters = $object->getParameters();
        $this->assertEquals($expectedValue, $parameters);
    }

    /**
     * Test the action setter and getter
     */
    public function testActionSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = 'AnAction';
        $returnedObject = $object->setAction($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $action = $object->getAction();
        $this->assertEquals($expectedValue, $action);
    }

    /**
     * Test the service type getter
     */
    public function testServiceTypeGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = IRequest::WS_REST_TYPE;
        $serviceType = $object->getServiceType();
        $this->assertEquals($expectedValue, $serviceType);
    }

    /**
     * Test the CURL handler setter and getter
     */
    public function testCURLHandleSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = curl_init();
        $returnedObject = $object->setCURLHandle($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $cURLHandle = $object->getCURLHandle();
        $this->assertEquals($expectedValue, $cURLHandle);
    }

    /**
     * Test the URL setter and getter
     * @expectedException \Exception
     * @expectedExceptionMessage URL is not valid (http//invalidurl)
     */
    public function testURLSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = 'http://example.com';
        $returnedObject = $object->setURL($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $url = $object->getURL();
        $this->assertEquals($expectedValue, $url);
        $object->setURL('http//invalidurl');
    }

    /**
     * Test the interface setter and getter
     */
    public function testInterfaceSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = 'Inter face';
        $returnedObject = $object->setInterface($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $interface = $object->getInterface();
        $this->assertEquals($expectedValue, $interface);
    }

    /**
     * Test the callType setter and getter
     * @expectedException Exception
     * @expectedExceptionMessage Call type expected to be IRequest::CALL_TYPE_JSON or IRequest::CALL_TYPE_XML
     */
    public function testCallTypeSetterAndGetter()
    {
        $expectedClass = '\apptlibrary\request\rest\Rest';
        $object = new Rest();
        $this->assertInstanceOf($expectedClass, $object);
        $expectedValue = IRequest::CALL_TYPE_JSON;
        $returnedObject = $object->setCallType($expectedValue);
        $this->assertInstanceOf($expectedClass, $returnedObject);
        $callType = $object->getCallType();
        $this->assertEquals($expectedValue, $callType);
        $object->setCallType('INVALID');
    }
}