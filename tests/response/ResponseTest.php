<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/30/2015
 * Time: 2:05 PM
 */

use apptlibrary\response\Response;
use apptlibrary\response\responsecodes\IResponse as RCIResponse;

/**
 * Class ResponseTest
 */
class ResponseTest extends \PHPUnit_Framework_TestCase
{
    protected $mockResponseData = '{"status":0,"statusMessage":"Authentication token successfully returned","details":{"employeeMasterId":7337,"clientMasterId":391911,"clientId":1657,"id":16291,"authenticationEntityId":1,"interfaceId":7,"token":"c0b2b7c7c367555917be34f864e96cd90420f9b33afc5cc6b59084781b2f12d6","apiUrl":"https:\/\/development.appointment-plus.com\/mobileapi\/v1\/","createdTimestamp":"2015-04-30 12:07:26","expireTimestamp":"2015-04-30 13:07:26","authenticationEntityObject":{"description":"AppointmentPlusEmployee","loginUrl":null,"inactivityUrl":null,"logoutUrl":null}}}';

    protected function getDataObject()
    {
        return json_decode($this->mockResponseData, false);
    }

    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $response = new Response();
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
    }

    /**
     * Test the response type getter
     */
    public function testResponseTypeGetter()
    {
        $response = new Response($this->mockResponseData);
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
        $expectedResponseType = Response::DATA_JSON;
        $responseType = $response->getResponseType();
        $this->assertEquals($expectedResponseType, $responseType);
    }

    /**
     * Tests the status setter and getter as well as the exception thrown
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid status given (Invalid). Expecting one of : \apptlibrary\response\responsecodes\IResponse::STATUS_SUCCESS, \apptlibrary\response\responsecodes\IResponse::STATUS_ERROR, \apptlibrary\response\responsecodes\IResponse::STATUS_AUTH_FAIL, \apptlibrary\response\responsecodes\IResponse::STATUS_ACCESS_NOT_ALLOWED, \apptlibrary\response\responsecodes\IResponse::STATUS_BUSY, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_XML_DATA_REQ, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_JSON_DATA_REQ, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_OBJ_DATA_REQ, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_CALL_TYPE, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_METHOD, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_RESOURCE, \apptlibrary\response\responsecodes\IResponse::STATUS_INVALID_PARAMETER, \apptlibrary\response\responsecodes\IResponse::STATUS_VALIDATION_ERROR, \apptlibrary\response\responsecodes\IResponse::STATUS_UNKNOWN_ERROR, \apptlibrary\response\responsecodes\IResponse::STATUS_NOT_YET_IMPLEMENTED
     */
    public function testStatusSetterGetterAndException()
    {
        $response = new Response();
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
        $expectedStatus = RCIResponse::STATUS_UNKNOWN_ERROR;
        $returnedObj = $response->setStatus($expectedStatus);
        $this->assertInstanceOf('apptlibrary\response\Response', $returnedObj);
        $returnedStatus = $response->getStatus();
        $this->assertEquals($expectedStatus, $returnedStatus);
        $response->setStatus('Invalid');
    }

    /**
     * Tests the status message setter and getter
     */
    public function testStatusMessageSetterAndGetter()
    {
        $response = new Response();
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
        $expectedStatus = 'My expected message';
        $returnedObj = $response->setStatusMessage($expectedStatus);
        $this->assertInstanceOf('apptlibrary\response\Response', $returnedObj);
        $returnedStatus = $response->getStatusMessage();
        $this->assertEquals($expectedStatus, $returnedStatus);
    }

    /**
     * Tests the details setter and getter
     */
    public function testDetailsSetterAndGetter()
    {
        $response = new Response();
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
        $expectedDetails = 'My expected details';
        $returnedObj = $response->setDetails($expectedDetails);
        $this->assertInstanceOf('apptlibrary\response\Response', $returnedObj);
        $returnedDetails = $response->getDetails();
        $this->assertEquals($expectedDetails, $returnedDetails);
    }

    /**
     * Test the setValues method and the exception
     * @throws Exception
     * @expectedException \Exception
     */
    public function testSetValues()
    {
        $response = new Response();
        $this->assertInstanceOf('apptlibrary\response\Response', $response);
        $expectedObject = $this->getDataObject();
        $response->setValues($this->mockResponseData);
        $expectedResponseType = Response::DATA_JSON;
        $responseType = $response->getResponseType();
        $this->assertEquals($expectedResponseType, $responseType);
        $this->assertEquals($expectedObject->status, $response->getStatus());
        $this->assertEquals($expectedObject->statusMessage, $response->getStatusMessage());
        $this->assertEquals($expectedObject->details, $response->getDetails());
        $response->setValues('Invalid');
    }
}