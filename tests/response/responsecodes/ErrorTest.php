<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 6:05 PM
 */

use apptlibrary\response\responsecodes\Error;

/**
 * Class ErrorTest
 */
class ErrorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\Error';
        $classInstance = new Error();
        $this->assertInstanceOf($expectedClass, $classInstance);
    }

    /**
     * Returns the expected status codes array
     * @return array
     */
    protected function getExpectedStatusCodesArray()
    {
        $expectedStatusCodes[Error::STATUS_ERROR] = 'An error has occurred';
        $expectedStatusCodes[Error::STATUS_INVALID_CALL_TYPE] = 'Invalid call type provided';
        $expectedStatusCodes[Error::STATUS_INVALID_JSON_DATA_REQ] = 'Invalid JSON request (data) provided';
        $expectedStatusCodes[Error::STATUS_INVALID_METHOD] = 'Invalid method provided';
        $expectedStatusCodes[Error::STATUS_INVALID_OBJ_DATA_REQ] = 'Invalid object request (data) provided';
        $expectedStatusCodes[Error::STATUS_INVALID_PARAMETER] = 'Invalid parameter provided';
        $expectedStatusCodes[Error::STATUS_INVALID_RESOURCE] = 'Invalid resource provided';
        $expectedStatusCodes[Error::STATUS_INVALID_XML_DATA_REQ] = 'Invalid XML request (data) provided';
        $expectedStatusCodes[Error::STATUS_VALIDATION_ERROR] = 'Validation error has occurred';
        return $expectedStatusCodes;
    }

    /**
     * Test the status codes and the exception if a wrong status code is provided
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid code. Got status code 'invalid' (expected one of the following integer values: 1, 10, 7, 20, 8, 25, 21, 6, 50)
     */
    public function testStatusCodes()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\Error';
        $classInstance = new Error();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $statusCodes = $classInstance->getStatusCodes();
        $expectedStatusCodes = $this->getExpectedStatusCodesArray();
        $this->assertEquals($expectedStatusCodes, $statusCodes);
        foreach ($statusCodes as $statusCode => $statusMessage)
        {
            $message = $classInstance->getResponseCodeMessage($statusCode);
            $expectedMessage = $expectedStatusCodes[$statusCode];
            $this->assertEquals($expectedMessage, $message);
        }
        $classInstance->getResponseCodeMessage('invalid');
    }

    /**
     * Test the statusCode setter and getter as well as the exception thrown when an invalid value is given
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid code. Got status code 'invalid' (expected one of the following integer values: 1, 10, 7, 20, 8, 25, 21, 6, 50)
     */
    public function testStatusCodeSetterAndGetterAndException()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\Error';
        $classInstance = new Error();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $expectedStatusCodes = $this->getExpectedStatusCodesArray();
        foreach ($expectedStatusCodes as $statusCode => $statusMessage)
        {
            $setterResponse = $classInstance->setStatusCode($statusCode);
            $this->assertInstanceOf($expectedClass, $setterResponse);
            $getterResponse = $classInstance->getStatusCode();
            $this->assertEquals($statusCode, $getterResponse);
        }
        $classInstance->setStatusCode('invalid');
    }
}