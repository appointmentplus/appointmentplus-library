<?php
/**
 * Created by PhpStorm.
 * User: spiro79
 * Date: 29/04/15
 * Time: 22:29
 */

use apptlibrary\response\responsecodes\Factory;
use apptlibrary\response\responsecodes\IResponse;

/**
 * Class FactoryTest
 * @author    Ernesto Spiro Peimbert Andreakis
 */
class FactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\Factory';
        $reflectionClass = new ReflectionClass($expectedClass);
        $isInstantiable = $reflectionClass->isInstantiable();
        $this->assertEquals(false, $isInstantiable);
        $classInstance = Factory::getInstance();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $this->assertInstanceOf('\apptlibrary\resources\designpatterns\IFactory', $classInstance);
    }

    /**
     * Test the factory builder
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Cannot build class from unrecognizable status code (Invalid)
     */
    public function testFBuilderAndException()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\Factory';
        $factory = Factory::getInstance();
        $this->assertInstanceOf($expectedClass, $factory);
        $classesArray = array(
            'Error' => array(
                IResponse::STATUS_ERROR,
                IResponse::STATUS_INVALID_CALL_TYPE,
                IResponse::STATUS_INVALID_JSON_DATA_REQ,
                IResponse::STATUS_INVALID_METHOD,
                IResponse::STATUS_INVALID_OBJ_DATA_REQ,
                IResponse::STATUS_INVALID_PARAMETER,
                IResponse::STATUS_INVALID_RESOURCE,
                IResponse::STATUS_INVALID_XML_DATA_REQ,
                IResponse::STATUS_VALIDATION_ERROR
            ),
            'Success' => array(
                IResponse::STATUS_SUCCESS
            ),
            'AccessNotAllowed' => array(
                IResponse::STATUS_ACCESS_NOT_ALLOWED
            ),
            'AuthFailed' => array(
                IResponse::STATUS_AUTH_FAIL
            ),
            'Busy' => array(
                IResponse::STATUS_BUSY
            ),
            'NotYetImplemented' => array(
                IResponse::STATUS_NOT_YET_IMPLEMENTED
            ),
            'Unknown' => array(
                IResponse::STATUS_UNKNOWN_ERROR
            )
        );
        foreach ($classesArray as $classType => $status)
        {
            foreach ($status as $statusType)
            {
                $expectedInstanceOf = '\\apptlibrary\\response\\responsecodes\\' . $classType;
                $createdObject = $factory->build($statusType);
                $this->assertInstanceOf($expectedInstanceOf, $createdObject);
            }
        }
        $factory->build('Invalid');
    }
}