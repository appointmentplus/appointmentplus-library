<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/29/2015
 * Time: 6:13 PM
 */

use apptlibrary\response\responsecodes\NotYetImplemented;

/**
 * Class NotYetImplementedTest
 */
class NotYetImplementedTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test the class instantiation
     */
    public function testInstantiation()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\NotYetImplemented';
        $classInstance = new NotYetImplemented();
        $this->assertInstanceOf($expectedClass, $classInstance);
    }

    /**
     * Test the status codes and the exception if a wrong status code is provided
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid code. Got status code 'invalid' (expected one of the following integer values: 101)
     */
    public function testStatusCodes()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\NotYetImplemented';
        $classInstance = new NotYetImplemented();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $statusCodes = $classInstance->getStatusCodes();
        $expectedStatusCodes[NotYetImplemented::STATUS_NOT_YET_IMPLEMENTED] = 'Not yet implemented';
        $this->assertEquals($expectedStatusCodes, $statusCodes);
        foreach ($statusCodes as $statusCode => $statusMessage)
        {
            $message = $classInstance->getResponseCodeMessage($statusCode);
            $expectedMessage = $expectedStatusCodes[$statusCode];
            $this->assertEquals($expectedMessage, $message);
        }
        $classInstance->getResponseCodeMessage('invalid');
    }

    /**
     * Test the statusCode setter and getter as well as the exception thrown when an invalid value is given
     * @throws Exception
     * @expectedException \Exception
     * @expectedExceptionMessage Invalid code. Got status code 'invalid' (expected one of the following integer values: 101)
     */
    public function testStatusCodeSetterAndGetterAndException()
    {
        $expectedClass = '\apptlibrary\response\responsecodes\NotYetImplemented';
        $classInstance = new NotYetImplemented();
        $this->assertInstanceOf($expectedClass, $classInstance);
        $expectedStatusCodes[NotYetImplemented::STATUS_NOT_YET_IMPLEMENTED] = 'Not yet implemented';
        foreach ($expectedStatusCodes as $statusCode => $statusMessage)
        {
            $setterResponse = $classInstance->setStatusCode($statusCode);
            $this->assertInstanceOf($expectedClass, $setterResponse);
            $getterResponse = $classInstance->getStatusCode();
            $this->assertEquals($statusCode, $getterResponse);
        }
        $classInstance->setStatusCode('invalid');
    }
}