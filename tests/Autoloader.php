<?php

/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/22/2015
 * Time: 4:11 PM
 */

function autoload($class)
{
    if (is_file("..\\$class.php"))
    {
        require_once("..\\$class.php");
    }
}

spl_autoload_register('autoload');