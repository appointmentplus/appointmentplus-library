<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/21/2015
 * Time: 3:29 PM
 */

namespace apptlibrary\api;

use apptlibrary\request\IRequest;

/**
 * Interface IApi
 * @package apptlibrary\api
 */
interface IApi
{
    /**
     * Sets the request engine for the API calls
     * @param IRequest $request
     * @return self
     */
    public function setRequestEngine(IRequest $request);

    /**
     * returns the request engine being used
     * @return IRequest
     */
    public function getRequestEngine();

    /**
     * Returns the token property value
     *
     * @return string
     */
    public function getToken();

    /**
     * Sets the token property value
     * @param string $token The token to be used on the calls
     *
     * @return $this
     */
    public function setToken($token);

    /**
     * Sends a request through the request engine
     * @param $action
     * @param array $parameters
     * @return mixed
     */
    public function sendRequest($action, array $parameters);

    /**
     * Returns the API version
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getVersion(array $parameters);

    /**
     * Return list of clients
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getClientInterfaces(array $parameters);

    /**
     * Returns a valid authentication object
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getAuthenticationToken(array $parameters);

    /**
     * Invalidates an authentication token
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function expireAuthenticationToken(array $parameters);

    /**
     * Resets the authentication token timeout
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function keepAlive(array $parameters);

    /**
     * Returns a single resource object
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getItem(array $parameters);

    /**
     * Returns a list resource objects
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getItemsByFilter(array $parameters);

    /**
     * Creates an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function createItem(array $parameters);

    /**
     * Updates an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function updateItem(array $parameters);

    /**
     * Deletes an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function deleteItem(array $parameters);

    /**
     * Runs a groups calls within one call
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function aggregate(array $parameters);
}