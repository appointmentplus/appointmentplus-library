<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/21/2015
 * Time: 3:56 PM
 */

namespace apptlibrary\api;

use apptlibrary\request\IRequest;

/**
 * Class AApi
 * @package apptlibrary\api
 */
abstract class AApi implements IApi
{
    /**
     * Which request engine REST or SOAP
     * @var IRequest
     * @access protected
     */
    protected $requestEngine;
    /**
     * Token needed by the API
     * @var string
     * @access protected
     */
    protected $token;

    /**
     * Sets the request engine for the API calls
     * @param IRequest $request The request engine to be used for the calls
     * @return self
     */
    public function setRequestEngine(IRequest $request)
    {
        $this->requestEngine = $request;
        return $this;
    }

    /**
     * returns the request engine being used
     * @return IRequest
     */
    public function getRequestEngine()
    {
        return $this->requestEngine;
    }

    /**
     * Returns the token property value
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Sets the token property value
     * @param string $token The token to be used on the calls
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * Send the request
     * @param string $action Request action which is treated as part of the endpoint
     * @param array $parameters The parameters assoc array
     * @return mixed
     */
    public function sendRequest($action, array $parameters)
    {
        $endpoint = $action;
        $reqEngine = $this->getRequestEngine();
        $reqEngine->setAction($endpoint);
        $reqEngine->setParameters($parameters);
        return $reqEngine->request();
    }

    /**
     * Returns the API version
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getVersion(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Return list of clients
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getClientInterfaces(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Returns a valid authentication object
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getAuthenticationToken(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Invalidates an authentication token
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function expireAuthenticationToken(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Resets the authentication token timeout
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function keepAlive(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Returns a single resource object
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getItem(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Returns a list resource objects
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function getItemsByFilter(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Creates an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function createItem(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Updates an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function updateItem(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Deletes an object for a resource
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function deleteItem(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Runs a group of calls within one call
     * @param array $parameters Assoc array of parameters in the form of name=>value
     * @return mixed
     */
    public function aggregate(array $parameters)
    {
        $action = ucfirst(__FUNCTION__);
        if (!array_key_exists('token', $parameters))
        {
            $parameters['token'] = $this->getToken();
        }
        return $this->sendRequest($action, $parameters);
    }

    /**
     * Show URL request as GET
     * @return string
     */
    public function giveURL()
    {
        return $this->getRequestEngine()->giveURL();
    }
}