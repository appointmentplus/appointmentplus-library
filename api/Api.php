<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/21/2015
 * Time: 4:24 PM
 */

namespace apptlibrary\api;

use apptlibrary\request\IRequest;

/**
 * Class Api
 * @package apptlibrary\api
 */
class Api extends AApi
{
    /**
     * Class constructor
     * @param IRequest $requestEngine The request engine to be used
     */
    public function __construct(IRequest $requestEngine = NULL)
    {
        if (!is_null($requestEngine))
        {
            $this->setRequestEngine($requestEngine);
        }
    }
}