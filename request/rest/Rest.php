<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/20/2015
 * Time: 2:36 PM
 */

namespace apptlibrary\request\rest;

use apptlibrary\request\ARequest;
use \Exception;

/**
 * Class Rest
 * @package apptlibrary\request\rest
 */
class Rest extends ARequest
{
    /**
     * Array of parameters to be sent
     * @var array
     * @access protected
     */
    protected $parameters;

    /**
     * Initialize object
     * @access protected
     */
    protected function initCURL()
    {
        parent::initCURL();
        $this->serviceType = self::WS_REST_TYPE;
    }

    /**
     * Class constructor
     * @param string $url [OPTIONAL] URL for the webservice. The main endpoint
     * @throws Exception
     */
    public function __construct($url = NULL)
    {
        $this->initCURL();
        if (!is_null($url))
        {
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $isValidURL = filter_var($url, FILTER_VALIDATE_URL);
            if ($isValidURL)
            {
                $this->setURL($url);
            }
            else throw new Exception('Provided URL is not valid (' . $url . ')');
        }
    }

    /**
     * Send the request and return the response
     * @return mixed
     * @throws Exception
     */
    public function request()
    {
        $this->initCURL();
        $url = $this->getURL();
        $action = $this->getAction();
        if (empty($url))
        {
            throw new Exception('Service\'s endpoint URL is either empty or is not valid');
        }
        $finalURL = $url;
        if (!empty($action))
        {
            $finalURL .= self::URL_PATH_SEPARATOR . $action;
        }
        curl_setopt($this->CURLHandle, CURLOPT_URL, $finalURL);
        curl_setopt($this->CURLHandle, CURLOPT_POSTFIELDS, $this->getParameters());
        $result = curl_exec($this->CURLHandle);
        if (false === $result)
        {
            throw new Exception('cURL error: ' . curl_error($this->CURLHandle));
        }
        return $result;
    }

    /**
     * Sets the parameters for the request
     * @param array $parameters Set the parameters for the request
     * @return mixed
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * Get the parameters set to be send to the web service
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * Show URL request as GET
     * @return string
     */
    public function giveURL()
    {
        $url = $this->getURL();
        $action = $this->getAction();
        $finalURL = $url;
        if (!empty($action))
        {
            $finalURL .= self::URL_PATH_SEPARATOR . $action;
        }
        $parametersArray = array();
        foreach($this->getParameters() as $parameter => $value)
        {
			$keyValuePair = "{$parameter}=";
			if ($value instanceof \CURLFile) {
				$keyValuePair .= json_encode($value);
			} else {
				$keyValuePair .= $value;
			}
			$parametersArray[] = $keyValuePair;
        }
        $parametersString = implode('&',$parametersArray);
        $requestString = implode('?', array($finalURL,$parametersString));
        return $requestString;
    }
}