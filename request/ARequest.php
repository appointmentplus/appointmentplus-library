<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/17/2015
 * Time: 2:47 PM
 */

namespace apptlibrary\request;

use \Exception;

/**
 * Class ARequest
 * @package apptlibrary\request
 */
abstract class ARequest implements IRequest
{
    /**
     * Indicates what kind of webservices are being consumed: IRequest::WS_SOAP_TYPE or IRequest::WS_REST_TYPE
     * @var string
     * @access protected
     */
    protected $serviceType;
    /**
     * The CURL handler
     * @var resource
     * @access protected
     */
    protected $CURLHandle;
    /**
     * URL of the service for the requests
     * @var string
     * @access protected
     */
    protected $URL;
    /**
     * Action to be executed
     * @var string
     * @access protected
     */
    protected $action;
    /**
     * 32-character string used to identify which interface is requesting data. Contact your service provider to obtain your interface ID.
     * Provide the interface ID when performing the following actions:
     * -GetClientInterfaces
     * -GetAuthenticationToken
     * @var string
     * @access protected
     */
    protected $interface;
    /**
     * Provide one of either JSON or XML
     * Can be any of self::CALL_TYPE_JSON or self::CALL_TYPE_XML
     * @var string
     * @access protected
     */
    protected $callType;

    /**
     * Initialize the CURL handler
     * @access protected
     */
    protected function initCURL()
    {
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_POST, true);
        curl_setopt($curlHandle, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $this->CURLHandle = $curlHandle;
    }

    /**
     * Returns the serviceType property value
     *
     * @return string
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }

    /**
     * Sets the serviceType property value
     * @param string $serviceType Specify the kind of service to be used which can be either IRequest::WS_REST_TYPE or IRequest::WS_SOAP_TYPE
     * @access protected
     *
     * @return $this
     * @throws Exception
     */
    protected function setServiceType($serviceType)
    {
        if (in_array($serviceType, array(IRequest::WS_REST_TYPE, IRequest::WS_SOAP_TYPE)))
        {
            $this->serviceType = $serviceType;
        }
        else
        {
            throw new Exception('Service type expected to be IRequest::WS_REST_TYPE or IRequest::WS_SOAP_TYPE, got: ' . $serviceType);
        }
        return $this;
    }

    /**
     * Returns the CURLHandle property value
     *
     * @return resource
     */
    public function getCURLHandle()
    {
        return $this->CURLHandle;
    }

    /**
     * Sets the CURLHandle property value
     * @param resource $CURLHandle The curl resource
     *
     * @return $this
     * @throws Exception
     */
    public function setCURLHandle($CURLHandle)
    {
        if (is_resource($CURLHandle))
        {
            $this->CURLHandle = $CURLHandle;
        }
        else
        {
            throw new Exception('Expecting a valid resource');
        }
        return $this;
    }

    /**
     * Returns the URL property value
     *
     * @return string
     */
    public function getURL()
    {
        return $this->URL;
    }

    /**
     * Sets the URL property value
     * @param string $URL The FQD for the URL
     *
     * @return $this
     * @throws Exception
     */
    public function setURL($URL)
    {
        $url = filter_var($URL, FILTER_SANITIZE_URL);
        $validURL = filter_var($url, FILTER_VALIDATE_URL);
        if ($validURL)
        {
            $this->URL = $url;
        }
        else
        {
            throw new Exception('URL is not valid (' . $URL . ')');
        }
        return $this;
    }

    /**
     * Sets the action for the request
     * @param string $action The action being requested. Alias for the endpoint
     * @return mixed
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * Get the action set for the request to be sent
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Returns the interface property value
     *
     * @return string
     */
    public function getInterface()
    {
        return $this->interface;
    }

    /**
     * Sets the interface property value
     * @param string $interface The interface code as provided by Appointment+
     *
     * @return $this
     */
    public function setInterface($interface)
    {
        $this->interface = $interface;
        return $this;
    }

    /**
     * Returns the callType property value
     *
     * @return string
     */
    public function getCallType()
    {
        return $this->callType;
    }

    /**
     * Sets the callType property value
     * @param string $callType The request call type which can be either IRequest::CALL_TYPE_JSON or IRequest::CALL_TYPE_XML
     *
     * @return $this
     * @throws Exception
     */
    public function setCallType($callType)
    {
        if (in_array($callType, array(IRequest::CALL_TYPE_JSON, IRequest::CALL_TYPE_XML)))
        {
            $this->callType = $callType;
        }
        else
        {
            throw new Exception('Call type expected to be IRequest::CALL_TYPE_JSON or IRequest::CALL_TYPE_XML got: ' . $callType);
        }
        return $this;
    }
}