<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/17/2015
 * Time: 2:04 PM
 */

namespace apptlibrary\request;

/**
 * Interface IRequest
 * @package apptlibrary\request
 */
interface IRequest
{
    /**
     * URL path separator character
     */
    const URL_PATH_SEPARATOR = '/';
    /**
     * Indicates if the calls will be made to a SOAP service
     */
    const WS_SOAP_TYPE = 'SOAP';
    /**
     * Indicates if the calls will be made to a REST service
     */
    const WS_REST_TYPE = 'REST';
    /**
     * JSON data type constant
     */
    const CALL_TYPE_JSON = 'json';
    /**
     * XML data type constant
     */
    const CALL_TYPE_XML = 'xml';

    /**
     * Sets the parameters for the request
     * @param array $parameters Array of parameters to be sent with the request
     * @return mixed
     */
    public function setParameters(array $parameters);

    /**
     * Sets the action for the request
     * @param string $action The action to be called for the request. Alias for the endpoint
     * @return mixed
     */
    public function setAction($action);

    /**
     * Send the request
     * @return mixed
     */
    public function request();

    /**
     * Show URL request as GET
     * @return string
     */
    public function giveURL();
}