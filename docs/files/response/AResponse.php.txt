<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/15/2015
 * Time: 3:33 PM
 */

namespace apptlibrary\response;

use \Exception;
use \ReflectionClass;

/**
 * Class AResponse
 * @package apptlibrary\response
 */
abstract class AResponse implements IResponse
{
    /**
     * Response type either self::DATA_XML or self::DATA_JSON
     * @var string
     * @access protected
     */
    protected $responseType;
    /**
     * Status code of response
     * @var integer
     * @access protected
     */
    protected $status;
    /**
     * Human-friendly message related to the status of the request
     * @var string
     * @access protected
     */
    protected $statusMessage;
    /**
     * Parse-able response of the request
     * @var mixed
     * @access protected
     */
    protected $details;

    /**
     * Returns the responseType property value
     *
     * @return string
     */
    public function getResponseType()
    {
        return $this->responseType;
    }

    /**
     * Sets the responseType property value
     * @param string $responseType
     * @access protected
     *
     * @return $this
     * @throws Exception
     */
    protected function setResponseType($responseType)
    {
        if (in_array($responseType, array(self::DATA_JSON, self::DATA_XML)))
        {
            $this->responseType = $responseType;
        }
        else
        {
            throw new Exception('Invalid response type given (' . $responseType . '). Expecting either IResponse::DATA_JSON or IResponse::DATA_XML');
        }
        return $this;
    }

    /**
     * Returns the status property value
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status property value
     * @param int $status
     *
     * @return $this
     * @throws Exception
     */
    public function setStatus($status)
    {
        $throwException = true;
        $reflectionInterface = new ReflectionClass('\apptlibrary\response\responsecodes\IResponse');
        $statusConstants = $reflectionInterface->getConstants();
        $validStatusValues = array();
        $validConstantsNames = array();
        foreach ($statusConstants as $constantName => $value)
        {
            if (stristr($constantName, 'STATUS_'))
            {
                $validStatusValues[] = $value;
                $validConstantsNames[] = $constantName;
            }
        }
        if (is_numeric($status))
        {
            if (in_array($status, $validStatusValues))
            {
                $this->status = (int)$status;
                $throwException = false;
            }
        }
        if ($throwException)
        {
            $acceptedConstNames = join(', \apptlibrary\response\responsecodes\IResponse::', $validConstantsNames);
            throw new Exception('Invalid status given (' . $status . '). Expecting one of : \apptlibrary\response\responsecodes\IResponse::' . $acceptedConstNames);
        }
        return $this;
    }

    /**
     * Returns the statusMessage property value
     *
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }

    /**
     * Sets the statusMessage property value
     * @param string $statusMessage
     *
     * @return $this
     */
    public function setStatusMessage($statusMessage)
    {
        $this->statusMessage = $statusMessage;
        return $this;
    }

    /**
     * Returns the details property value
     *
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Sets the details property value
     * @param mixed $details
     *
     * @return $this
     */
    public function setDetails($details)
    {
        $this->details = $details;
        return $this;
    }

    /**
     * Gets the details data from a JSON string
     * @param string $data JSON string
     * @param bool $asArray Return as array
     * @param string $errMsg [OPTIONAL] Error message for the json string
     * @access protected
     * @return bool|mixed
     */
    protected function getFromJSON($data, $asArray = false, &$errMsg = '')
    {
        $ret = false;
        $errMsg = '';
        $result = json_decode($data, $asArray);
        switch (json_last_error())
        {
            case JSON_ERROR_NONE:
                $errMsg = ''; // JSON is valid
                break;
            case JSON_ERROR_DEPTH:
                $errMsg = 'Maximum stack depth exceeded.' . PHP_EOL;
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $errMsg = 'Underflow or the modes mismatch.' . PHP_EOL;
                break;
            case JSON_ERROR_CTRL_CHAR:
                $errMsg = 'Unexpected control character found.' . PHP_EOL;
                break;
            case JSON_ERROR_SYNTAX:
                $errMsg = 'Syntax error, malformed JSON.' . PHP_EOL;
                break;
            // only PHP 5.3+
            case JSON_ERROR_UTF8:
                $errMsg = 'Malformed UTF-8 characters, possibly incorrectly encoded.' . PHP_EOL;
                break;
            default:
                $errMsg = 'Unknown JSON error occurred.' . PHP_EOL;
                break;
        }
        if (empty($errMsg))
        {
            $ret = $result;
        }
        else
        {
            $errMsg = 'JSON errors' . PHP_EOL . '--------------------------------------------' . PHP_EOL . $errMsg . PHP_EOL . PHP_EOL . '--------------------------------------------' . PHP_EOL . PHP_EOL;
        }
        return $ret;
    }

    /**
     * Get the XML errors
     * @param \LibXMLError $error The error object
     * @param array $xml The xml array
     * @access protected
     * @return string
     */
    protected function getXMLError($error, array $xml)
    {
        $return = $xml[$error->line - 1] . PHP_EOL;
        $return .= str_repeat('-', $error->column - 1) . '^' . PHP_EOL;

        switch ($error->level)
        {
            case LIBXML_ERR_WARNING:
                $return .= "Warning {$error->code}: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error {$error->code}: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error {$error->code}: ";
                break;
        }

        $return .= trim($error->message) . PHP_EOL .
            "  Line: {$error->line}" . PHP_EOL .
            "  Column: {$error->column}";

        if ($error->file)
        {
            $return .= PHP_EOL . "  File: {$error->file}";
        }
        return 'XML errors' . PHP_EOL . '--------------------------------------------' . PHP_EOL . $return . PHP_EOL . PHP_EOL . '--------------------------------------------' . PHP_EOL . PHP_EOL;
    }

    /**
     * Get the data from the XML string
     * @param string $data XML string
     * @param string $errMsg [OPTIONAL] Returns an error message if there was an error
     * @access protected
     * @return \SimpleXMLElement
     */
    protected function getFromXML($data, &$errMsg = '')
    {
        libxml_use_internal_errors(true);
        $doc = simplexml_load_string($data);
        $xml = explode("\n", $data);
        if (!$doc)
        {
            $errors = libxml_get_errors();
            foreach ($errors as $error)
            {
                $errMsg .= self::getXMLError($error, $xml);
            }
            libxml_clear_errors();
        }
        return $doc;
    }

    /**
     * Gets the data from the details element as an object of stdClass for JSON or \SimpleXMLElement
     * @param string $dataInput XML or JSON string
     * @param string $responseType the response type as detected by the function [Value is returned here]
     * @param string $errors A list of errors [Value is returned here]
     * @access protected
     * @return bool|\stdClass|\SimpleXMLElement
     * @throws Exception
     */
    protected function getData($dataInput, &$responseType = '', &$errors = '')
    {
        $return = NULL;
        $data = $this->getFromJSON($dataInput, false, $errors);
        if (false !== $data)
        {
            $responseType = self::DATA_JSON;
            $return = $data;
        }
        else
        {
            $data = $this->getFromXML($dataInput, $errors);
            if (false !== $data)
            {
                $responseType = self::DATA_XML;
                $return = $data;
            }
        }
        return $return;
    }
}
