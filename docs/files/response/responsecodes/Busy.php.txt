<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:16 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class Busy
 * @package apptlibrary\response\responsecodes
 */
class Busy extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_BUSY;
        $this->statusCodes[self::STATUS_BUSY] = 'Service is busy please try again later';
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}
