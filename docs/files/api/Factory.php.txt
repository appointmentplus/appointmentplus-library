<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/21/2015
 * Time: 4:35 PM
 */

namespace apptlibrary\api;


use apptlibrary\request\IRequest;
use apptlibrary\request\rest\Rest;
use apptlibrary\resources\designpatterns\ASingleton;
use apptlibrary\resources\designpatterns\IFactory;
use \Exception;

/**
 * Class Factory
 * @package apptlibrary\api
 */
class Factory extends ASingleton implements IFactory
{
    /**
     * The class instance
     * @access protected
     * @var self
     */
    protected static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     * @static
     *
     * @return self
     */
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * Builds the desired object
     * @param string $class The class to build
     * @param array $options Associative array with additional options [OPTIONAL]
     * @return object
     * @throws Exception
     */
    public static function build($class, array $options = NULL)
    {
        $api = new Api();
        switch ($class)
        {
            case IRequest::WS_REST_TYPE:
                $request = new Rest();
                if (isset($options['url']))
                {
                    $request->setURL($options['url']);
                }
                else
                {
                    throw new Exception('\'url\' option is required when request building of ' . IRequest::WS_REST_TYPE);
                }
                $api->setRequestEngine($request);
                break;
        }
        return $api;
    }
}
