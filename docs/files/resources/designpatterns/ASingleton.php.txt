<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/8/2015
 * Time: 3:44 PM
 */

namespace apptlibrary\resources\designpatterns;

/**
 * Class ASingleton
 * Defines basic functionality for any Singleton class
 * @package apptlibrary\resources\designpatterns
 */
abstract class ASingleton implements ISingleton
{
    /**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     * @access protected
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     * @access protected
     *
     * @return void
     */
    protected function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     * @access protected
     *
     * @return void
     */
    protected function __wakeup()
    {
    }
}
