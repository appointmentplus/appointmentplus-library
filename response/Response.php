<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/14/2015
 * Time: 12:23 PM
 */

namespace apptlibrary\response;

use \Exception;

/**
 * Class Response
 * @package apptlibrary\response
 */
class Response extends AResponse
{
    /**
     * Sets the values for the object's properties from a string that might be JSON or XML.
     * The function will determine what type it is and assign data accordingly.
     * @param string $responseData The response raw string
     * @throws Exception
     */
    public function setValues($responseData)
    {
        if (!empty($responseData))
        {
            $responseType = NULL;
            $errors = NULL;
            $data = $this->getData($responseData, $responseType, $errors);
            if ((false === $data) || is_null($data))
            {
                throw new Exception('Error instatiating class ' . __CLASS__ . ' with data.' . PHP_EOL . 'Errors:' . PHP_EOL . $errors);
            }
            $this->setResponseType($responseType);
            if ($this->getResponseType() == self::DATA_XML)
            {
                $data = json_decode(json_encode($data));
            }
            $this->setStatus($data->status);
            $this->setStatusMessage($data->statusMessage);
            $this->setDetails($data->details);
        }
    }

    /**
     * Class constructor
     * @param mixed $responseData [OPTIONAL] response data to be assigned to the class properties Tre
     * @throws Exception
     */
    public function __construct($responseData = '')
    {
        if (!empty($responseData))
        {
            $this->setValues($responseData);
        }
    }
}