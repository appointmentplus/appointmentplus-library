<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/14/2015
 * Time: 11:56 AM
 */

namespace apptlibrary\response;


/**
 * Interface Response
 * @package apptlibrary\response
 */
interface IResponse
{
    /**
     * Constant for XML data type
     */
    const DATA_XML = 'xml';
    /**
     * Constant for JSON data type
     */
    const DATA_JSON = 'json';

    /**
     * Gets the status code
     * As per the list of status codes
     * @return mixed
     */
    public function getStatus();

    /**
     * Gets the status message
     * @return mixed
     */
    public function getStatusMessage();

    /**
     * gets the response data
     * @return mixed
     */
    public function getDetails();
}