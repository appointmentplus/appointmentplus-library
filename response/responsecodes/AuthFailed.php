<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:13 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class AuthFailed
 * @package apptlibrary\response\responsecodes
 */
class AuthFailed extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_AUTH_FAIL;
        $this->statusCodes[self::STATUS_AUTH_FAIL] = 'Authentication failed';
    }
    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}