<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:07 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class Error
 * @package apptlibrary\response\responsecodes
 */
class Error extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_ERROR;
        $this->statusCodes[self::STATUS_ERROR] = 'An error has occurred';
        $this->statusCodes[self::STATUS_INVALID_CALL_TYPE] = 'Invalid call type provided';
        $this->statusCodes[self::STATUS_INVALID_JSON_DATA_REQ] = 'Invalid JSON request (data) provided';
        $this->statusCodes[self::STATUS_INVALID_METHOD] = 'Invalid method provided';
        $this->statusCodes[self::STATUS_INVALID_OBJ_DATA_REQ] = 'Invalid object request (data) provided';
        $this->statusCodes[self::STATUS_INVALID_PARAMETER] = 'Invalid parameter provided';
        $this->statusCodes[self::STATUS_INVALID_RESOURCE] = 'Invalid resource provided';
        $this->statusCodes[self::STATUS_INVALID_XML_DATA_REQ] = 'Invalid XML request (data) provided';
        $this->statusCodes[self::STATUS_VALIDATION_ERROR] = 'Validation error has occurred';
    }
    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}