<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/10/2015
 * Time: 12:43 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Interface IResponse
 * @package apptlibrary\response\responsecodes
 */
interface IResponse
{
    /**
     * Status success
     */
    const STATUS_SUCCESS = 0;
    /**
     * An error has occurred
     */
    const STATUS_ERROR = 1;
    /**
     * Authentication failed
     */
    const STATUS_AUTH_FAIL = 2;
    /**
     * Access not allowed
     */
    const STATUS_ACCESS_NOT_ALLOWED = 3;
    /**
     * Service is busy, please try again later
     */
    const STATUS_BUSY = 5;
    /**
     * Invalid XML request (data) provided
     */
    const STATUS_INVALID_XML_DATA_REQ = 6;
    /**
     * Invalid JSON request (data) provided
     */
    const STATUS_INVALID_JSON_DATA_REQ = 7;
    /**
     * Invalid object request (data) provided
     */
    const STATUS_INVALID_OBJ_DATA_REQ = 8;
    /**
     * Invalid call type provided
     */
    const STATUS_INVALID_CALL_TYPE = 10;
    /**
     * Invalid method provided
     */
    const STATUS_INVALID_METHOD = 20;
    /**
     * Invalid resource provided
     */
    const STATUS_INVALID_RESOURCE = 21;
    /**
     * Invalid parameter provided
     */
    const STATUS_INVALID_PARAMETER = 25;
    /**
     * Validation error has occurred
     */
    const STATUS_VALIDATION_ERROR = 50;
    /**
     * Unknown error has occurred
     */
    const STATUS_UNKNOWN_ERROR = 100;
    /**
     * Not yet implemented
     */
    const STATUS_NOT_YET_IMPLEMENTED = 101;
    /**
     * HTTP success code
     */
    const HTTP_SUCCESS = 200;
    /**
     * HTTP error code
     */
    const HTTP_ERROR = 400;
    /**
     * HTTP authentication fail code
     */
    const HTTP_AUTH_FAIL = 401;
    /**
     * HTTP access not allowed code
     */
    const HTTP_ACCESS_NOT_ALLOWED = 403;
    /**
     * HTTP unknown error code
     */
    const HTTP_UNKNOWN_ERROR = 500;
    /**
     * HTTP not yet implemented code
     */
    const HTTP_NOT_YET_IMPLEMENTED = 501;
    /**
     * HTTP busy code
     */
    const HTTP_BUSY = 503;

    /**
     * Gets the response message string
     * @param integer $statusCode
     * @return string
     * @throws \Exception
     */
    public function getResponseCodeMessage($statusCode = NULL);
}