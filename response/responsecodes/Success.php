<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 3:58 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class Success
 * @package apptlibrary\response\responsecodes
 */
class Success extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_SUCCESS;
        $this->statusCodes[self::STATUS_SUCCESS] = 'Request processed successfully';
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}