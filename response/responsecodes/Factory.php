<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 5:19 PM
 */

namespace apptlibrary\response\responsecodes;


use apptlibrary\resources\designpatterns\ASingleton;
use apptlibrary\resources\designpatterns\IFactory;
use \Exception;

/**
 * Class Factory for Responses
 * @package apptlibrary\response\responsecodes
 */
class Factory extends ASingleton implements IFactory
{
    /**
     * The class instance
     * @access protected
     * @var self
     */
    protected static $instance;

    /**
     * Returns the *Singleton* instance of this class.
     * @static
     *
     * @return self
     */
    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * Builds the desired object
     * @param integer $statusCode The status code value to build the corresponding object.
     * Must be an integer value
     * @return object
     * @throws Exception
     */
    public static function build($statusCode)
    {
        $response = NULL;
        $responseClass = NULL;
        if (is_numeric($statusCode))
        {
            $statusCode = (int)$statusCode;
            switch ($statusCode)
            {
                case IResponse::STATUS_ERROR:
                case IResponse::STATUS_INVALID_CALL_TYPE:
                case IResponse::STATUS_INVALID_JSON_DATA_REQ:
                case IResponse::STATUS_INVALID_METHOD:
                case IResponse::STATUS_INVALID_OBJ_DATA_REQ:
                case IResponse::STATUS_INVALID_PARAMETER:
                case IResponse::STATUS_INVALID_RESOURCE:
                case IResponse::STATUS_INVALID_XML_DATA_REQ:
                case IResponse::STATUS_VALIDATION_ERROR:
                    $responseClass = 'Error';
                    break;
                case IResponse::STATUS_SUCCESS:
                    $responseClass = 'Success';
                    break;
                case IResponse::STATUS_ACCESS_NOT_ALLOWED:
                    $responseClass = 'AccessNotAllowed';
                    break;
                case IResponse::STATUS_AUTH_FAIL:
                    $responseClass = 'AuthFailed';
                    break;
                case IResponse::STATUS_BUSY:
                    $responseClass = 'Busy';
                    break;
                case IResponse::STATUS_NOT_YET_IMPLEMENTED:
                    $responseClass = 'NotYetImplemented';
                    break;
                case IResponse::STATUS_UNKNOWN_ERROR:
                    $responseClass = 'Unknown';
                    break;
            }
        }
        if (!is_null($responseClass))
        {
            $responseClass = __NAMESPACE__ . self::NAMESPACE_SEPARATOR . $responseClass;
            $response = new $responseClass();
            $response->setStatusCode($statusCode);
        }
        else
        {
            throw new Exception('Cannot build class from unrecognizable status code (' . $statusCode . ')');
        }
        return $response;
    }
}