<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:17 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class Unknown
 * @package apptlibrary\response\responsecodes
 */
class Unknown extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_UNKNOWN_ERROR;
        $this->statusCodes[self::STATUS_UNKNOWN_ERROR] = 'Unknown error has occurred';
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}