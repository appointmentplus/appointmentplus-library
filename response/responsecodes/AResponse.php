<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 11:30 AM
 */

namespace apptlibrary\response\responsecodes;

use apptlibrary\resources\designpatterns\ASingleton;
use \Exception;

/**
 * Class AResponse
 * @package apptlibrary\response\responsecodes
 */
abstract class AResponse implements IResponse
{
    /**
     * The set of status codes valid for the http response code
     * @var array
     * @access protected
     */
    protected $statusCodes;
    /**
     * The HTTP response code
     * @var integer
     * @access protected
     */
    protected $httpCode;
    /**
     * Selected status code
     * @var integer
     * @access protected
     */
    protected $statusCode;

    /**
     * Returns the statusCodes property value
     *
     * @return array
     */
    public function getStatusCodes()
    {
        return $this->statusCodes;
    }

    /**
     * Returns the statusCode property value
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the statusCode property value
     * @param int $statusCode
     *
     * @return $this
     * @throws Exception
     */
    public function setStatusCode($statusCode)
    {
        $statusCodes = array_keys($this->getStatusCodes());
        if (in_array($statusCode, $statusCodes, true))
        {
            $this->statusCode = $statusCode;
        }
        else
        {
            $validStatusCodes = implode(', ', $statusCodes);
            throw new Exception('Invalid code. Got status code \'' . $statusCode . '\' (expected one of the following integer values: ' . $validStatusCodes . ')');
        }
        return $this;
    }

    /**
     * Gets the response message string
     * @param integer $statusCode
     * @return string
     * @throws Exception
     */
    public function getResponseCodeMessage($statusCode = NULL)
    {
        $return = NULL;
        $statusCode = (is_null($statusCode)) ? $this->statusCode : $statusCode;
        $statusCodes = array_keys($this->getStatusCodes());
        if (in_array($statusCode, $statusCodes, true))
        {
            $return = $this->statusCodes[$statusCode];
        }
        else
        {
            $validStatusCodes = implode(', ', $statusCodes);
            throw new Exception('Invalid code. Got status code \'' . $statusCode . '\' (expected one of the following integer values: ' . $validStatusCodes . ')');
        }
        return $return;
    }
}