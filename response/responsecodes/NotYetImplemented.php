<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:18 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class NotYetImplemented
 * @package apptlibrary\response\responsecodes
 */
class NotYetImplemented extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_NOT_YET_IMPLEMENTED;
        $this->statusCodes[self::STATUS_NOT_YET_IMPLEMENTED] = 'Not yet implemented';
    }

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}