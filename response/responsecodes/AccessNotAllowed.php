<?php
/**
 * Created by PhpStorm.
 * User: Ernesto Spiro
 * Date: 4/13/2015
 * Time: 4:14 PM
 */

namespace apptlibrary\response\responsecodes;

/**
 * Class AccessNotAllowed
 * @package apptlibrary\response\responsecodes
 */
class AccessNotAllowed extends AResponse
{
    /**
     * Initializer
     * @access protected
     */
    protected function init()
    {
        $this->httpCode = self::HTTP_ACCESS_NOT_ALLOWED;
        $this->statusCodes[self::STATUS_ACCESS_NOT_ALLOWED] = 'Access not allowed';
    }
    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->init();
    }
}